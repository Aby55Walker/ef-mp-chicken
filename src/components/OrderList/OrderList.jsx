import React from 'react';
import { OrderItem } from '../OrderItem/OrderItem';
import { NothingYet } from '../NothingYet/NothingYet';

// Список заказов во вкладке "История заказов"
export const OrderList = (props) => {
  const { array = [], classes = '', ...other } = props;

  const list = array.reverse().map((data) => <OrderItem key={data.id} data={data} classes="order-list__item" />);

  return (
    <>
      {list.length ? (
        <ol className={`order-list${classes ? ` ${classes}` : ''}`} {...other}>
          {list}
        </ol>
      ) : (
        <NothingYet />
      )}
    </>
  );
};
