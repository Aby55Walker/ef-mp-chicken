/* eslint-disable camelcase */
import React from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Btn } from '../Btn/Btn';
import { shortAuthPage } from '../../constants/pagelinks';
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd';
import getFormattedPrice from '../../functions/getFormattedPrice';

export const OrderBtn = (props) => {
  const {
    btnText = 'Заказать',
    classes,
    onClickRegistered,
    onClickUnregistered,
    ...other
  } = props;

  const { pathname } = useLocation();
  const history = useHistory();
  const { loggedIn } = useSelector((state) => state.auth);
  const { sum_total, time_to_completion } = useSelector((state) => state.cart.content.data);
  const blockClasses = cn(`order-btn ${classes || ''}`, { });
  const fixedPrice = sum_total ? sum_total.replace(/[^0-9\.]+/g, '').slice(0, -3) : false;

  const clickHandle = () => {
    if (loggedIn) {
      // пересчёт корзины и переход на стр оформления заказа
      history.push('/delivery/1');
      if (onClickRegistered) onClickRegistered();
    } else {
      // открытие модалки короткой регистрации,
      // после введения кода: пересчёт корзины и переход на стр оформления заказа
      history.push(`${deleteSlashAtTheEnd(pathname)}${shortAuthPage}`);
      if (onClickUnregistered) onClickUnregistered();
    }
  };

  return (
    <div className={blockClasses}>
      <Btn primary fullWidth centred text={btnText} classes="order-btn__target" onClick={clickHandle} {...other} />

      {(typeof time_to_completion !== 'undefined' && fixedPrice)
        ? (
          <>
            <span className="order-btn__text order-btn__time">~{time_to_completion}&nbsp;мин.</span>
            <span className="order-btn__text order-btn__price">{getFormattedPrice(fixedPrice)}&#160;&#8381;</span>
          </>
        )
        : null}
    </div>
  );
};
