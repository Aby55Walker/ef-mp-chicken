import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import tempImage from '../../assets/images/catalog/product-2.png';
import { Spoiler } from '../Spoiler/Spoiler';
import { InternalLink } from '../InternalLink/InternalLink';
import couponImg from '../../assets/images/coupon-1.png';
import { ProductComments } from '../ProductComments/ProductComments';
import { Modifiers } from './includes/ModifiersList';

export const Product = (props) => {
  const {
    classes,
    weight,
    price,
    name,
    description,
    id,
    img,
    comments = [],
    combo,
    modifiers = [],
    renderActionBlock,
    onCommentsChange,
    onAllChosen,
    onAdditivesChange,
    innerProduct = {},
    ...other
  } = props;

  const dispatch = useDispatch();

  const [allAdditives, setAllAdditives] = useState([]); // все добавки (для отправки)
  const [allChosen, setAllChosen] = useState(false); // boolean все добавки выбраны
  const [additivePriceSum, setAdditivePriceSum] = useState(0); // суммарная цена всех добавок
  const [commentsPriceSum, setCommentsPriceSum] = useState(0); // суммарная цена всех комментариев
  const [allInnerProductsChosen, setAllInnerProductsChosen] = useState(false); // boolean выбраны все добавки во внутренних продуктах
  const [btnDisabled, setBtnDisabled] = useState(true);

  // массив с id добавок
  const commentsChangeHandler = (selected, commentsPrice) => {
    setCommentsPriceSum(commentsPrice);
    if (onCommentsChange) onCommentsChange(selected, commentsPrice, id);
  };

  const additivesChangeHandler = (additivesList, additivesPrice) => {
    setAllAdditives(additivesList);
    setAdditivePriceSum(additivesPrice);
    if (onAdditivesChange) onAdditivesChange(additivesList, additivesPrice, id);
  };

  const allChosenChangeHandler = (val) => {
    setAllChosen(val);
    if (onAllChosen) onAllChosen(val, id);
  };

  // btn disabled
  useEffect(() => {
    const hasCombo = combo?.length;
    const hasModifiers = modifiers?.length;
    if (!hasCombo && !hasModifiers) setBtnDisabled(false);

    if (hasCombo || hasModifiers) {
      setBtnDisabled(
        !allChosen || allInnerProductsChosen ? !allInnerProductsChosen : combo?.length,
      );
    }
  }, [combo?.length, modifiers?.length, allChosen, allInnerProductsChosen]);

  // boolean выбраны опции в всех внутренних продуктах
  useEffect(() => {
    if (Object.keys(innerProduct)?.length) {
      setAllInnerProductsChosen(
        Object.values(innerProduct)?.map((item) => item.allChosen)?.length === combo?.length,
      );
    }
  }, [innerProduct]);

  const blockClasses = cn(`product ${classes || ''}`, {});

  return (
    <>
      <div className={blockClasses} {...other}>
        <div className="product__img-wp">
          <img className="product__img" src={tempImage} alt={name} />
        </div>
        <h3 className="product__title">{name}</h3>
        <div className="product__desc" dangerouslySetInnerHTML={{ __html: description }} />
        <span className="product__weight">{weight}</span>

        {/* добавки */}

        {comments?.length
          ? (
            <Spoiler title="Добавки" classes="product__spoiler">
              <ProductComments onChange={commentsChangeHandler} comments={comments} />
            </Spoiler>
          ) : null}

        {
          combo?.length
            ? (
              combo.map(({ id, name }) => (
                <InternalLink
                  key={id}
                  success={innerProduct[id]?.allChosen}
                  prodId={id}
                  img={couponImg}
                  title={name}
                  desc="Выберите глазировку"
                />
              ))
            ) : null
        }

        {modifiers?.length
          ? (
            <Modifiers
              onAllChosenChange={allChosenChangeHandler}
              onChange={additivesChangeHandler}
              modifiers={modifiers}
            />
        ) : null}

      </div>
      {/* рендерит блок внизу модалки */}
      {renderActionBlock
      && renderActionBlock({
        ...props,
        disabled: btnDisabled,
        allAdditives,
        fullPrice: Math.floor(price) + additivePriceSum + commentsPriceSum,
      })}
    </>
  );
};

Product.propTypes = {
  classes: PropTypes.any,
  description: PropTypes.any,
  id: PropTypes.any,
  img: PropTypes.any,
  modifiers: PropTypes.array,
  name: PropTypes.any,
  price: PropTypes.any,
  weight: PropTypes.any,
};
