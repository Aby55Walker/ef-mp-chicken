export const productFakeData = {
  data: {
    id: 3,
    name: 'Ягодный пунш',
    category: {
      slug: 'burgery',
      name: 'Бургеры',
    },
    description: 'Наш ягодный пунш согреет вас во время самых холодных зим!',
    pictures: {
      preview: 'http://example.com/preview.jpg',
      detail: 'http://example.com/detail.jpg',
    },
    weight: 250,
    price: '99.00',
    stocks: [
      {
        id: 2950,
        title: 'Название акции',
        description: 'Текст акции',
        activity: {
          from: '2020-01-01',
          to: '2021-05-01',
        },
        picture: {
          preview: 'http://example.com/preview.jpg',
          detail: 'http://example.com/detail.jpg',
        },
        discounts: {
          name: 'tass',
          unit: 'percentage',
          value: 53,
        },
      },
      {
        id: 45,
        title: 'Название акции',
        description: 'Текст акции',
        activity: {
          from: '2020-01-01',
          to: '2021-05-01',
        },
        picture: {
          preview: 'http://example.com/preview.jpg',
          detail: 'http://example.com/detail.jpg',
        },
        discounts: {
          name: 'tass',
          unit: 'percentage',
          value: 53,
        },
      },
    ],
    markers: [
      {
        name: 'MARKER 1',
        image: 'http:\\/\\/api.chickenbox.test\\/asdaf',
      },
      {
        name: 'MARKER 2',
        image: 'http:\\/\\/api.chickenbox.test\\/asdaf',
      },
    ],
    comments: [
      {
        id: 1,
        name: 'dssvdavsv',
        price: 0,
      },
      {
        id: 2,
        name: 'COMMENT',
        price: 123.12,
      },
      {
        id: 3,
        name: 'COCOMMENT',
        price: 123.12,
      },
    ],
    modifiers: {
      0: {
        name: 'Васаби-кунжут',
        price: '123.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
        type: 1,
        count_available: 23,
        count: 465,
      },
      1: {
        name: 'Лимон-перц',
        price: '24.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
      },
      2: {
        name: 'Тайская',
        price: '24.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
      },
      3: {
        name: 'Тайская',
        price: '24.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
      },
      4: {
        name: 'Тайская',
        price: '24.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
      },
      5: {
        name: 'Очень длинное название',
        price: '24.00',
        image: {
          preview: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
          detail: 'http:\\/\\/api.chickenbox.test\\/dsfqe',
        },
      },
      type: 2,
      count_available: 156,
    },
    stops: [
      1,
      2,
      3,
      4,
    ],
    upsale: [
      {
        id: 6,
        name: 'Название товара',
        description: 'Описание товара',
        weight: 321,
        price: '11.00',
        pictures: {
          preview: 'http:\\/\\/api.chickenbox.test\\/asdaf',
          detail: 'http:\\/\\/api.chickenbox.test\\/asdaf',
        },
        stocks: [
          {
            id: 687,
            title: 'Название акции',
            description: 'Текст акции',
            activity: {
              from: '2020-01-01',
              to: '2021-05-01',
            },
            picture: {
              preview: 'http://example.com/preview.jpg',
              detail: 'http://example.com/detail.jpg',
            },
            discounts: {
              name: 'tass',
              unit: 'percentage',
              value: 53,
            },
          },
          {
            id: 3924,
            title: 'Название акции',
            description: 'Текст акции',
            activity: {
              from: '2020-01-01',
              to: '2021-05-01',
            },
            picture: {
              preview: 'http://example.com/preview.jpg',
              detail: 'http://example.com/detail.jpg',
            },
            discounts: {
              name: 'tass',
              unit: 'percentage',
              value: 53,
            },
          },
        ],
      },
      {
        id: 8,
        name: 'Название товара',
        description: 'Описание товара',
        weight: 123,
        price: '12.00',
        pictures: {
          preview: 'http:\\/\\/api.chickenbox.test\\/asdaf',
          detail: 'http:\\/\\/api.chickenbox.test\\/asdaf',
        },
        stocks: [
          {
            id: 1440,
            title: 'Название акции',
            description: 'Текст акции',
            activity: {
              from: '2020-01-01',
              to: '2021-05-01',
            },
            picture: {
              preview: 'http://example.com/preview.jpg',
              detail: 'http://example.com/detail.jpg',
            },
            discounts: {
              name: 'tass',
              unit: 'percentage',
              value: 53,
            },
          },
          {
            id: 4917,
            title: 'Название акции',
            description: 'Текст акции',
            activity: {
              from: '2020-01-01',
              to: '2021-05-01',
            },
            picture: {
              preview: 'http://example.com/preview.jpg',
              detail: 'http://example.com/detail.jpg',
            },
            discounts: {
              name: 'tass',
              unit: 'percentage',
              value: 53,
            },
          },
        ],
      },
    ],
  },
};
