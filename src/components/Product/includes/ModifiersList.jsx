import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import { Spoiler } from '../../Spoiler/Spoiler';
import { ReactComponent as Done } from '../../../assets/icons/done.svg';
import { ReactComponent as ErrorIcon } from '../../../assets/icons/error.svg';
import { AdditiveList } from '../../AdditiveList/AdditiveList';

export const Modifiers = (props) => {
  const {
    modifiers = [],
    onChange,
    onAllChosenChange,
    classes,
  } = props;

  // нужен для определения выбранны ли все обязательные модификаторы
  const [doneList, setDoneList] = useState(modifiers.map((item) => ({ groupName: item.groupName, done: false, required: item.count })));
  // все модификаторы выбраны
  const [allChosen, setAllChosen] = useState(false);
  // свписок всех модификаторов для передачи родителю
  const [allAdditives, setAllAdditives] = useState([]);
  const [additivePriceSum, setAdditivePriceSum] = useState(0);

  const additiveChangeHandler = (data, { groupName, sum }, chosenMax) => {
    // нужен для определения выбранны ли все обязательные модификаторы
    setDoneList((prevState) => prevState.map((group) => {
      if (groupName === group.groupName && chosenMax) {
        return { ...group, groupName: group.groupName, done: true };
      }
      if (groupName === group.groupName && !chosenMax) {
        return { ...group, groupName: group.groupName, done: false };
      }
      return group;
    }));
    // объект вида { [id добавки]: { val, price, name }}
    setAllAdditives((prevState) => ({
      ...prevState,
      ...data,
    }));
  };

  useEffect(() => {
    setAllChosen(
      doneList.filter((item) => item.done && item.required).length
      === modifiers.filter((x) => x.count).length,
    );
  }, [doneList]);

  useEffect(() => {
    // считает сумму стоимости всех добавок
    setAdditivePriceSum(
      Object.keys(allAdditives)
        .reduce((acc, curr) => acc + Math.floor(allAdditives[curr].price), 0),
    );
  }, [allAdditives]);

  useEffect(() => {
    if (onAllChosenChange) onAllChosenChange(allChosen);
  }, [allChosen]);

  useEffect(() => {
    if (onChange) onChange(allAdditives, additivePriceSum);
  }, [allAdditives]);

  const spoilerClasses = cn(`product__spoiler ${classes || ''}`, {});

  const getChooseMoreString = (done, maxPieces, maxItems) => {
    const chosenAmount = Object.values(allAdditives)
      .filter((item) => item.val && item.groupName === done.groupName)
      .reduce((acc, curr) => acc + curr.val, 0);

    if (done.done) {
      return null;
    }
    if (chosenAmount === 0) {
      return maxPieces ? `Выберите до ${maxItems} отдельных глазировок для ${maxPieces} кусочков` : `${maxItems} на выбор`;
    }
    return maxPieces ? `Выберите до ${maxItems} отдельных глазировок для ${maxPieces} кусочков` : `Олсталось выбрать ${maxItems - chosenAmount}`;
  };

  return (
    <>
      {modifiers.map(({
        groupName, groupItems, count_available: maxItems, count: maxPieces,
      }, index) => (
        <Spoiler
          key={index}
          title={groupName}
          description={[
            ...Object.values(allAdditives)
              .filter((item) => item.val && item.groupName === groupName)
              .map(({ name, val, price }) => `${name}${price ? ` (+${price / val}₽/шт) ` : ''} – ${val}шт.`),
          ]}
          text={getChooseMoreString(doneList[index], maxPieces, maxItems)}
          classes={spoilerClasses}
          Icon={
          (maxPieces
            ? doneList[index].done
            : doneList[index].done)
            ? <Done className="product__icon product__icon_done" />
            : (<ErrorIcon className={`product__icon product__icon_error ${!maxPieces ? 'product__icon_white' : ''}`} />)
        }
        >
          <AdditiveList
            groupName={groupName}
            onChange={additiveChangeHandler}
            maxPieces={maxPieces}
            maxItems={maxItems}
            data={groupItems}
          />
        </Spoiler>
      ))}
    </>
  );
};
