import React, { useState } from 'react';
import cn from 'classnames';
import { Counter } from '../../Counter/Counter';
import { Btn } from '../../Btn/Btn';
import getFormattedPrice from '../../../functions/getFormattedPrice';

export const ProductPrice = (props) => {
  const {
    classes,
    price,
    onClick,
    loading,
    id,
    modifiers,
    comments,
    disabled,
    ...other
  } = props;
  const [count, setCount] = useState(1);

  const blockClasses = cn(`product-price ${classes || ''}`, {});

  const changeCount = (x) => {
    setCount(x);
  };

  return (
    <div className={blockClasses} {...other}>
      <span className="product-price__total">{getFormattedPrice(Math.floor(price) * count)}&#160;&#8381;</span>
      <Counter
        classes="product-price__counter"
        amount={1}
        disabledMinus={count <= 1}
        noClean
        onDecrement={changeCount}
        onIncrement={changeCount}
      />
      <Btn
        onClick={() => onClick({
          id,
          count,
        })}
        disabled={!!disabled}
        loading={loading}
        classes="product-price__add"
        text="В корзину"
        primary
        centred
      />
    </div>
  );
};
