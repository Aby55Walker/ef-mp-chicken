import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import classNames from 'classnames';
import { ReactComponent as IconMessage } from '../../assets/icons/message.svg';
import { Btn } from '../Btn/Btn';

export const Help = (props) => {
  const location = useLocation();
  const {
    list = [],
    type = 1, // 1 - текст, 2 - текст с кнопкой, 3 - список с текстом и кнопкой
    text = '',
    to = `${location.pathname}/side-modal/help`, // сюда можно записать стандартный путь до страницы "Помощь"
    small = false,
    classes = '',
    ...other
  } = props;

  const textClass = classNames(`help__text${classes ? ` ${classes}` : ''}`, {
    help__text_small: small,
  });
  const textBlockClass = classNames('help__text help__text-block', {
    help__text_small: small,
  });
  const blockClass = classNames(`help${classes ? ` ${classes}` : ''}`, {});

  return (
    <>
      {/* Просто текст */}
      {(type === 1 || type === '1') && (
        <p className={textClass} {...other}>
          {text}{' '}
          <Link className="help__link" to={to}>
            Помощь
          </Link>
        </p>
      )}

      {/* текст с кнопкой */}
      {(type === 2 || type === '2') && (
        <p className={textClass} {...other}>
          {text && <span className="help__desk">{text}</span>}
          <Btn
            tag="Link"
            to={to}
            classes="help__link--block"
            centred
            leftIcon={<IconMessage />}
            text="Помощь"
            outlined
          />
        </p>
      )}

      {/* список с текстом и кнопкой */}
      {(type === 3 || type === '3') && (
        <div className={blockClass} {...other}>
          <h2 className="help__title">Помощь</h2>

          <ul className="help__list">
            {list.map((elem, i) => (
              <li key={i} className="help__item">
                <Link className="help__link--item" to={elem.to}>
                  {elem.text}
                </Link>
              </li>
            ))}
          </ul>

          <p className={textBlockClass}>
            {text && <span className="help__desk">{text}</span>}
            <Btn
              tag="Link"
              to={to}
              classes="help__link--block"
              centred
              leftIcon={<IconMessage />}
              text="Помощь"
              outlined
            />
          </p>
        </div>
      )}
    </>
  );
};
