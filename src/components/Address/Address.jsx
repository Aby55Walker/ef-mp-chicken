import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ReactComponent as FlatIcon } from '../../assets/icons/flat.svg';
import { ReactComponent as Edit } from '../../assets/icons/edit.svg';

export const Address = (props) => {
  const {
    buildingIcon = <FlatIcon />,
    text, // описание адреса
    address, // строка улица, дом
    edit = true, // иконка "редактировать"
    classes,
    editLink = '/side-modal/edit-address',
    index,
    ...other
  } = props;

  const { pathname } = useLocation();

  const renderText = () => {
    // если в text передан массив, то выводится: https://prnt.sc/ssc5iu
    // если строка, то: https://prnt.sc/ssc5uf
    if (typeof text === 'object') {
      return text.map((t, i) => (t ? (
        <span key={i} className="address__text">
          {t}
        </span>
      ) : null));
    }

    return <span className="address__text">{text}</span>;
  };

  return (
    <div {...other} className={`address${classes ? ` ${classes}` : ''}`}>
      <div className="address__building">{buildingIcon}</div>

      <div className="address__info">
        <span className="address__street">{address}</span>
        {text && <div className="address__text-wrapper">{renderText()}</div>}
      </div>

      {/* index передается для подстановки данных в EditAddress па параметру */}
      {edit && (
        <Link className="address__edit" to={`${pathname}${editLink}/${index}`}>
          <Edit />
        </Link>
      )}
    </div>
  );
};

Address.propTypes = {
  address: PropTypes.string.isRequired,
  buildingIcon: PropTypes.element.isRequired,
  classes: PropTypes.string,
  edit: PropTypes.bool,
  editLink: PropTypes.string,
  index: PropTypes.number,
  text: PropTypes.oneOfType([PropTypes.array, PropTypes.string]).isRequired,
};
