import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import { Btn } from '../Btn/Btn';
import { couponsPage } from '../../constants/pagelinks';

export const NotAuth = (props) => {
  const {
    classes,
    title = 'Авторизуйтесь, чтобы начать получать купоны',
    text = 'И получите первое персональное предложение, скидку 15% на первый заказ',
    withBtn = true,
    ...other
  } = props;

  const notAuthClasses = cn(`not-auth ${classes || ''}`);

  return (
    <div {...other} className={notAuthClasses}>
      <h3 className="not-auth__title">{title}</h3>
      <p className="not-auth__text">{text}</p>
      {withBtn && (
        <Btn classes="not-auth__tbn" text="Зарегистрироваться или войти" primary tag="Link" to={`${couponsPage}/side-modal/registration`} />
      )}
    </div>
  );
};

NotAuth.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  classes: PropTypes.string,
};
