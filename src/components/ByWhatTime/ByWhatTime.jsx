import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { Dropdown } from '../Dropdown/Dropdown';
import { WithRadio } from '../HOCS/WithRadio';
import { CircleRadioBtn } from '../CircleRadioBtn/CircleRadioBtn';

export const ByWhatTime = (props) => {
  const {
    preorder: { today, tomorrow } = {}, disabledToday, classes = '', register, triggerValidation, onDayChange, onTimeChange, ...other
  } = props;

  const elemClass = classNames(`by-what-time ${classes || ''}`, {});
  const [showInputs, setShowInputs] = useState(false);
  const [data, setData] = useState([]);
  const [days, setDays] = useState([]);
  const [time, setTime] = useState([]);

  const timeToday = today || [];
  const timeTomorrow = tomorrow || [];

  // показыть / скрыть drawdown для Input "К выбранному времени"
  const inputChangeHandler = (value) => {
    setShowInputs(value === 'selectedTime');
  };

  // заменять список времени в dropdown "Время"
  const dayChangeHandler = (value) => {
    if (value === 'today' && timeToday.length) setTime(timeToday);
    else if (value === 'tomorrow' && timeTomorrow.length) setTime(timeTomorrow);

    if (onDayChange) onDayChange(value);
  };

  const timeChangeHandler = (value) => {
    if (onTimeChange) onTimeChange(value);
  };

  // создать и присвоить массивы данных
  useEffect(() => {
    // inputs
    setData([
      { value: 'soon', text: 'Сразу как будет готово', disabled: disabledToday },
      { value: 'selectedTime', text: 'К выбранному времени', disabled: !today && !tomorrow },
    ]);

    // time
    const days = [];
    if (today && !disabledToday) days.push({ text: 'Сегодня', value: 'today' });
    if (tomorrow) days.push({ text: 'Завтра', value: 'tomorrow' });
    setDays(days);

    // time today
    if (today && !disabledToday && timeToday.length) setTime(timeToday);
    else if (tomorrow && timeTomorrow.length) setTime(timeTomorrow);
  }, []);

  return (
    <div {...other} className={elemClass}>
      <h2 className="pickup__title by-what-time__title page-delivery__title">Ко&nbsp;скольки готовить?</h2>

      <WithRadio
        defaultSelected={null}
        Component={CircleRadioBtn}
        list={data}
        register={register}
        name="byWhatTime"
        triggerValidation={triggerValidation}
        classes="by-what-time__radio-group"
        onChange={inputChangeHandler}
      />

      {showInputs && (
        <div className="by-what-time__fluids">
          <Dropdown items={days} label="День" name="day" onChange={dayChangeHandler} initialValue classes="by-what-time__dropdown" />
          <Dropdown items={time} label="Время" name="time" onChange={timeChangeHandler} initialValue classes="by-what-time__dropdown" />
        </div>
      )}
    </div>
  );
};
// );
