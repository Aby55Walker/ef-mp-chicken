export const circleRadioBtns = [
  {
    value: 'soon',
    text: 'Сразу как будет готово',
    prompt: '(~17:30)',
  },
  {
    value: 'selectedTime',
    text: 'К выбранному времени',
  },
]

export const days = [
  {
    text: 'Сегодня',
    value: 'today',
  },
  {
    text: 'Завтра',
    value: 'tomorrow',
  },
]

export const time = [
  {
    text: '18:45-19:00',
    value: '18:45-19:00',
  },
  {
    text: '19:00-19:15',
    value: '19:00-19:15',
  },
  {
    text: '19:15-19:30',
    value: '19:15-19:30',
  },
]
