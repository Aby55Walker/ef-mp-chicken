/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Input } from '../Input/Input';
import { addressRules, useFormParams } from '../../constants/validate';
import { ApartmentInputs } from '../ApartmentInputs/ApartmentInputs';
import { Textarea } from '../Textarea/Textarea';
import { flat, office } from '../../constants/address';
import { WithRadio } from '../HOCS/WithRadio';
import { CircleRadioBtn } from '../CircleRadioBtn/CircleRadioBtn';
import { RadioButton } from '../RadioGroup/includes/RadioButton';
import { intercomList, radioButtons } from '../AddAddress/data';
// import { ByWhatTime } from '../ByWhatTime/ByWhatTime';

const initValues = {
  address: '',
  type: 'flat',
  flat: '',
  floor: '',
  porch: '',
  room: '',
  is_intercom_broken: 'false',
  comment: '',
  houseType: '',
};

export const AddressForm = (props) => {
  const {
    classes = '', onSubmit, children, triggerOnMount = false, initialValues = initValues, initialHouseType = flat,
  } = props;

  const {
    handleSubmit, register, errors, clearError, triggerValidation, formState, setError, unregister, reset,
  } = useForm(useFormParams); // валидация формы

  const [houseType, setHouseType] = useState(initialValues.houseType || radioButtons[0].text);

  const submitHandler = (data, e) => {
    if (houseType === office) {
      data.room = data.flat;
    }
    onSubmit(data);
  };

  useEffect(() => {
    if (triggerOnMount) {
      triggerValidation();
    }
    setHouseType(initialHouseType);
  }, []);

  const changeHandler = (name) => {
    triggerValidation(name);
  };

  const houseTypeChangeHandler = (value) => {
    const houseType = radioButtons.find((btn) => btn.value === value);
    setHouseType(houseType.text);
  };

  return (
    <form className={` ${classes}`} action="" noValidate onSubmit={handleSubmit(submitHandler)}>
      <div className="add-address__section">
        <Input
          ref={register(addressRules)}
          error={errors.address}
          errorText={errors.address && errors.address.message}
          required
          value={initialValues.address}
          onChange={() => changeHandler('address')}
          name="address"
          label="Улица, дом"
          classes="add-address__input"
        />
      </div>

      <div className="add-address__section">
        <WithRadio
          Component={RadioButton}
          list={radioButtons}
          onChange={houseTypeChangeHandler}
          name="type"
          register={register}
          triggerValidation={triggerValidation}
          defaultSelected={initialValues.type}
          wrapperClasses="radio-group"
        />
      </div>

      <div className="add-address__section">
        <ApartmentInputs
          initialValues={{
            flat: initialValues.flat,
            floor: initialValues.floor,
            porch: initialValues.porch,
            room: initialValues.room,
          }}
          houseType={houseType}
          register={register}
          errors={errors}
          clearError={clearError}
          setError={setError}
          reset={reset}
          unregister={unregister}
          triggerValidation={triggerValidation}
        />
      </div>

      <div className="add-address__section add-address__section_large-margin">
        <WithRadio
          Component={CircleRadioBtn}
          list={intercomList}
          name="isIntercomBroken"
          defaultSelected={initialValues?.is_intercom_broken?.toString()}
          register={register}
          triggerValidation={triggerValidation}
        />
      </div>

      <div className="add-address__section">
        <Textarea
          ref={register({ required: false })}
          name="comment"
          initialValue={initialValues.comment}
          placeholder="Комментарий (не обязательно)"
        />
      </div>

      {children.length ? (
        children.map((c, i) => (
          <div key={i} className="add-address__section">
            {/* {React.cloneElement(c, { disabled: i === 0 && !formState.isValid, register, triggerValidation })} */}
            {React.cloneElement(c, { disabled: !formState.isValid, register, triggerValidation })}
          </div>
        ))
      ) : (
        <div className="add-address__section">{React.cloneElement(children, { disabled: !formState.isValid })}</div>
      )}
    </form>
  );
};
