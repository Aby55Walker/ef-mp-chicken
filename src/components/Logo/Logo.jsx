import React from 'react'

export const Logo = (props) => {
  const { classes = '', ...other } = props

  return (
    <div className={`main-logo${classes ? ' ' + classes : ''}`} {...other}>
      <span className='main-logo__text'>#chickenbox</span>
    </div>
  )
}
