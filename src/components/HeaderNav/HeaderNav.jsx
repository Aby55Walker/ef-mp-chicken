import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import {
  couponsPage,
  deliveryTermPage,
  personalAccountPage,
  restaurantsPage,
  promotionsPage,
} from '../../constants/pagelinks'

const navItems = [
  {
    text: 'Купоны',
    link: couponsPage,
  },
  {
    text: 'Рестораны',
    link: restaurantsPage,
  },
  {
    text: 'Условия доставки',
    link: deliveryTermPage,
  },
  {
    text: 'style',
    link: '/style',
  },
  {
    text: 'ЛК',
    link: personalAccountPage,
  },  {
    text: 'Акции',
    link: promotionsPage,
  },
]

export const HeaderNav = () => {
  return (
    <nav className='header-nav'>
      {navItems.map(({ text, link }) => (
        <NavLink
          key={text}
          className='header-nav__item link'
          to={link}
          title={text}
        >
          {text}
        </NavLink>
      ))}
    </nav>
  )
}
