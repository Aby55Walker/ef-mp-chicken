/* eslint-disable camelcase */
import React from 'react';
import { useRouteMatch, useLocation } from 'react-router-dom';
import classNames from 'classnames';
import couponImg from '../../assets/images/coupon-1.png';
import { Btn } from '../Btn/Btn';
import { modalCoupon, modalProduct } from '../../constants/pagelinks';
import getUnit from '../../functions/getUnit';
import getFormattedPrice from '../../functions/getFormattedPrice';

export const CouponCard = (props) => {
  const {
    classes,
    btnText = 'Применить',
    Button = null,
    children,
    // data: { id } = {},
    data: { name } = {},
    data: { image } = {},
    data: { code } = {},
    data: { discount } = {},
    data: { unit } = {},
    data: { price } = {},
    data: { total_price } = {},
    ...other
  } = props;

  const { url } = useRouteMatch();
  const couponClasses = classNames(`coupon-card ${classes || ''}`, {});

  return (
    <div className={couponClasses} {...other}>
      <div className="coupon-card__inner">
        {children || (
          <>
            {/* код */}
            <span className="coupon-card__code">
              <strong>{code}</strong>
            </span>
            {/* картинка */}
            <div className="coupon-card__preview">
              {image && <img className="coupon-card__img" src={couponImg} alt={name || 'купон'} />}
            </div>
            {/* описание */}
            <p className="coupon-card__desc">{name}</p>
            {/* цена */}
            <div className="coupon-card__price">
              {price && <span className="coupon-card__price_old">{getFormattedPrice(price)}₽</span>}
              {total_price && <span className="coupon-card__price_new">{getFormattedPrice(total_price)}₽</span>}
              {discount && <span className="coupon-card__price_new">-{discount}{getUnit(unit)}</span>}
            </div>
          </>
        )}
      </div>

      {Button || (
        <Btn
          text={btnText}
          transparent
          fullWidth
          centred
          tag="Link"
          // to={`${url}${modalCoupon}`}
          to={{
            pathname: `${url}${modalCoupon}`,
            search: `productId=${code}`,
            state: { modalIsOpen: true },
          }}
          classes="coupon-card__btn"
        />
      )}
    </div>
  );
};
