import React from 'react'
import classNames from 'classnames'
import PropTypes, { any } from 'prop-types'
import { Btn } from '../Btn/Btn'
//https://i.ibb.co/tXyN1hY/image-179.png
export const Human = (props) => {
  const { name, position, image, classes, ...other } = props

  const humanClasses = classNames(`human ${classes ? classes : ''}`, {})

  return (
    <div {...other} className={humanClasses}>
      <img className='human__img' src={image} alt={name} />
      <div className='human__info'>
        <span className='human__name'>
          <strong>{name}</strong>
        </span>
        <span className='human__position'>{position}</span>
      </div>
    </div>
  )
}

Human.propTypes = {
  name: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  classes: PropTypes.string,
  other: PropTypes.any,
}
