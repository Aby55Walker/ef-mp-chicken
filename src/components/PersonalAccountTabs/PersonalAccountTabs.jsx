import React from 'react';
import {
  Link,
  useRouteMatch,
  useLocation,
  Redirect,
} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { TabControls } from './include/TabControls/TabControls';
import { TabContent } from './include/TabContent/TabContent';
import { authLogOut } from '../../store/modules/auth/actionCreators';
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd';
import { currentOrderPage, personalAccountPage } from '../../constants/pagelinks';

export const PersonalAccountTabs = (props) => {
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const { pathname } = useLocation();

  const listDefault = [
    {
      link: '/current-order',
      text: 'Текущий заказ',
    },
    {
      link: '/order-history',
      text: 'История заказов',
    },
    {
      link: '/profile',
      text: 'Профиль',
    },
  ];

  const { list = listDefault, children } = props;

  const exit = () => {
    dispatch(authLogOut());
  };

  return (
    <div className="personal-account-tabs">
      <TabControls list={list} path={match.url}>
        <Link
          title="Выход"
          className="tab-controls__link personal-account-tabs__exit"
          to="/"
          onClick={exit}
        >
          Выход
        </Link>
      </TabControls>

      <div className="personal-account-tabs__content-wp">
        {
          // без редиректа при открытии модалок компонент дестроится
          deleteSlashAtTheEnd(pathname) === personalAccountPage ? (
            <Redirect to={currentOrderPage} />
          ) : (
            <TabContent path={match.url} />
          )
        }
        {children}
      </div>
    </div>
  );
};
