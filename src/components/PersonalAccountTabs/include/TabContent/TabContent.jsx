// TabContent
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { useSelector } from 'react-redux';
import { CurrentOrder } from '../../../../modules/CurrentOrder/CurrentOrder';
import { md } from '../../../../constants/breakpoints';
import { OrderHistory } from '../../../../modules/OrderHistory/OrderHistory';
import { Help } from '../../../Help/Help';
import { ProfileContainer } from '../../../../containers/profile/ProfileContainer';
import {
  modalHelpOrder, modalHelp, profilePage, currentOrderPage, personalAccountPage, historyOrderPage,
} from '../../../../constants/pagelinks';

export const TabContent = (props) => {
  const { screenWidth } = useSelector((state) => state.screenSize);

  const {
    children, ...other
  } = props;

  return (
    <Route
      path={personalAccountPage}
      render={() => (
        <Switch>
          <Route path={currentOrderPage}>
            <CurrentOrder classes="tab-content" />
            {screenWidth > md

              ? (
                <Help
                  classes="tab-content__help-block"
                  type={3}
                  to={`${currentOrderPage}${modalHelpOrder}`}
                  text="Хотите отменить заказ?"
                  list={[
                    { to: `${currentOrderPage}${modalHelpOrder}`, text: 'Как изменить адрес доставки?' },
                    { to: `${currentOrderPage}${modalHelpOrder}`, text: 'Заказ долго везут, что делать?' },
                    { to: `${currentOrderPage}${modalHelpOrder}`, text: 'Заказ не соответствует ожиданиям, что делать?' },
                  ]}
                />
              )
              : (
                null
              )}
          </Route>

          <Route path={historyOrderPage}>
            <OrderHistory classes="tab-content" />
          </Route>

          <Route path={profilePage}>
            <ProfileContainer classes="tab-content personal-account-tabs__profile" />
            {screenWidth > md

              ? (
                <Help
                  classes="tab-content__help-block"
                  type={3}
                  to={`${profilePage}${modalHelp}`}
                  text="У меня есть вопрос"
                  list={[
                    { to: `${profilePage}${modalHelp}`, text: 'Как изменить дату рождения?' },
                    { to: `${profilePage}${modalHelp}`, text: 'Как привязать аккаунт к новому телефону?' },
                    { to: `${profilePage}${modalHelp}`, text: 'Могу ли я отписаться от уведомлений на e-mail?' },
                  ]}
                />
              )
              : (
                null
              )}
          </Route>
        </Switch>
      )}
    />
  );
};
