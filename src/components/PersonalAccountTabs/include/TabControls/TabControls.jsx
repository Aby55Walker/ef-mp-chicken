import React, { useState, useRef, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
  useRouteMatch,
  useLocation,
} from 'react-router-dom';

import { useSelector } from 'react-redux';
import { sm } from '../../../../constants/breakpoints';
import deleteSlashAtTheEnd from '../../../../functions/deleteSlashAtTheEnd';

export const TabControls = (props) => {
  const { screenWidth } = useSelector((state) => state.screenSize);
  const location = useLocation();
  const match = useRouteMatch(location.pathname);

  const {
    children, list = [], path = match.url, ...other
  } = props;

  return (
    <Route
      path={path}
      render={() => (
        <div className="tab-controls">
          <ul className="tab-controls__list" {...other}>
            {list.map(({ text, link }, i) => (
              <li className="tab-controls__item" key={i}>
                <NavLink
                  exact
                  title={text}
                  className="tab-controls__link"
                  to={`${deleteSlashAtTheEnd(path)}${link}`}
                >
                  {text}
                </NavLink>
              </li>
            ))}
          </ul>
          {children}
        </div>
      )}
    />
  );
};
