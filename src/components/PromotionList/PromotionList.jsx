// PromotionList
import React from 'react';
import cn from 'classnames';
import { PromotionCard } from '../PromotionCard/PromotionCard';

export const PromotionList = (props) => {
  const { classes, data = [] } = props;

  const listClasses = cn(`promotion-list ${classes || ''}`, { });

  return (
    <>
      {data.length
        ? (
          <div className="promotion-list__wp">
            <ul className={listClasses}>
              {data.map((param, i) => (
                <li key={i}>
                  <PromotionCard
                    classes="promotion-list__card"
                    data={param}
                  />
                </li>
              ))}
            </ul>
          </div>
        )
        : null}
    </>
  );
};
