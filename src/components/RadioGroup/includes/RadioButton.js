import React from 'react';
import classNames from 'classnames';

export const RadioButton = (props) => {
  const {
    value,
    text,
    onChange,
    name,
    register, // из useForm
    active,
    ...other
  } = props;

  const buttonClasses = classNames('radio-button', {
    active,
  });

  const changeHandler = ({ target }) => {
    if (onChange) onChange(target);
  };

  return (
    <label {...other} className={buttonClasses}>
      <input
        className="radio-button__input"
        onChange={changeHandler}
        checked={active}
        name={name}
        value={value}
        ref={register({ required: true })}
        type="radio"
      />
      <span
        className="radio-button__text"
        dangerouslySetInnerHTML={{ __html: text }}
      />
      <span className="radio-button__background" />
    </label>
  );
};
