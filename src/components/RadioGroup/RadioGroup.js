import React, { useState } from 'react'
import { RadioButton } from './includes/RadioButton'

export const RadioGroup = (props) => {
  const {
    name = '',
    buttons = [],
    onChange,
    register, // из useForm
    initialValue,
    ...other
  } = props

  const [selected, setSelected] = useState(initialValue || buttons[0].value)

  const changeHandler = (selectedValue) => {
    setSelected(selectedValue)
    if (onChange) onChange(selectedValue)
  }

  return (
    <div {...other} className='radio-group'>
      {buttons.map(({ value, text }, i) => (
        <RadioButton
          style={{ width: 100 / buttons.length + '%' }}
          selected={selected}
          onChange={changeHandler}
          value={value}
          text={text}
          key={i}
          name={name}
          register={register}
        />
      ))}
    </div>
  )
}
