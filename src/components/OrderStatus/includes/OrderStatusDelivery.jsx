// Delivery

// OrderStatus
import React from 'react';
import { OrderItem } from '../../OrderItem/OrderItem';
import { Statuses } from '../../Statuses/Statuses';
import { Address } from '../../Address/Address';
import { ReactComponent as IconOffice } from '../../../assets/icons/office.svg';
import { AverageDeliveryTime } from './AverageDeliveryTime';

// TODO просто для теста (потом этот массив будем получать по api вместе с другими данными)
const tempDeliveryStatuses = [
  { status: 'Принят', id: 1 },
  { status: 'Готовится на кухне', id: 2 },
  { status: 'Выехал с курьером', id: 3 },
  { status: 'Доставлен', id: 3 },
];

// TODO просто для теста (потом этот массив будем получать по api вместе с другими данными)
const tempDeliveryStatusesPickup = [
  { status: 'Уже забрали', id: 1, foods: ['Клюквенный морс', 'Шефбургер'] },
  { status: 'Заберите в кофе-автомате', id: 2, foods: ['Кофе латте'] },
  { status: 'Заберите на кассе', id: 2, foods: ['Шефбургер с брусникой'] },
  { status: 'Ещё готовится', id: 3, foods: ['Крылышки по-азиатски'] },
];

export const OrderStatusDelivery = (props) => {
  const {
    classes = '', childClasses = '', data, isOrderTypeDelivery, ...other
  } = props;

  return (
    <div className={classes} {...other}>
      <OrderItem
        data={data}
        undated={isOrderTypeDelivery}
        isOrderTypeDelivery={isOrderTypeDelivery}
        light
        classes={`${childClasses} order-status__order`}
        Tag="div"
        showStatus
      />

      <Statuses
        // TODO тут тернарный оператор просто для примера, потом его нужно убрать и передавать обычный массив
        data={
          isOrderTypeDelivery
            ? tempDeliveryStatuses
            : tempDeliveryStatusesPickup
        }
        isOrderTypeDelivery={isOrderTypeDelivery}
        classes={`${childClasses} order-status__statuses`}
      />

      {data.address && (
        <Address
          text="19:30-19:45"
          address={data.address}
          edit={false}
          buildingIcon={<IconOffice />}
          classes={`${childClasses} order-status__address`}
        />
      )}

      {isOrderTypeDelivery && (
        <AverageDeliveryTime
          time="43 мин."
          classes={`${childClasses} order-status__average-time`}
        />
      )}
    </div>
  );
};
