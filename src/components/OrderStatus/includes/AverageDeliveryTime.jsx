import React, { useEffect, useState } from 'react'
import classNames from 'classnames'

import { OrderItem } from '../../OrderItem/OrderItem'
import { Statuses } from '../../Statuses/Statuses'
import { Address } from '../../Address/Address'

export const AverageDeliveryTime = (props) => {
  const { time = '', classes = '', ...other } = props

  const textClasses = classNames(
    `average-delivery-time${classes ? ' ' + classes : ''}`,
    {}
  )

  return (
    <p className={textClasses} {...other}>
      <span className='average-delivery-time__text'>{time}</span>
      <span className='average-delivery-time__text'>
        среднее время доставки
      </span>
    </p>
  )
}
