// OrderStatus
import React from 'react';
import classNames from 'classnames';
import { OrderStatusDelivery } from './includes/OrderStatusDelivery';
import { Btn } from '../Btn/Btn';
import { ReactComponent as IconQR } from '../../assets/icons/qr.svg';

export const OrderStatus = (props) => {
  const {
    classes, data = {}, isOrderTypeDelivery, ...other
  } = props;

  const blockClass = classNames(`order-status ${classes || ''}`, {});

  return (
    <>
      <OrderStatusDelivery
        classes={blockClass}
        data={data}
        isOrderTypeDelivery={isOrderTypeDelivery}
        childClasses="order-status__child"
        {...other}
      />

      {!isOrderTypeDelivery && (
      <Btn
        text="Забрать заказ"
        primary
        centred
        classes="current-order__pick-up"
        leftIcon={<IconQR />}
      />
      )}
    </>
  );
};
