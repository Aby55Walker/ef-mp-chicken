import React from 'react'
import { Footer } from '../Footer/Footer'
import { Header } from '../Header/Header'

import { useSelector } from 'react-redux'
import classNames from 'classnames'

export const Layout = ({ children }) => {
  const store = useSelector((state) => state)

  const pageClass = classNames(`page`, { 'page--slide': store.showSlideModal })

  return (
    <div className={pageClass}>
      <Header />
      <main className='page__content'>{children}</main>
      <Footer />
    </div>
  )
}
