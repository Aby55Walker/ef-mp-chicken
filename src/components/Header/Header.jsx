import React, { useEffect, useState } from 'react'
import { Logo } from '../Logo/Logo'
import { HeaderNav } from '../HeaderNav/HeaderNav'
import { Link, useLocation } from 'react-router-dom'
import { ReactComponent as SmileWhite } from '../../assets/icons/smile_white.svg'
import { ReactComponent as Burger } from '../../assets/icons/burger.svg'
import { useSelector } from 'react-redux'
import { OrderType } from '../OrderType/OrderType'
import { CityPicker } from '../CityPicker/CityPicker'
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd'

export const Header = () => {
  const location = useLocation()

  const { loggedIn } = useSelector((state) => state.auth)
  const [profileLink, setProfileLink] = useState('')

  useEffect(() => {
    const path =
      location.pathname === '/'
        ? `side-modal/registration`
        : `/side-modal/registration`
    setProfileLink(
      loggedIn ? `/personal-account` : `${location.pathname}${path}`
    )
  }, [location])

  return (
    <header className='header'>
      <div className='container'>
        <div className='header__inner-wrapper'>
          <Link to='/' className='header__logo'>
            <Logo />
          </Link>
          <CityPicker classes={'header__city-picker'} />
          <div className='header__nav'>
            <HeaderNav />
          </div>

          <OrderType classes={'header__order-type'} />

          <Link to={profileLink} className='header__btn header__login-btn'>
            <SmileWhite />
          </Link>
          <Link
            to={`${deleteSlashAtTheEnd(location.pathname)}/side-modal/burger`}
            className='header__btn header__burger-btn'
          >
            <Burger />
          </Link>
        </div>
      </div>
    </header>
  )
}
