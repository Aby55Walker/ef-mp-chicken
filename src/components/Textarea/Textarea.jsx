import React, { useState } from 'react';

export const Textarea = React.forwardRef((props, ref) => {
  // export const Textarea = (props) => {
  const {
    placeholder,
    register,
    initialValue = '',
    onChange,
    classes = '',
    classesWp = '',
    error = false, // значение введено с ошибкой
    errorText = '', // сообщение об ошибке
    required,
    name,
    ...other
  } = props;

  const [value, setValue] = useState('');

  const changeHandler = ({ target }) => {
    setValue(target.value);
    if (onChange) onChange(target.value);
  };

  return (
    <>
      <div className={`textarea-wp ${classesWp || ''}`}>
        <textarea
          required={required}
          className={`textarea ${classes || ''}`}
          placeholder={placeholder}
          name={name}
          value={value || initialValue}
          onChange={changeHandler}
          ref={ref}
          // ref={register({ required: false })}
          {...other}
        />

        {/* текст ошибки */}
        {error && errorText && (
        <span
          className="input__text input__error"
          dangerouslySetInnerHTML={{ __html: errorText }}
        />
        )}

      </div>
    </>
  );
});
