import React from 'react';
import { CommonModal } from '../CommonModal/CommonModal';
import { CartContainer } from '../../containers/cart/CartContainer';

export const CartModal = () => (
  <CommonModal>
    <CartContainer isSticky={false} mainClass="cart-modal" />
  </CommonModal>
);
