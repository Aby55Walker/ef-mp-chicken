import React from 'react';
import { Coupons } from '../../modules/Coupons/Coupons';
import { ProductCategory } from '../ProductCategory/ProductCategory';

export const ProductCategoryList = React.forwardRef((props) => {
  const { data = [], refs = [] } = props;

  return (
    <div className="product-category-list">
      {data.map((category, i) => {
        if (i === 0) {
          return (
            <React.Fragment key={category.slug}>
              <ProductCategory
                data={category}
                ref={refs[i]}
                id={category.name}
              />
              <Coupons />
            </React.Fragment>
          );
        }
        return (
          <ProductCategory data={category} ref={refs[i]} key={category.slug} id={category.name} />
        );
      })}
    </div>
  );
});
