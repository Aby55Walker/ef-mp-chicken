/* eslint-disable camelcase */
import React from 'react';
import classNames from 'classnames';
import { ReactComponent as Delivery } from '../../assets/icons/delivery.svg';
import { ReactComponent as TakeIn } from '../../assets/icons/take_in.svg';
// функции
import checkYesterday from '../../functions/isYesterday';
import getFormattedPrice from '../../functions/getFormattedPrice';

// Элемент в списке заказов
// Содержит информацию о заказе: id, дата, стоимость, самовывоз/доставка
export const OrderItem = (props) => {
  const {
    data,
    date = data?.date && data.date.split(' ')[0],
    time = data?.date && data.date.split(' ')[1].slice(0, -3),
    id = data?.id,
    amount = data?.amount && getFormattedPrice(data.amount),
    status = data?.status?.name,
    isOrderTypeDelivery,
    showStatus = false,
    undated = false,
    classes = '',
    light = false,
    Tag = 'li',
    ...other
  } = props;

  const currentDate = date;
  const isYesterday = currentDate ? checkYesterday(currentDate.split(' ')[0].split('.').reverse().join('-')) : false;

  const iconClass = classNames('order-item__icon', {
    'order-item__icon_light': light,
    'order-item__icon_delivery': isOrderTypeDelivery,
  });

  return (
    <Tag className={`order-item${classes ? ` ${classes}` : ''}`} {...other}>
      {/* Иконка. Может быть 2 видов: самовывоз, доставка */}
      <div className={iconClass}>
        {isOrderTypeDelivery ? <Delivery /> : <TakeIn />}
      </div>

      <div className="order-item__inner">
        {/* Название заказа */}
        <h3 className="order-item__title">
          Заказ {isOrderTypeDelivery ? 'D' : 'S'}-{id}
        </h3>

        {/* Дата и время заказа */}
        <span className="order-item__full-date">
          {!undated && (
            <span className="order-item__date">
              {isYesterday ? 'Вчера' : date}
            </span>
          )}
          {time}
        </span>
      </div>

      {/* Стоимость заказа */}
      <div className="order-item__price-wp">
        <strong className="order-item__price">{amount}&#160;&#8381;</strong>
        {status && showStatus && (
          <span className="order-item__status">{status}</span>
        )}
      </div>
    </Tag>
  );
};
