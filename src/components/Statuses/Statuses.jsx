import React from 'react';
import classNames from 'classnames';

import { Status } from './includes/Status';

export const Statuses = (props) => {
  const {
    data = [], classes = '', isOrderTypeDelivery, ...other
  } = props;

  const statusesClasses = classNames(`statuses${classes || ''}`, {});

  return (
    <ul className={statusesClasses} {...other}>
      {data.map((data, index) => (
        <Status data={data} key={index} isOrderTypeDelivery={isOrderTypeDelivery} />
      ))}
    </ul>
  );
};
