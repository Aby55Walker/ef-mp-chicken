// Status

import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
// иконки
import { ReactComponent as IconDone } from '../../../assets/icons/done.svg';
import { ReactComponent as IconProcess } from '../../../assets/icons/process.svg';
import { ReactComponent as IconFuture } from '../../../assets/icons/future.svg';
import { ReactComponent as IconCancel } from '../../../assets/icons/cancel.svg';

/*
 * Статусы заказа (для примера, когда подключим api узнаем как они обозначаются)
 * 1 - done
 * 2 - process
 * 3 - future
 * 4 - cancel
 */

export const Status = (props) => {
  const {
    data = {},
    id = data?.id,
    status = data?.status,
    foods = data?.foods || [],
    classes = '',
    isOrderTypeDelivery,
    ...other
  } = props;

  const done = id === 1;
  const process = id === 2;
  const cancel = id === 4;

  const statusName = done || process ? 'primary' : cancel ? 'cancel' : '';
  const Icon = done
    ? IconDone
    : process
      ? IconProcess
      : cancel
        ? IconCancel
        : IconFuture;

  const statusClasses = classNames(`status ${classes || ''}`, {
    'status--delivery': isOrderTypeDelivery,
  });
  const textClasses = classNames('status__text-wrap', {});
  const statusNameClasses = classNames(
    `status__name${statusName ? ` status__name_${statusName}` : ''}`,
    {},
  );
  const lineClasses = classNames(
    `status__line${statusName ? ` status__line_${statusName}` : ''}`,
    {},
  );
  const iconClasses = classNames(
    `status__icon${statusName ? ` status__icon_${statusName}` : ''}`,
    {},
  );
  const foodClasses = classNames(
    `status__food${statusName ? ` status__food_${statusName}` : ''}`,
    { status__food_done: done },
  );

  return (
    <li className={statusClasses} {...other}>
      <div className="status__graphic-content">
        <Icon className={iconClasses} />
        <div className={lineClasses} />
      </div>

      <p className={textClasses}>
        <span className={statusNameClasses}>{status}</span>
        {foods.map((text, i) => (
          <span key={i} className={foodClasses}>
            {text}
          </span>
        ))}
      </p>
    </li>
  );
};
