import React from 'react'
import { NavLink } from 'react-router-dom'
import { FooterLinkList } from './FooterLinkList'
import { FooterSocial } from './FooterSocial'
import { FooterBottom } from './includes/FooterBottom'

const footerLinks = [
  {
    text: 'О сети',
    id: 1,
    link: '/',
  },
  {
    text: 'Контакты',
    id: 2,
    link: '/pages/contacts',
  },
  {
    text: 'Новости',
    id: 3,
    link: '/',
  },
  {
    text: 'Вакансии',
    id: 4,
    link: '/',
  },
  {
    text: 'Франшиза',
    id: 5,
    link: '/',
  },
  {
    text: 'Обратная связь',
    id: 6,
    link: '/',
  },
]

export const Footer = () => {
  return (
    <footer className='footer'>
      <div className='container footer__container'>
        <div className='footer__nav'>
          <FooterLinkList list={footerLinks} />
          <FooterSocial className='footer__social' />
        </div>
      </div>
      <FooterBottom />
    </footer>
  )
}
