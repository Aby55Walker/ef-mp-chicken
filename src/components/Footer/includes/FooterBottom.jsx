import React from 'react'
import { Copyrights } from '../../Copyrights/Copyrights'

export const FooterBottom = () => {
  return (
    <div className='container footer__container'>
      <Copyrights classes={'footer__copyrights'} />
    </div>
  )
}
