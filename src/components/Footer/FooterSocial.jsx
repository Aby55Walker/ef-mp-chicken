import React from 'react'
import { ReactComponent as Vk } from '../../assets/icons/vk.svg'
import { ReactComponent as Facebook } from '../../assets/icons/facebook.svg'
import { ReactComponent as Instagram } from '../../assets/icons/instagram.svg'

export const FooterSocial = ({ className }) => {
  const icons = [
    {
      Icon: Vk,
      title: 'vk',
    },
    {
      Icon: Facebook,
      title: 'facebook',
    },
    {
      Icon: Instagram,
      title: 'instagram',
    },
  ]

  return (
    <div className={className}>
      <div className='footer-social'>
        {icons.map(({ title, Icon }) => {
          return (
            <a
              href='#'
              key={title}
              className='footer-social__link link'
              title={title}
            >
              <Icon />
            </a>
          )
        })}
      </div>
    </div>
  )
}
