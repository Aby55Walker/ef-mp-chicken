import { NavLink } from 'react-router-dom'
import React from 'react'

export const FooterLinkList = ({ list }) => {
  return list.map(({ text, link, id }) => {
    return (
      <NavLink key={id} title={text} className='link-list__link link' to={link}>
        {text}
      </NavLink>
    )
  })
}
