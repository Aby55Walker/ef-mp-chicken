import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import LoaderPrimary from '../../assets/icons/loader_primary.png';
import LoaderSecondary from '../../assets/icons/loader_secondary.png';

export const Btn = (props) => {
  const {
    text, // btn text
    disabled,
    primary, // primary theme
    outlined, // outlined theme
    transparent, // transparent theme
    loading,
    classes = '',
    leftIcon,
    centred, // текст по центру
    rightIcon,
    fullWidth,
    tag = 'button',
    error, // делает кнопку красной
    ...other
  } = props;

  const btnClass = classNames(`btn ${classes || ''}`, {
    btn_primary: primary,
    btn_disabled: disabled,
    btn_outlined: outlined,
    btn_transparent: transparent,
    'btn_full-width': fullWidth,
    btn_error: error,
  });

  const contentClass = classNames('btn__content', {
    btn__content_centred: centred,
  });

  // оборачивает кнопку в Link или нативный тег
  const Tag = tag === 'Link' ? Link : `${tag}`;

  return (
    <Tag {...other} disabled={disabled || loading} className={btnClass}>
      {loading && (
        <img
          alt="spinner"
          src={primary ? LoaderPrimary : LoaderSecondary}
          className="btn__loader"
        />
      )}
      <span className={contentClass}>
        {leftIcon && <span className="btn__left-icon">{leftIcon}</span>}
        <span
          className="btn__text"
          dangerouslySetInnerHTML={{ __html: text }}
        />
        {rightIcon && <span className="btn__right-icon">{rightIcon}</span>}
      </span>
    </Tag>
  );
};

Btn.propTypes = {
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  primary: PropTypes.bool,
  outlined: PropTypes.bool,
  transparent: PropTypes.bool,
  loading: PropTypes.bool,
  fullWidth: PropTypes.bool,
  leftIcon: PropTypes.any,
  centred: PropTypes.bool,
  rightIcon: PropTypes.any,
  tag: PropTypes.string,
  error: PropTypes.bool,
};
