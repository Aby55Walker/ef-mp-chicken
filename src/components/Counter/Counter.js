import React, { useEffect, useState } from 'react';
import { ReactComponent as Plus } from '../../assets/icons/plus.svg';
import { ReactComponent as Minus } from '../../assets/icons/minus.svg';
import { ReactComponent as Clear } from '../../assets/icons/clear.svg';

export const Counter = ({
  amount = 0,
  globalValue = null,
  onDecrement,
  onIncrement,
  disabledPlus,
  disabledMinus,
  classes,
  stateFull = true, // компонент сам управляет своим состоянием
  noClean, // отключить показ кнопки очистки (крестик)
}) => {
  const [count, setCount] = useState(amount);

  const handleDecrement = () => {
    if (count > 0) {
      if (stateFull) setCount(count - 1);
      if (onDecrement) onDecrement(count - 1);
    }
  };

  const handleIncrement = () => {
    if (stateFull) setCount(count + 1);
    if (onIncrement) onIncrement(count + 1);
  };

  const handleKeyDown = (e) => {
    e.preventDefault();
    const { key } = e;

    if (key === 'ArrowRight' || key === 'ArrowUp') {
      handleIncrement();
    } else if (key === 'ArrowLeft' || key === 'ArrowDown') {
      handleDecrement();
    }
  };

  useEffect(() => {
    if (stateFull) return;
    setCount(amount);
  }, [amount]);

  return (
    <div className={`counter ${classes || ''}`} onKeyDown={handleKeyDown}>
      <button
        disabled={(globalValue !== null ? globalValue === 0 : count === 0) || disabledMinus}
        onClick={handleDecrement}
        className="counter__btn counter__minus"
      >
        {(globalValue === 1 || count === 1) && !noClean ? <Clear /> : <Minus />}
      </button>

      <span className="counter__amount">{globalValue || count}</span>

      <button
        disabled={disabledPlus}
        onClick={handleIncrement}
        className="counter__btn counter__plus"
      >
        <Plus />
      </button>
    </div>
  );
};
