import React, { useEffect, useState } from 'react';
import {
  useLocation, useRouteMatch, Route, Switch,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { SideModal } from '../SideModal/SideModal';
import { ProductContainer } from '../../containers/catalog/ProductContainer';
import { modalProduct } from '../../constants/pagelinks';
import { ProductInnerChoiceContainer } from '../../containers/catalog/ProductInnerChoiceContainer';
import { productRequest, productReset } from '../../store/modules/product/actions';
import {
  innerProductReset,
} from '../../store/modules/product/innerProduct/actions';

export const ProductModal = () => {
  const { pathname } = useLocation();
  const { url } = useRouteMatch(`*${modalProduct}`);
  const dispatch = useDispatch();

  useEffect(() => () => {
    dispatch(productReset());
    dispatch(innerProductReset());
  }, []);

  const exitedHandler = () => {
    dispatch(productReset());
  };

  return (
    <SideModal onExited={exitedHandler} swipeOnMobile internal={pathname !== url}>
      <Switch>
        <Route
          path={`${url}/choice`}
          render={() => (
            <ProductInnerChoiceContainer />
          )}
        />
      </Switch>

      <ProductContainer hidden={pathname !== url} />
    </SideModal>
  );
};
