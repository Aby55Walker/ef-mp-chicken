import React, { useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import { ReactComponent as Close } from '../../assets/icons/clear.svg'
import { useHistory, useLocation } from 'react-router-dom'
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock'
import { Transition } from '../Transition/Transition'

export const Modal = (props) => {
  const { contentClasses = '', children, classes, ...other } = props
  const history = useHistory()
  const [modalIsOpen, setModalIsOpen] = useState(true)

  useEffect(() => {
    disableBodyScroll(null)
    return () => {
      clearAllBodyScrollLocks()
      closeModal()
    }
  }, [])

  const closeModal = () => setModalIsOpen(false)

  return createPortal(
    <Transition
      closeMethod={() => history.goBack()}
      toggle={modalIsOpen}
      classNames='fade'
    >
      <div className={`modal ${classes || ''}`} {...other}>
        <div className='modal__overlay' onClick={closeModal} />
        <div
          className={`modal__content${
            contentClasses ? ' ' + contentClasses : ''
          }`}
        >
          <Close className='modal__close' onClick={closeModal} />
          {children}
        </div>
      </div>
    </Transition>,
    document.getElementById('modal-root')
  )
}
