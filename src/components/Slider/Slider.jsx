import React, { useEffect, useState } from 'react';
import SwiperCore, { EffectCoverflow } from 'swiper';
import PropTypes from 'prop-types';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useSelector } from 'react-redux';

SwiperCore.use([EffectCoverflow]);

const defaultSettings = {
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: 'auto',
  spaceBetween: 30,
  coverflowEffect: {
    rotate: 20,
    stretch: 0,
    depth: 200,
    modifier: 1,
    slideShadows: true,
  },
  breakpoints: {
    1025: {
      mousewheel: true,
      spaceBetween: 10,
      slidesPerView: 'auto',
      freeMode: true,
      effect: 'slide',
      centeredSlides: false,
      coverflowEffect: null,
    },
  },
};

export const Slider = React.forwardRef((props, ref) => {
  const {
    settings = defaultSettings,
    children,
    sliderClasses = 'slider',
    slideClasses = 'slider__item',
    prevButton,
    nextButton,
    onSlideChange,
    onInit,
    loading,
    ...other
  } = props;

  const [swiper, setSwiper] = useState(null);
  const [progress, setProgress] = useState(0);
  const { screenWidth } = useSelector((state) => state.screenSize);

  const initHandler = (swiper) => {
    setSwiper(swiper);
    if (onInit) onInit(swiper);
  };

  const nextSlideHandler = () => {
    if (swiper) {
      swiper.slideNext();
      setProgress(swiper.progress);
    }
  };
  const prevSlideHandler = () => {
    if (swiper) {
      swiper.slidePrev();
      setProgress(swiper.progress);
    }
  };

  const slideChangeHandler = (swiper) => {
    if (onSlideChange) onSlideChange(swiper.activeIndex);
    setProgress(swiper.progress);
  };

  useEffect(() => {
    if (swiper) {
      swiper.update();
    }
  }, [screenWidth]);

  return (
    <Swiper
      {...settings}
      {...other}
      ref={ref}
      onInit={initHandler}
      className={sliderClasses}
      onSlideChange={slideChangeHandler}
    >
      {
        // 1 или несколько детей
        children.length
          ? children.map((item, i) => (
            <SwiperSlide className={slideClasses} key={i}>
              {item}
            </SwiperSlide>
          ))
          : children
      }

      {prevButton
        && prevButton({
          onClick: prevSlideHandler,
          disabled: progress <= 0,
        })}
      {nextButton
        && nextButton({
          onClick: nextSlideHandler,
          disabled: progress >= 1,
        })}
    </Swiper>
  );
});

Slider.propTypes = {
  settings: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  sliderClasses: PropTypes.string,
  slideClasses: PropTypes.string,
  prevButton: PropTypes.any,
  nextButton: PropTypes.any,
};
