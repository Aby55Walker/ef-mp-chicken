// RegistrationData

import React, { useState, useRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Btn } from '../Btn/Btn';
import { Input } from '../Input/Input';
import { Checkbox } from '../Checkbox/Checkbox';
import {
  useFormParams,
  nameRules,
  emailRules,
  telRules,
  dateRules,
} from '../../constants/validate';
import { ReactComponent as IconCalendar } from '../../assets/icons/calendar.svg';

const defaultValues = {
  phone: '',
  firstName: '',
  lastName: '',
  birthday: '',
  email: '',
  isAcceptNotifications: true,
};

export const RegistrationData = (props) => {
  const { values = defaultValues, onSubmit, ...other } = props;

  const refForm = useRef();
  const {
    handleSubmit,
    register,
    errors,
    triggerValidation,
    clearError,
    formState,
  } = useForm(useFormParams); // валидация формы

  useEffect(() => {
    getToInputByIndex(); // поставить фокус на первый input
  }, []);

  const onChange = (name) => {
    clearError([name]);
    triggerValidation(name);
  };

  const submitHandler = (data, e) => {
    // console.log('RegistrationData onSubmit =>', data);
    if (onSubmit) onSubmit(data);
  };

  const onClear = (name) => {
    clearError([name]);
  };

  // перейти к input по его индексу
  const getToInputByIndex = (index = 0) => {
    const elem = refForm.current.elements[index];
    if (elem) elem.focus();
  };

  return (
    <div className="registration-data" {...other}>
      <h2 className="side-modal__title">Профиль</h2>
      <p className="side-modal__text">
        Похоже, вы&nbsp;у&nbsp;нас впервые. Заполните свой профиль, чтобы
        завершить регистрацию.
      </p>
      <b className="side-modal__subtitle">Личные данные</b>

      <form
        onSubmit={handleSubmit(submitHandler)}
        className="registration-data__form"
        ref={refForm}
      >
        <Input
          ref={register(nameRules)}
          error={errors?.first_name}
          errorText={errors?.first_name?.message}
          value={values.firstName}
          name="first_name"
          label="Имя"
          classes="registration-data__input registration-data__first_name"
          onChange={() => onChange('first_name')}
          onClear={() => onClear('first_name')}
        />

        <Input
          ref={register(nameRules)}
          error={errors?.last_name}
          errorText={errors?.last_name?.message}
          name="last_name"
          value={values.lastName}
          label="Фамилия"
          classes="registration-data__input registration-data__last_name"
          onChange={() => onChange('last_name')}
          onClear={() => onClear('last_name')}
        />

        <Input
          ref={register(telRules)}
          value={values.phone}
          mask="tel"
          type="tel"
          error={errors?.phone}
          errorText={errors?.phone?.message}
          name="phone"
          label="Телефон"
          classes="registration-data__input registration-data__phone"
          onChange={() => onChange('phone')}
          onClear={() => onClear('phone')}
        />

        <Input
          ref={register(dateRules)}
          label="Дата рождения"
          mask="date"
          name="birthday"
          value={values.birthday}
          error={errors?.birthday}
          errorText={errors?.birthday?.message}
          icon={<IconCalendar />}
          classes="registration-data__input registration-data__birthday"
          onChange={() => onChange('birthday')}
        />

        <Input
          ref={register(emailRules)}
          name="email"
          value={values.email}
          error={errors?.email}
          errorText={errors?.email?.message}
          label="E-mail"
          classes="registration-data__input registration-data__email"
          onChange={() => onChange('email')}
          onClear={() => onClear('email')}
        />

        <b className="side-modal__subtitle">Уведомления</b>

        <Checkbox
          register={register({ required: false })}
          name="is_accept_notifications"
          text={
            'Даю согласие на&nbsp;получение СМС и&nbsp;e&#8209;mail уведомлений'
          }
          checked={values.isAcceptNotifications}
          classes="registration-data__checkbox"
        />
      </form>

      <Btn
        classes="registration-data__btn"
        text="Зарегистрироваться"
        primary
        centred
        fullWidth
        disabled={!formState.isValid}
        onClick={handleSubmit(submitHandler)}
      />
    </div>
  );
};
