// HelpModal
import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { SideModal } from '../SideModal/SideModal';
import { HelpProblemsContainer } from '../../containers/help/HelpProblemsContainer';
import { HelpOrderProblemsContainer } from '../../containers/help/HelpOrderProblemsContainer';
import { HelpSiteProblemsContainer } from '../../containers/help/HelpSiteProblemsContainer';
import { HelpOrderFormContainer } from '../../containers/help/HelpOrderFormContainer';
import { modalHelp } from '../../constants/pagelinks';

export const HelpModal = () => {
  const match = useRouteMatch(`*${modalHelp}`);

  return (
    <SideModal>
      <Switch>
        <Route exact path={`${match.url}/`}>
          <HelpProblemsContainer />
        </Route>

        <Route exact path={`${match.url}/order-questions`}>
          <HelpOrderProblemsContainer />
        </Route>

        <Route exact path={`${match.url}/site-questions`}>
          <HelpSiteProblemsContainer />
        </Route>

        <Route exact path={`${match.url}/order-form`}>
          <HelpOrderFormContainer />
        </Route>
      </Switch>
    </SideModal>
  );
};
