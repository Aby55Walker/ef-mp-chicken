/* eslint-disable camelcase */
import React, { useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Input } from '../../Input/Input';
import { Textarea } from '../../Textarea/Textarea';
import { Btn } from '../../Btn/Btn';
import {
  telRules, nameRules, useFormParams,
} from '../../../constants/validate';
import { lg } from '../../../constants/breakpoints';
import { modalAnimationDuration } from '../../../constants/animation';

export const HelpOrderForm = (props) => {
  const {
    onClick,
    onSubmit,
    ...other
  } = props;

  const refForm = useRef();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const first_name = useSelector((state) => state.user?.data?.first_name);
  const phone = useSelector((state) => state.user?.data?.phone);
  const authPhone = useSelector((state) => state.auth.phone);

  const {
    handleSubmit,
    register,
    errors,
    triggerValidation,
    clearError,
    formState,
  } = useForm(useFormParams); // валидация формы

  useEffect(() => {
    triggerValidation(); // fix бага с input tel
    setTimeout(() => {
      if (refForm
          && refForm.current
          && refForm.current.elements
          && refForm.current.elements[0]) refForm.current.elements[0].focus();
    }, modalAnimationDuration);
  }, []);

  const submitHandler = (data) => {
    if (onSubmit) onSubmit(data);
  };

  const onChange = (e, name) => {
    clearError([name]);
    triggerValidation(name);
  };

  const onClear = (name) => {
    clearError([name]);
  };

  return (
    <form ref={refForm} className="help-order-form" onSubmit={handleSubmit(submitHandler)} {...other}>

      <h3
        className="help-problems__title help-order-form__title"
        dangerouslySetInnerHTML={{ __html: 'Укажите свою причину' }}
      />

      {screenWidth <= lg
        ? (
          <Input
            ref={register(nameRules)}
            name="comment"
            error={errors?.comment}
            errorText={errors?.comment?.message}
            label="Ваш ответ"
            classes="help-order-form__input"
            onChange={(e) => onChange(e, 'comment')}
            onClear={() => onClear('comment')}
          />
        )
        : (
          <Textarea
            ref={register(nameRules)}
            name="comment"
            classesWp="help-order-form__textarea-wp"
            classes="help-order-form__textarea"
            error={errors?.comment}
            errorText={errors?.comment?.message}
            placeholder="Ваш ответ"
          />
        )}

      <Input
        ref={register(nameRules)}
          // eslint-disable-next-line camelcase
        value={first_name || ''}
        name="firstName"
        error={errors?.firstName}
        errorText={errors?.firstName?.message}
        label="Ваше имя"
        classes="help-order-form__input"
        onChange={(e) => onChange(e, 'firstName')}
        onClear={() => onClear('firstName')}
      />

      <Input
        ref={register(telRules)}
        value={phone || authPhone || ''}
        mask="tel"
        name="phone"
        error={errors?.phone}
        errorText={errors?.phone?.message}
        label="Телефон"
        classes="help-order-form__input"
        onChange={(e) => onChange(e, 'phone')}
        onClear={() => onClear('phone')}
        helperText="Перезвоним в течении 10 минут"
      />

      <Btn
        text="Отправить"
        primary
        disabled={!formState.isValid}
        type="submit"
        classes="help-order-form__btn"
        centred
        fullWidth
      />
    </form>
  );
};
