import React from 'react';
import { Link } from 'react-router-dom';

export const HelpProblems = (props) => {
  const {
    title = '',
    data = [],
    onClick,
    path = '/',
    ...other
  } = props;

  const clickHandle = (data) => {
    if (onClick) onClick(data);
  };

  return (
    <div className="help-problems" {...other}>
      <h3 className="help-problems__title" dangerouslySetInnerHTML={{ __html: title }} />

      {data && data.length
        ? (
          <ul className="help-problems__list">
            {data.map((data, i) => (
              <li key={i} className="help-problems__item">
                <Link
                  to={data.to || path}
                  className="help-problems__link"
                  onClick={() => clickHandle(data)}
                >
                  {typeof data === 'object' ? data.name : data}
                </Link>
              </li>
            ))}
          </ul>
        ) : null}
    </div>
  );
};
