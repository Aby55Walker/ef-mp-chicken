/* eslint-disable camelcase */
import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import cn from 'classnames';
import { Counter } from '../Counter/Counter';
import Image from '../../assets/images/cart-item-1.png';
import { ReactComponent as Edit } from '../../assets/icons/edit.svg';
import { CartSpoiler } from './includes/CartSpoiler';

export const CartItem = (props) => {
  const {
    classes,
    loading = false,
    onCountChange,
    edit = true,
    data = [],
    title = data?.name,
    id = data?.id,
    quantity = data?.quantity,
    img = data?.pictures?.preview,
    old_price = data?.old_price,
    total_price = data?.total_price,
    discount = data?.discount, // добавил параметр дял примера
    list = data?.list, // добавил для примера
    ...other
  } = props;

  const [count, setCount] = useState(quantity || 1);

  const changeCount = (n) => {
    if (onCountChange) onCountChange(n);
  };

  const { pathname } = useLocation();
  const itemClasses = cn(`cart-item ${classes || ''}`, {});
  const newPriceClasses = cn('cart-item__price_new', {
    'cart-item__price_new_discount': discount,
  });

  return (
    <div {...other} className={itemClasses}>
      <div className="cart-item__img-wp">
        <img className="cart-item__img" src={Image} alt={title} />
      </div>

      <div className="cart-item__content">
        <h4 className="cart-item__title">{title}</h4>

        {list && <CartSpoiler data={list} />}

        <div className="cart-item__footer">
          <Counter
            disabledPlus={(discount && count > 0) || loading}
            disabledMinus={loading}
            stateFull={false}
            amount={quantity}
            onDecrement={changeCount}
            onIncrement={changeCount}
          />

          {/* цена */}
          <div className="cart-item__price">
            {old_price && (
              <span className="cart-item__price_old">
                {old_price.slice(0, -3)}&#160;&#8381;
              </span>
            )}
            {total_price && (
              <span className={newPriceClasses}>
                {total_price.slice(0, -3)}&#160;&#8381;
              </span>
            )}
          </div>
        </div>
      </div>

      {/* Открытие модалки для редактирования товара */}
      {edit && (
        <Link
          className="cart-item__edit"
          to={{
            pathname: `${pathname}/side-modal/product`,
            search: `productId=${id}`,
            state: { modalIsOpen: true },
          }}
        >
          <Edit />
        </Link>
      )}
    </div>
  );
};
