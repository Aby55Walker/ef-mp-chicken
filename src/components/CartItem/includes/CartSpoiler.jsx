import React, { useState, useRef, useEffect } from 'react'
import cn from 'classnames'
import { CSSTransition } from 'react-transition-group'
import { ReactComponent as IconArray } from '../../../assets/icons/chevron_up.svg'

export const CartSpoiler = (props) => {
  const {
    classes,
    data = [],
    onOpenChange,
    onCloseChange,
    minCount = 3, // минимальное к-во строк, нужное для преобразования списка в раскрывающийся блок
    open = data.length < minCount,
    ...other
  } = props

  // state
  const [opened, changeOpen] = useState(open) // {boolean} раскрыт ли блок
  const [height, setHeight] = useState(open ? 'auto' : 0) // {string || number} высота блока
  const [maxHeight, setMaxHeight] = useState(0) // максимальная высота блока
  const refContent = useRef(null) // контент, высоту которого нужно анимировать

  // записать высоту блока после того, как компонент будет создан
  useEffect(() => {
    const currentHeight = refContent.current.scrollHeight // scrollHeight, чтобы получать высоту и для скрытого блока
    setMaxHeight(currentHeight)
    setHeight(opened ? currentHeight : 0)
  }, [])

  // при нажатии на кнопку показать/скрыть блок
  const handleChangeOpen = () => {
    const newValue = !opened

    changeOpen(newValue)
    setHeight(newValue ? maxHeight : 0)
  }

  const openHandle = () => {
    if (onOpenChange) onOpenChange()
  }

  const closeHandle = () => {
    if (onCloseChange) onCloseChange()
  }

  useEffect(() => {
    opened ? openHandle() : closeHandle()
  }, [opened])

  const blockClasses = cn(`cart-spoiler ${classes || ''}`, {})
  const contentClasses = cn(`cart-spoiler__content ${classes || ''}`, {
    'cart-spoiler__toggled-content': data.length >= minCount,
  })

  return (
    <>
      <div {...other} className={blockClasses}>
        {data.length >= minCount && (
          <div
            className={`cart-spoiler__control-open`}
            onClick={handleChangeOpen}
          >
            {data[0] && data[0].isTitle ? (
              <b className={'cart-spoiler__subtitle'}>{data[0].text}</b>
            ) : (
              <span className={'cart-spoiler__text'}>{data[0].text}</span>
            )}
            <IconArray
              className={`cart-spoiler__array${
                opened ? ' cart-spoiler__array_open' : ''
              }`}
            />
          </div>
        )}

        {/* Контент, который можно скрыть */}
        <div
          className={contentClasses}
          ref={refContent}
          style={{ maxHeight: height }}
        >
          <div className={`cart-spoiler__inner`}>
            <ul className={'cart-spoiler__list'}>
              {data.map(({ text, isTitle }, i) => {
                if (i === 0 && data.length >= minCount) return null
                const Tag = isTitle ? 'b' : 'span'
                const textClass = isTitle
                  ? 'cart-spoiler__subtitle'
                  : 'cart-spoiler__text'
                const textWpClass = isTitle
                  ? 'cart-spoiler__subtitle-wp'
                  : 'cart-spoiler__text-wp'

                return (
                  <li key={i} className={textWpClass}>
                    <Tag className={textClass}>{text}</Tag>
                  </li>
                )
              })}
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}
