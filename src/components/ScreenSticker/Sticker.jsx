import React, { useEffect, useState } from 'react';
import { randomSticker } from '../../hooks/useRandomSticker';

export const Sticker = React.forwardRef(({ image, onClick, style }, ref) => {
  const [loaded, setLoaded] = useState(false);
  const [sticker] = useState(randomSticker());

  const clickHandler = () => {
    if (onClick) onClick();
  };

  useEffect(() => {

  }, []);

  return (

    <div onClick={clickHandler} ref={ref} className="sticker" style={style}>
      <img
        className={loaded ? 'loaded' : ''}
        src={image || sticker}
        onLoad={() => setLoaded(true)}
        alt=""
      />
    </div>
  );
});
