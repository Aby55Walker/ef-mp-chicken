import React, { useEffect, useRef, useState } from 'react';
import { randomInteger } from '../../functions/randomInteger';
import { removeStickerTimeout } from '../../constants/stickers';
import { Sticker } from './Sticker';

export const ScreenStickerContainer = ({ sticker, index, onRemove }) => {
  const [style, setStyle] = useState({});
  const stickerRef = useRef();

  useEffect(() => {
    const halfWidth = stickerRef.current.clientHeight / 2;
    const windowHeight = window.innerHeight;
    const windowWidth = window.innerWidth;
    const { scrollTop } = document.documentElement;
    // появляются в рандомной точке экрана
    setStyle({
      transform: `rotate(${randomInteger(0, 360)}deg)`,
      left: `${
        (randomInteger(halfWidth, windowWidth - halfWidth * 2) / windowWidth) *
        100
      }%`,
      top: `${randomInteger(
        halfWidth + scrollTop,
        -halfWidth + scrollTop + windowHeight
      )}px`,
    });

    setTimeout(() => {
      onRemove(index);
    }, removeStickerTimeout);
  }, []);

  if (sticker === null) return null;

  return (
    <Sticker
      onClick={() => onRemove(index)}
      style={style}
      ref={stickerRef}
      image={sticker}
    />
  );
};
