import React, { useEffect, useState } from 'react';
import { addStickerInterval } from '../../constants/stickers';
import { ScreenStickerContainer } from './ScreenStickerContainer';
import { randomSticker } from '../../hooks/useRandomSticker';

export const ScreenStickersContainer = ({ img, ...other }) => {
  const [stickers, setStickers] = useState([]);

  // добавляет стикер
  const addSticker = () => {
    setStickers((prevState) => [...prevState, randomSticker()]);
  };

  // интервал появление стикеров
  useEffect(() => {
    const stickerInterval = setInterval(addSticker, addStickerInterval);

    return () => clearInterval(stickerInterval);
  }, []);

  const removeHandler = (index) => {
    setStickers((prevState) => prevState.map((item, itemIndex) => {
      if (index === itemIndex) return null;
      return item;
    }));
  };

  return (
    <div {...other} className="sticky-sticker">
      {!!stickers.length
        && stickers.map((sticker, i) => (
          <ScreenStickerContainer
            key={i}
            index={i}
            onRemove={removeHandler}
            sticker={sticker}
            alt=""
          />
        ))}
    </div>
  );
};
