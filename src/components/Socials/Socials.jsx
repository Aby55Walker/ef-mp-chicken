import React from 'react'
import classNames from 'classnames'

import { ReactComponent as Vk } from '../../assets/icons/logo_vk_color.svg'
import { ReactComponent as Facebook } from '../../assets/icons/logo_facebook_color.svg'
import { ReactComponent as Instagram } from '../../assets/icons/logo_instagram_color.svg'
import { ReactComponent as Telegram } from '../../assets/icons/logo_telegram_color.svg'
import { ReactComponent as Viber } from '../../assets/icons/logo_viber_color.svg'
import { ReactComponent as Whatsapp } from '../../assets/icons/logo_whatsapp_color.svg'
import { ReactComponent as Twitter } from '../../assets/icons/logo_twitter_color.svg'
import { ReactComponent as Ok } from '../../assets/icons/logo_ok_color.svg'

const socialNames = [
  {
    name: 'viber',
    icon: Viber,
  },
  {
    name: 'whatsapp',
    icon: Whatsapp,
  },
  {
    name: 'telegram',
    icon: Telegram,
  },
  {
    name: 'instagram',
    icon: Instagram,
  },
  {
    name: 'vk',
    icon: Vk,
  },
  {
    name: 'facebook',
    icon: Facebook,
  },
  {
    name: 'twitter',
    icon: Twitter,
  },
  {
    name: 'ok',
    icon: Ok,
  },
]

export const Socials = (props) => {
  const { data, classes, ...other } = props

  const socialClasses = classNames(`socials ${classes ? classes : ''}`, {})

  return (
    <div {...other} className={socialClasses}>
      {data &&
        socialNames.map((item) => {
          if (data[item.name]) {
            return (
              <a
                key={item.name}
                className={'socials__item'}
                href={data[item.name]}
                title={item.name}
                target={'_blank'}
              >
                <item.icon className={'socials__icon'} />
              </a>
            )
          }
        })}
    </div>
  )
}
