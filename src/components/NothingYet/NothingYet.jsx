import React from 'react'
import { Btn } from '../Btn/Btn'
import { Link } from 'react-router-dom'

export const NothingYet = (props) => {
  const {
    classes = '',
    headline = `Пока ничего нет`,
    text = `Но&nbsp;шеф-повар готов взяться за&nbsp;дело в&nbsp;любой момент!`,
    lastOrder = true, // показать ссылку "Посмотреть последний заказ"
    ...other
  } = props

  return (
    <div className={`nothing-yet${classes ? ' ' + classes : ''}`} {...other}>
      <strong
        className='nothing-yet__headline'
        dangerouslySetInnerHTML={{ __html: headline }}
      ></strong>
      <p
        className='nothing-yet__text'
        dangerouslySetInnerHTML={{ __html: text }}
      ></p>

      <div className='nothing-yet__buttons'>
        <Btn
          to={'/'}
          text='Заказать что-нибудь'
          tag='Link'
          centred
          primary
          classes='nothing-yet__btn'
        />
        {lastOrder && (
          <Btn
            to={'/'}
            text='Посмотреть последний заказ'
            tag='Link'
            centred
            transparent
            classes='nothing-yet__btn'
          />
        )}
      </div>
    </div>
  )
}
