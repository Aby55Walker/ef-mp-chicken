import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Masonry from 'react-masonry-component';
import classNames from 'classnames';
import { throttle } from 'lodash';

const defaultOptions = {
  transitionDuration: '0.5s',
  columnWidth: '.masonry-grid__sizer',
  itemSelector: '.masonry-grid__item',
  percentPosition: true,
  horizontalOrder: true,
};

export const MasonryGrid = (props) => {
  const {
    classes = '',
    options = defaultOptions,
    children = [],
    gridItemClasses,
    onLoad,
    ...other
  } = props;

  const gridRef = useRef(null);
  const [layoutComplete, setLayoutComplete] = useState(false);

  const resizeHandler = throttle(() => {
    if (gridRef.current) {
      gridRef.current.masonry.layout();
    }
  }, 500);

  useEffect(() => {
    window.addEventListener('resize', resizeHandler);
    return () => window.removeEventListener('resize', resizeHandler);
  }, []);

  const gridClasses = classNames(`masonry-grid ${classes || ''}`, {
    'masonry-grid_complete': layoutComplete,
  });

  const itemClasses = classNames(`masonry-grid__item ${gridItemClasses || ''}`);

  const handleLayoutComplete = () => {
    setLayoutComplete(true);
    if (onLoad) onLoad();
  };

  return (
    <Masonry
      ref={gridRef}
      className={gridClasses}
      options={options}
      onLayoutComplete={handleLayoutComplete}
      onImagesLoaded={handleLayoutComplete}
      {...other}
    >
      <div className="masonry-grid__sizer" />
      <div className="masonry-grid__gutter-sizer" />
      {children.map((c, i) => (
        <div
          key={i}
          className={`${itemClasses} ${c.props.gridItemClasses || ''}`}
        >
          {c}
        </div>
      ))}
    </Masonry>
  );
};

MasonryGrid.propTypes = {
  classes: PropTypes.string,
  options: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  gridItemClasses: PropTypes.string,
  onLoad: PropTypes.func,
};
