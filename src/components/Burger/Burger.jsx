import React from 'react'
import { SideModal } from '../SideModal/SideModal'
import { Route, useHistory } from 'react-router-dom'
import { BurgerOrderType } from './includes/BurgerOrderType/BurgerOrderType'
import { CityPicker } from '../CityPicker/CityPicker'
import { BurgerList } from './includes/BurgerList/Burgerlist'
import { ReactComponent as Smile } from '../../assets/icons/smile_white.svg'
import { Copyrights } from '../Copyrights/Copyrights'
import {
  aboutChainPage,
  aboutUsPage,
  authPage,
  contactsPage,
  couponsPage,
  deliveryTermPage,
  restaurantsPage,
  stocksPage,
} from '../../constants/pagelinks'

const burgerList = [
  {
    text: 'Акции',
    link: stocksPage,
  },
  {
    text: 'Купоны',
    link: couponsPage,
  },
  {
    text: 'Рестораны',
    link: restaurantsPage,
  },
  {
    text: 'Условия доставки',
    link: deliveryTermPage,
  },
  {
    text: 'Style',
    link: '/style',
  },
  {
    text: 'О нас',
    link: aboutUsPage,
    innerList: [
      {
        text: 'О сети',
        link: aboutChainPage,
      },
      {
        text: 'Контакты',
        link: contactsPage,
      },
    ],
  },
  {
    text: 'Вход',
    link: authPage,
    icon: Smile,
  },
]

export const Burger = () => {
  const path = `*/side-modal/burger`

  return (
    <Route
      path={path}
      render={() => (
        <SideModal classes={'burger'} contentClasses={'burger__side-modal'}>
          <div className='burger__city-picker'>
            <CityPicker />
          </div>

          <BurgerOrderType />

          <BurgerList list={burgerList} />

          <Copyrights classes={'burger__copyrights'} />
        </SideModal>
      )}
    />
  )
}
