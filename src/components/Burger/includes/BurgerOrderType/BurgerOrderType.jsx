import React from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as Pickup } from '../../../../assets/icons/take_in.svg'
import { ReactComponent as Delivery } from '../../../../assets/icons/delivery.svg'
import {
  setOrderTypeDelivery,
  setOrderTypePickup,
} from '../../../../store/actions'
import { useDispatch } from 'react-redux'
import { catalogPage } from '../../../../constants/pagelinks'

export const BurgerOrderType = () => {
  const dispatch = useDispatch()

  return (
    <div className={'burger-order-type'}>
      <Link
        onClick={() => dispatch(setOrderTypeDelivery())}
        className={
          'burger-order-type_delivery burger-order-type__link burger__item'
        }
        to={catalogPage}
      >
        <Delivery className={'burger-order-type__icon'} />
        Привезите мне
      </Link>
      <Link
        onClick={() => dispatch(setOrderTypePickup())}
        className={
          'burger-order-type_pickup burger-order-type__link burger__item'
        }
        to={catalogPage}
      >
        <Pickup className={'burger-order-type__icon'} />
        Заберу сам
      </Link>
    </div>
  )
}
