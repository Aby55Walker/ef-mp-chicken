import React from 'react'
import { NavLink } from 'react-router-dom'
import { Spoiler } from '../../../Spoiler/Spoiler'

export const BurgerList = (props) => {
  const { list = [] } = props

  return (
    <div className={'burger-list'}>
      {list.map((item) => {
        // вложенный раскрывающийся список в бургере
        if (item.innerList) {
          return (
            <Spoiler classes={'burger__spoiler'} title={item.text}>
              {item.innerList.map((innerItem) => (
                <NavLink className={'burger__spoiler-item'} to={innerItem.link}>
                  {innerItem.text}
                </NavLink>
              ))}
            </Spoiler>
          )
        }

        return (
          <NavLink className={'burger__item burger-list__item'} to={item.link}>
            {item.icon && <item.icon className={'burger-list__icon'} />}
            {item.text}
          </NavLink>
        )
      })}
    </div>
  )
}
