import React from 'react';
import { CSSTransition } from 'react-transition-group';
import { useHistory } from 'react-router-dom';
import { modalAnimationDuration } from '../../constants/animation';

export const Transition = ({
  toggle,
  classNames,
  children,
  closeMethod,
  onExited,
}) => {
  const exitHandler = () => {
    if (closeMethod) closeMethod();
    if (onExited) onExited();
  };

  return (
    <CSSTransition
      in={toggle}
      timeout={modalAnimationDuration}
      classNames={classNames}
      unmountOnExit
      appear
      onExited={exitHandler}
    >
      {children}
    </CSSTransition>
  );
};
