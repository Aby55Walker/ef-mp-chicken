// MainPageHeader
import React from 'react'
import { useDispatch } from 'react-redux'
import { Btn } from '../Btn/Btn'
import { setOrderTypeDelivery, setOrderTypePickup } from '../../store/actions'
import classNames from 'classnames'
import { sm } from '../../constants/breakpoints'
import { useSelector } from 'react-redux'

export const MainPageHeader = (props) => {
  const {
    text = `Сочная курица, экзотичные напитки и&nbsp;неповторимое Signature-меню с&nbsp;доставкой на&nbsp;дом или&nbsp;в&nbsp;ресторане`,
    classes = '',
    ...other
  } = props

  const dispatch = useDispatch()
  const { screenWidth } = useSelector((state) => state.screenSize)
  const headerClass = classNames(
    `main-page-header${classes ? ' ' + classes : ''}`,
    {}
  )

  return (
    <header className={headerClass} {...other}>
      <h1
        className='main-page-header__title'
        dangerouslySetInnerHTML={{ __html: text }}
      ></h1>

      <Btn
        onClick={() => dispatch(setOrderTypeDelivery())}
        centred
        fullWidth={screenWidth <= sm}
        primary
        classes='main-page-header__btn main-page-header__delivery'
        text='Привезите мне'
      />

      <Btn
        onClick={() => dispatch(setOrderTypePickup())}
        centred
        fullWidth={screenWidth <= sm}
        primary
        classes='main-page-header__btn main-page-header__pickup'
        text='Заберу сам'
      />
    </header>
  )
}
