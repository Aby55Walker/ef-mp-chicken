import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

export const Copyrights = (props) => {
  const { classes } = props

  const copyrightsClasses = classNames(`copyrights ${classes || ''}`, {})

  return (
    <div className={copyrightsClasses}>
      <span className='copyrights__item copyrights__year'>
        © 2019 Welcome group
      </span>

      <div className='copyrights__item'>
        <a className='link' href='#' title='Пользовательское соглашение'>
          Пользовательское соглашение
        </a>
      </div>

      <div className='copyrights__item copyrights__made-by'>
        <span>
          Сделано в{' '}
          <a className='link' href='#' title='Nutnet'>
            Nutnet
          </a>
          , {new Date().getFullYear()}
        </span>
      </div>
    </div>
  )
}

Copyrights.propTypes = {
  classes: PropTypes.string,
}
