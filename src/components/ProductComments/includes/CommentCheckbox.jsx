import React, { useState } from 'react';
import cn from 'classnames';

import { ReactComponent as CheckMark } from '../../../assets/icons/check_mark.svg';

export const CommentCheckbox = (props) => {
  const {
    value,
    name,
    text,
    price,
    onChange,
    classes,
  } = props;

  const [checked, setChecked] = useState(false);

  // classes
  const labelClasses = cn(`comment-checkbox ${classes || ''}`, { active: checked });
  const fakeCheckboxClasses = cn('comment-checkbox__fake-checkbox', { active: checked });
  const textClasses = cn('comment-checkbox__text', { active: checked });
  const commentPrice = `${Math.floor(price)}`;
  const commentPriceString = commentPrice > 0 ? `(+${commentPrice} ₽)` : '';

  const changeHandler = ({ target }, id) => {
    setChecked(target.checked);
    if (onChange) onChange(target, id, commentPrice);
  };

  return (
    <label className={labelClasses}>
      <input className="comment-checkbox__input" name={name} checked={checked} onChange={(e) => changeHandler(e, value)} type="checkbox" />
      <span className={textClasses}>{text} {commentPriceString}</span>
      {checked
        ? (
          <div className="comment-checkbox__icon-wrapper">
            <CheckMark className="comment-checkbox__icon" />
          </div>
        )
        : <span className={fakeCheckboxClasses} />}
    </label>
  );
};
