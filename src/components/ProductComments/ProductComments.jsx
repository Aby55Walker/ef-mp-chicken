import React, { useEffect, useState } from 'react';
import { CommentCheckbox } from './includes/CommentCheckbox';

export const ProductComments = (props) => {
  const { comments, onChange } = props;
  const [selected, setSelected] = useState(comments.map((x) => ({ id: x.id, checked: null, price: 0 })));

  const changeHandler = (target, id, price) => {
    setSelected((prevState) => prevState.map((x) => {
      if (id === x.id) return { ...x, checked: target.checked, price };
      return x;
    }));
  };

  useEffect(() => {
    if (onChange) {
      onChange(
        // передаёт id выбранных
        selected.filter((item) => item.checked).map((item) => item.id),
        // передаёт сумму выбранных
        selected.reduce((acc, curr) => {
          if (curr.checked) return acc + Math.floor(+curr.price);
          return 0;
        }, 0),
      );
    }
  }, [selected]);

  return (
    <div className="product-comments">
      {
        comments.map(({ id, name, price }) => (
          <CommentCheckbox
            key={id}
            text={name}
            value={id}
            name={name}
            price={price}
            onChange={changeHandler}
          />
        ))
      }
    </div>
  );
};
