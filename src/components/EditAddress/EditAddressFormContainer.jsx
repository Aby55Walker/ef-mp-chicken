import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { intercomList, radioButtons } from '../AddAddress/data';
import { Btn } from '../Btn/Btn';
import { AddressForm } from '../AddressForm/AddressForm';
import { flat, house, office } from '../../constants/address';
import {
  userAddAddress,
  userDeleteAddressRequest,
  userUpdateAddress,
} from '../../store/modules/user/actionCreators';
import { toCamelCase } from '../../functions/toCamelCase';

export const allAddressFields = [
  'address',
  'flat',
  'floor',
  'porch',
  'room',
  'is_intercom_broken',
  'call_intercom',
  'id',
  'type',
  'comment',
];

export const EditAddressFormContainer = ({ onSubmit }) => {
  const { addressIndex } = useParams();
  const addressData = useSelector(
    (state) => state.user.data.addresses[addressIndex],
  );
  const { loading } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const history = useHistory();

  const submitHandler = (data) => {
    const updateAddressData = {
      id: addressData.id,
    };

    // отправляет только не пустые поля
    allAddressFields.forEach((item) => {
      if (data[toCamelCase(item)]) {
        updateAddressData[item] = data[toCamelCase(item)];
      }
    });

    dispatch(userAddAddress(updateAddressData, history));
  };

  const deleteAddressHandler = () => {
    dispatch(
      userDeleteAddressRequest({
        data: addressData.id,
        meta: {
          history,
        },
      }),
    );
  };

  const getHouseType = () => (addressData?.type === 'office'
    ? office
    : addressData?.type === 'house'
      ? house
      : flat);

  return (
    <AddressForm
      initialValues={addressData}
      initialHouseType={getHouseType()}
      triggerOnMount
      radioButtons={radioButtons}
      onSubmit={submitHandler}
      circleRadioBtns={intercomList}
    >
      <Btn
        type="submit"
        text="Сохранить"
        primary
        centred
        loading={loading}
        fullWidth
        disabled
      />
      <Btn
        type="button"
        text="Удалить"
        transparent
        loading={loading}
        centred
        fullWidth
        onClick={deleteAddressHandler}
        error
      />
    </AddressForm>
  );
};
