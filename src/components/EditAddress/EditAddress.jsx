import React from 'react';
import { Route } from 'react-router-dom';
import { SideModal } from '../SideModal/SideModal';

// data
import { EditAddressFormContainer } from './EditAddressFormContainer';

export const EditAddress = () => {
  const path = '/*/side-modal/edit-address/:addressIndex';

  const submitHandler = (data) => {
    // console.log('Edit address data', data)
  };

  return (
    <Route
      path={path}
      render={() => (
        <SideModal contentClasses="add-address__modal-content">
          <h2 className="side-modal__title">Адрес доставки</h2>

          <div className="add-address">
            <EditAddressFormContainer onSubmit={submitHandler} />
          </div>
        </SideModal>
      )}
    />
  );
};
