import React, { useEffect } from 'react';
import { Input } from '../Input/Input';
import { flatRules, floorRules, porchRules } from '../../constants/validate';
import { ApartmentError } from './ApartmentError';
import { flat, house, office } from '../../constants/address';

const defaultValues = {
  flat: '',
  floor: '',
  porch: '',
  type: 'Квартира',
};

export const ApartmentInputs = (props) => {
  const {
    houseType, // тип помещения
    register, // из useForm
    errors, // из useForm
    clearError, // из useForm
    setError,
    triggerValidation,
    unregister,
    reset,
    initialValues = defaultValues,
    ...rest
  } = props;

  const isHouse = houseType.toLowerCase() === house.toLowerCase();
  const isOffice = houseType.toLowerCase() === office.toLowerCase();

  const changeHandler = (name) => {
    triggerValidation(name);
    clearError([name]);
  };

  useEffect(() => {
    setTimeout(() => {
      if (isHouse) {
        clearError(['flat', 'floor', 'porch']);
        unregister(['flat', 'floor', 'porch']);
      }
      if (isOffice) {
        clearError(['porch', 'floor']);
        unregister(['porch', 'floor']);
      }
      triggerValidation(['flat', 'floor', 'porch']);
    }, 100);
  }, [houseType]);

  const setRegister = (rules, name) => {
    if (isHouse) {
      return { required: false };
    } if (isOffice && name === 'flat') {
      return register(rules);
    } if (isOffice && (name === 'porch' || name === 'floor')) {
      return { required: false };
    }
    return register(rules);
  };

  return (
    <div {...rest} className="apartment-inputs">
      <Input
        classes="apartment-inputs__input"
        ref={setRegister(flatRules, 'flat')}
        error={errors.flat}
        // errorText={errors.flat && errors.flat.message}
        name="flat"
        value={initialValues.flat || initialValues.room}
        disabled={isHouse}
        label={houseType === house ? 'Дом' : houseType}
        onChange={() => changeHandler('flat')}
      />
      <Input
        classes="apartment-inputs__input"
        ref={setRegister(porchRules, 'porch')}
        error={errors.porch}
        // errorText={errors.porch && errors.porch.message}
        value={initialValues.porch}
        name="porch"
        label="Подъезд"
        disabled={isHouse || isOffice}
        onChange={() => changeHandler('porch')}
      />
      <Input
        classes="apartment-inputs__input"
        ref={setRegister(floorRules, 'floor')}
        error={errors.floor}
        // errorText={errors.floor && errors.floor.message}
        value={initialValues.floor}
        name="floor"
        label="Этаж"
        disabled={isHouse || isOffice}
        onChange={() => changeHandler('floor')}
      />
      <ApartmentError
        houseType={houseType}
        errors={[errors.flat, errors.porch, errors.floor]}
      />
    </div>
  );
};
