import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { flat, office } from '../../constants/address';

export const ApartmentError = ({ errors = [], houseType = flat }) => {
  const [error, setError] = useState('');

  const errorsCount = [];
  // определяет есть ли ошибки с типом required
  const errorTypeRequiredIndex = errors.findIndex(
    (e) => e && e.type && e.type === 'required',
  );

  errors.forEach((e) => {
    if (errorTypeRequiredIndex >= 0) {
      if (e && e.type === 'required') errorsCount.push(e);
    } else if (e) errorsCount.push(e);
  });
  // заменяет "квартира" на "офис" в тексте ошибки
  if (houseType === office) {
    errorsCount.forEach((item) => {
      if (item.message === flat.toLowerCase()) {
        item.message = office.toLowerCase();
      }
    });
  }

  const formatErrorString = () => {
    switch (errorsCount.length) {
      case 0:
        setError('');
        break;
      case 1:
        setError(`Некорректный формат ${errorsCount[0].message}`);
        break;
      case 2:
        setError(
          `Некорректный формат ${errorsCount[0].message} и ${errorsCount[1].message}`,
        );
        break;
      case 3:
        setError(
          `Некорректный формат ${errorsCount[0].message}, ${errorsCount[1].message} и ${errorsCount[2].message}`,
        );
        break;
      default:
        setError('Заполните обязательные поля');
    }
  };

  const requiredErrorString = () => {
    switch (errorsCount.length) {
      case 0:
        setError('');
        break;
      case 1:
        setError(
          `Поле ${
            houseType === 'office' ? 'офиса' : errorsCount[0].message
          } обязательно`,
        );
        break;
      case 2:
        setError(
          `Поля ${errorsCount[0].message} и ${errorsCount[1].message} обязательны`,
        );
        break;
      case 3:
        setError(
          `Поля ${errorsCount[0].message}, ${errorsCount[1].message} и ${errorsCount[2].message} обязательны`,
        );
        break;
      default:
        setError('Заполните обязательные поля');
    }
  };

  useEffect(() => {
    if (errorTypeRequiredIndex >= 0) {
      requiredErrorString();
    } else {
      formatErrorString();
    }
  }, [errors]);

  return <span className="apartment-error">{error}</span>;
};

ApartmentError.propTypes = {
  errors: PropTypes.any.isRequired,
  houseType: PropTypes.string.isRequired,
};
