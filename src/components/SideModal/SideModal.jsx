// SideModal

import React, { useEffect, useState, useRef } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import { useHistory, useLocation } from 'react-router-dom';
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { showSlideModal } from '../../store/actions';
import { Transition } from '../Transition/Transition';
import { sm, lg } from '../../constants/breakpoints';
import { useSwipeClose } from '../../hooks/useSwipeClose';
import { SideModalContent } from './SideModalContent';

export const SideModal = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const [isOpen, setIsOpen] = useState(true);
  const [nesting, setNesting] = useState(false);
  const { screenWidth } = useSelector((state) => state.screenSize);
  const modalRef = useRef();
  const {
    children,
    classes = '',
    internal = false, // внутрянняя страница, кнопки "Закрыть" и свайпа нет
    contentClasses = '',
    swipeOnMobile, // в мобильной версии закрывать свайпом
    onExited, // callBakc после закрытия модалки
  } = props;

  useEffect(() => {
    disableBodyScroll(modalRef.current);
    // сдвинуть контент страницы если это не мобильная версия со свайпом
    dispatch(showSlideModal(!(swipeOnMobile && screenWidth < lg)));

    return () => {
      clearAllBodyScrollLocks();
      dispatch(showSlideModal(false));
    };
  }, []);

  useEffect(() => {
    const root = document.documentElement;
    if (swipeOnMobile) {
      root.style.setProperty('--modal-width-desktop', '392px');
    }
    return () => {
      root.style.setProperty('--modal-width-desktop', '560px');
    };
  }, []);

  useEffect(() => {
    if (isOpen) disableBodyScroll(modalRef.current);
    setNesting(checkNesting());
  }, [location, isOpen]);

  // если модальное окно закрыто, но в url по какой-то причине осталась запись "side-modal",
  // то передать в стор параметр для возвращения контента страницы на место (он сдвигается)
  useEffect(() => history.listen((location) => {
    const isOpenModal = location.pathname.indexOf('side-modal/') > -1;

    if (!isOpenModal) {
      dispatch(showSlideModal(false));
    }
  }), [history]);

  // вернуться назад
  const back = () => history.goBack();

  // закрыть модальное окно
  const closeModal = () => {
    setIsOpen(false);
    dispatch(showSlideModal(false));
  };

  const exitHandler = () => {
    if (onExited) onExited();
  };

  const {
    swipeStyles,
    touchStartHandler,
    touchMoveHandler,
    touchEndHandler,
  } = useSwipeClose(100, closeModal);

  // проверка на вложенность
  const checkNesting = () => {
    const pathArray = location.pathname.split('side-modal/');
    if (pathArray.length > 1) {
      const nestingPathArray = pathArray[1].replace(/^\/|\/$/g, '').split('/');
      return nestingPathArray.length > 1;
    }
  };

  const overlayClass = classNames('side-modal-overlay', { show: isOpen });
  const sideModalClass = classNames(`side-modal ${classes || ''}`, {
    'side-modal_with-swipe': swipeOnMobile,
    'side-modal_internal': internal,
  });
  const contentClass = classNames(`side-modal__content ${contentClasses || ''}`, {
    'side-modal__content_with-swipe': swipeOnMobile,
    'side-modal__content_internal': internal,
  });
  const contentWpClass = classNames(`side-modal__content-wp ${contentClasses || ''}`, {
    'side-modal__content-wp_internal': internal,
  });

  return createPortal(
    <div className={sideModalClass} ref={modalRef}>

      <Transition
        classNames="fade"
        toggle={isOpen}
      >
        <div className={overlayClass} onClick={closeModal} />
      </Transition>

      <Transition
        classNames={(swipeOnMobile && screenWidth < lg) ? 'fade' : 'side'}
        toggle={isOpen}
        onExited={exitHandler}
        closeMethod={() => history.push(location.pathname.split('/side-modal/')[0])}
      >

        {swipeOnMobile && screenWidth < lg
          ? (
            <div className={contentWpClass}>
              <SideModalContent
                {...props}
                swipeStyles={swipeStyles}
                touchStartHandler={touchStartHandler}
                touchMoveHandler={touchMoveHandler}
                touchEndHandler={touchEndHandler}
                back={back}
                nesting={nesting}
                closeModal={closeModal}
                internal={internal}
                contentClass={contentClass}
              />
            </div>
          )
          : (
            <SideModalContent
              {...props}
              swipeStyles={swipeStyles}
              touchStartHandler={touchStartHandler}
              touchMoveHandler={touchMoveHandler}
              touchEndHandler={touchEndHandler}
              back={back}
              nesting={nesting}
              closeModal={closeModal}
              internal={internal}
              contentClass={contentClass}
            />
          )}

      </Transition>
    </div>,
    document.getElementById('side-modal-root'),
  );
};

SideModal.propTypes = {
  classes: PropTypes.string,
  contentClasses: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};
