import React from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { ReactComponent as IconClose } from '../../assets/icons/clear.svg';
import { ReactComponent as IconBack } from '../../assets/icons/arrow_back.svg';
import { lg } from '../../constants/breakpoints';

export const SideModalContent = (props) => {
  const { screenWidth } = useSelector((state) => state.screenSize);
  const {
    children,
    contentClass = '',
    swipeOnMobile, // в мобильной версии закрывать свайпом
    swipeStyles,
    back,
    internal,
    nesting,
    touchStartHandler,
    touchEndHandler,
    touchMoveHandler,
    closeModal,
  } = props;

  return (
    <div
      className={contentClass}
      style={swipeOnMobile && screenWidth < lg ? swipeStyles : null}
    >
      {nesting && (
      <button
        className="side-modal__btn side-modal__back"
        onClick={back}
        title="Назад"
      >
        <IconBack className="side-modal__icon" />
      </button>
      )}
      {/* показать кнопку Закрыть если это не: 1)мобильная версия со свайпом 2)внутрянняя страница */}
      {!(screenWidth < lg && swipeOnMobile || internal) && (
      <button
        className="side-modal__btn side-modal__close"
        onClick={closeModal}
        title="Закрыть"
      >
        <IconClose className="side-modal__icon" />
      </button>
      )}

      {screenWidth < lg && swipeOnMobile && !internal && (
      <div
        onTouchStart={touchStartHandler}
        onTouchEnd={touchEndHandler}
        onTouchMove={touchMoveHandler}
        className="common-modal__swipe"
      />
      )}
      {typeof children === 'function'
        ? children(closeModal) : children}
    </div>
  );
};
