import React from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import { Communications } from './includes/Communications/Communications'
import { Human } from '../Human/Human'
import { Socials } from '../Socials/Socials'

const contactProps = {
  image: `https://i.ibb.co/tXyN1hY/image-179.png`,
  name: `Максим Анатольевич Коновалов`,
  position: `Генеральный директор холдинга`,
}

export const Contact = (props) => {
  const {
    classes,
    renderHuman,
    title,
    children,
    data,
    borderTop,
    ...other
  } = props

  const contactClasses = classNames(`contact ${classes ? classes : ''}`, {})

  if (!data) return null

  return (
    <div {...other} className={contactClasses}>
      {(data.title || title) && (
        <h3 className={'contact__title'}>{data.title || title}</h3>
      )}

      {data.description && (
        <span className={'contact__description'}>{data.description}</span>
      )}

      {data.first_name && (
        <Human
          image={`https://i.ibb.co/tXyN1hY/image-179.png`}
          name={`${data.first_name} ${data.last_name}`}
          position={data.position}
          classes={'contacts__human'}
        />
      )}

      {(data.vk || data.instagram || data.twitter || data.facebook) && (
        <Socials classes={'contacts__social'} data={data} />
      )}

      <Communications email={data.email} phone={data.phone} />
    </div>
  )
}

Contact.propTypes = {
  title: PropTypes.string,
}
