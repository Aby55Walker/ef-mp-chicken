import React from 'react'
import classNames from 'classnames'
import { useGeolocation } from '../../../../hooks/hooks'

export const InfoBlock = (props) => {
  const { title, text, classes, children, buildRoad, ...other } = props

  const infoBlockClasses = classNames(
    `info-block ${classes ? classes : ''}`,
    {}
  )

  const [lat, lon] = useGeolocation()

  return (
    <div {...other} className={infoBlockClasses}>
      <h3 className={'info-block__title'}>{title}</h3>
      <div
        className={'info-block__text'}
        dangerouslySetInnerHTML={{ __html: text }}
      />

      {buildRoad && (
        <a
          className={'info-block__link'}
          target={'_blank'}
          href={`https://yandex.ru/maps/?rtext=${lat},${lon}~56.820833,53.208903&rtt=comparison`}
        >
          Построить маршрут
        </a>
      )}

      {children}
    </div>
  )
}
