import React from 'react'
import classNames from 'classnames'

import { ReactComponent as Phone } from '../../../../assets/icons/phone.svg'
import { ReactComponent as Email } from '../../../../assets/icons/email.svg'

export const Communications = (props) => {
  const { email, phone, classes, ...other } = props

  const communicationsClasses = classNames(
    `communications ${classes ? classes : ''}`,
    {}
  )

  return (
    <div {...other} className={communicationsClasses}>
      {phone && (
        <a className='communications__item' href={`tel: +${phone}`}>
          <Phone className='communications__icon' />
          <span className='communications__text'>{phone}</span>
        </a>
      )}
      {email && (
        <a className='communications__item' href={`mailto: ${email}`}>
          <Email className='communications__icon' />
          <span className='communications__text'>{email}</span>
        </a>
      )}
    </div>
  )
}
