import React, { useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import { useHistory, useLocation } from 'react-router-dom';
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { Transition } from '../Transition/Transition';
import { ReactComponent as Close } from '../../assets/icons/clear.svg';
import { lg } from '../../constants/breakpoints';
import { useSwipeClose } from '../../hooks/useSwipeClose';

export const CommonModal = (props) => {
  const { screenWidth } = useSelector((state) => state.screenSize);

  const {
    children,
    classes,
    contentClasses = '',
    swipeOnMobile = screenWidth < lg,
    ...other
  } = props;

  const history = useHistory();
  const [modalIsOpen, setModalIsOpen] = useState(true);
  const location = useLocation();

  const closeModal = () => setModalIsOpen(false);

  const {
    swipeStyles,
    touchStartHandler,
    touchMoveHandler,
    touchEndHandler,
  } = useSwipeClose(100, closeModal);

  const contentRef = useRef();
  const modalRef = useRef();

  const modalClasses = classNames(`common-modal ${classes || ''}`, {
    'common-modal_with-swipe': swipeOnMobile,
  });
  const innerContentClasses = classNames(
    `common-modal__content ${contentClasses || ''}`,
    {
      'common-modal__content_with-swipe': swipeOnMobile,
    },

  );

  useEffect(() => {
    disableBodyScroll(modalRef.current);
    return () => clearAllBodyScrollLocks();
  }, []);

  return createPortal(
    <Transition
      // closeMethod={() => history.goBack()}
      closeMethod={() => history.push(location.pathname.split('/modal/')[0])}
      toggle={modalIsOpen}
      classNames="fade"
    >
      <div {...other} ref={modalRef} className={modalClasses}>
        <div className="common-modal__overlay" onClick={closeModal} />

        <div
          ref={contentRef}
          style={swipeStyles}
          className={innerContentClasses}
        >
          <Close className="common-modal__close" onClick={closeModal} />

          {screenWidth < lg && swipeOnMobile && (
            <div
              onTouchStart={touchStartHandler}
              onTouchEnd={touchEndHandler}
              onTouchMove={touchMoveHandler}
              className="common-modal__swipe"
            />
          )}

          {children}
        </div>
      </div>
    </Transition>,
    document.getElementById('common-modal-root'),
  );
};
