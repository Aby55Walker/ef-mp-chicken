import React from 'react';
import classNames from 'classnames';
import Stories from 'react-insta-stories';
import { sm } from '../../constants/breakpoints';

export const Story = (props) => {
  const {
    classes,
    stories = [],
    isPaused = false,
    duration = 5000,
    ...other
  } = props;

  const classesBlock = classNames(`story ${classes || ''}`, {});

  return (
    <div className={classesBlock}>
      <Stories
        {...other}
        isPaused={isPaused}
        stories={stories}
        defaultInterval={duration}
        width={getComputedStyle(document.documentElement).getPropertyValue(
          '--story-width',
        )}
        height={
          window.innerWidth > sm
            ? getComputedStyle(document.documentElement).getPropertyValue(
              '--story-height',
            )
            : window.innerHeight
        }
      />
    </div>
  );
};
