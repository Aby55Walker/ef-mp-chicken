import React, { useState, useRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useRouteMatch, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { debounce } from 'lodash';
import { Btn } from '../Btn/Btn';
import { Input } from '../Input/Input';
import { codeRules } from '../../constants/validate';
import {
  authCodeError,
  authCodeSuccess,
} from '../../store/modules/auth/actionCreators';
import { makeMaskedPhone } from '../../functions/makeMaskedPhone';
import { fetchCodeRequest } from '../../store/modules/auth/actions';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';

export const CodeIntroduction = (props) => {
  const {
    phoneNumber,
    codeLength = 4,
    matchUrl = '*/side-modal/registration',
    onCodeSuccessLink = '/personal-account',
    onSubmit,
    changePhoneBtn = true,
    ...other
  } = props;

  const history = useHistory();
  const dispatch = useDispatch();
  const {
    codeError, codeSuccess, phone, loading,
  } = useSelector((state) => state.auth);

  const match = useRouteMatch(`${matchUrl}`);
  const [inputCode, changeCode] = useState([...Array(codeLength)]);
  const refForm = useRef();
  const {
    handleSubmit, register, errors,
  } = useForm(); // валидация формы

  // поставить фокус на первый input
  useEffect(() => { getToInputByIndex(); }, []);

  useEffect(() => {
    if (codeSuccess) history.push(`${onCodeSuccessLink}`);
    return () => dispatch(authCodeSuccess(false));
  }, [codeSuccess]);

  const submit = (data) => {
    if (onSubmit) onSubmit(data);
  };

  const debounced = debounce(submit, 500);
  const submitHandler = (data) => { debounced(data); };

  // проверить введёл ли код полностью
  // если да => submitHandler
  const preSubmitChecking = () => {
    const fullCode = inputCode.join('');
    if (fullCode.length === codeLength) submitHandler(fullCode);
  };

  useEffect(() => { preSubmitChecking(); }, [inputCode]);

  // при изменении значения input
  const handleChange = (value = '', id = 0) => {
    // удаляет ошибку
    dispatch(authCodeError(null));

    const newArray = inputCode;
    newArray[id] = value;
    changeCode(newArray); // записать значение в массив

    // если значение заполнено, то поставить фокус на следующий input
    if (value) getToInputByIndex(id + 1);

    preSubmitChecking();
  };

  // при нажатии на Ctrl+V в пределах формы
  // ставить значение в поля
  const onPaste = (e) => {
    e.preventDefault();
    const paste = e.clipboardData.getData('text');
    const pasteArray = paste.replace(/[^\d]/g, '').slice(0, codeLength);
    const newArray = pasteArray.split('');

    changeCode(newArray);
  };

  const onKeyDown = (e, id) => {
    const { keyCode } = e;
    const backspaceKey = 8;

    // если значение инпута пустое и нажат Backspace => перейти к предыдущему input
    if (!inputCode[id] && keyCode === backspaceKey) getToInputByIndex(id - 1);
  };

  // перейти к input по его индексу
  const getToInputByIndex = (index = 0) => {
    const input = refForm.current.elements[index];
    if (input) {
      input.focus();
      input.select();
    }
  };

  const clickHandler = () => {
    // TODO тут должна происходить очистка
    dispatch(
      fetchCodeRequest({
        phone: makeUnmaskedPhone(phone),
      }),
    );
  };

  return (
    <div className="code-introduction" {...other}>
      <h2 className="side-modal__title">Введите код</h2>
      <p className="side-modal__text code-introduction__text">
        Мы отправили код на {makeMaskedPhone(phone)}
      </p>
      {changePhoneBtn && (
        <Link
          to={{ pathname: `${match.url}` }}
          className="code-introduction__change"
        >
          Изменить
        </Link>
      )}

      <form
        onSubmit={handleSubmit(preSubmitChecking)}
        className="code-introduction__form"
        onPaste={onPaste}
        ref={refForm}
      >
        <label className="code-introduction__input-wrapper">
          {[...Array(codeLength)].map((elem, i) => (
            <Input
              key={i}
              value={inputCode[i] || ''}
              ref={register(codeRules)}
              name={`codeNumber_${i}`}
              error={errors && errors[`codeNumber_${i}`]}
              square
              classes="code-introduction__input"
              onChange={(value) => handleChange(value, i)}
              onKeyDown={(e) => onKeyDown(e, i)}
              onFocus={() => getToInputByIndex(i)}
            />
          ))}
        </label>

        {codeError && (
          <span className="code-introduction__error-text">{codeError}</span>
        )}
      </form>

      <Btn
        loading={loading}
        onClick={clickHandler}
        classes="code-introduction__btn"
        text="Получить другой код"
        transparent
        centred
        fullWidth
      />
    </div>
  );
};
