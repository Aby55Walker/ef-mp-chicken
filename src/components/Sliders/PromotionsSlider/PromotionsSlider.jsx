import React from 'react'
import { useSelector } from 'react-redux'
import { Slider } from '../../Slider/Slider'
import { PromotionCard } from '../../PromotionCard/PromotionCard'
import { sm } from '../../../constants/breakpoints'

export const PromotionsSlider = (props) => {
  const { data = [], classes = '', ...other } = props

  return (
    <>
      {data.length ? (
        <Slider
          {...other}
          sliderClasses={`promotions-slider ${classes || ''}`}
          settings={{
            slidesPerView: 'auto',
            spaceBetween: 8,
            breakpoints: {
              1025: {
                mousewheel: true,
                spaceBetween: 10,
                slidesPerView: 'auto',
                freeMode: true,
                effect: 'slide',
              },
            },
          }}
        >
          {data.map((param, i) => (
            <PromotionCard
              key={i}
              index={i}
              classes='promotions-slider__card'
              data={param}
            />
          ))}
        </Slider>
      ) : (
        ''
      )}
    </>
  )
}
