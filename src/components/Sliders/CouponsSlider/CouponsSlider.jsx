/* eslint-disable react/button-has-type */
import React from 'react';
import { useSelector } from 'react-redux';
import { Slider } from '../../Slider/Slider';
import { CouponCard } from '../../CouponCard/CouponCard';
import { ReactComponent as Arrow } from '../../../assets/icons/arrow_back.svg';
import { lg } from '../../../constants/breakpoints';

export const CouponsSlider = (props) => {
  const {
    data = [],
    ...other
  } = props;
  const { screenWidth } = useSelector((state) => state.screenSize);

  return (
    <Slider
      {...other}
      sliderClasses="coupons-slider"
      prevButton={
        screenWidth >= lg
          ? (props) => (
            <button {...props} className="coupons-slider__btn coupons-slider__btn_prev">
              <Arrow />
            </button>
          ) : null
      }
      nextButton={
        screenWidth >= lg
          ? (props) => (
            <button {...props} className="coupons-slider__btn coupons-slider__btn_next">
              <Arrow />
            </button>
          ) : null
      }
    >
      {data.map((data, index) => (
        <CouponCard key={index} data={data} classes="coupons-slider__card" />
      ))}
    </Slider>
  );
};
