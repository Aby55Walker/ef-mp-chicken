import React, { useState } from 'react';
import PropTypes from 'prop-types';

export const WithRadio = (props) => {
  const {
    list,
    defaultSelected = null,
    Component,
    onClick,
    wrapperClasses,
    triggerValidation,
    onChange,
    ...other
  } = props;

  const [selected, setSelected] = useState(defaultSelected);

  const changeHandler = (value, name, index) => {
    setSelected(value);
    if (triggerValidation) triggerValidation(name);
    if (onChange) onChange(value, index);
  };

  return (
    <div className={wrapperClasses}>
      {list.map((item, i) => {
        if (item.hide) return null;
        return (
          <Component
            key={i}
            onChange={({ value, name }) => changeHandler(value, name, i)}
            active={selected == item.value}
            {...item}
            {...other}
          />
        );
      })}
    </div>
  );
};

WithRadio.propTypes = {
  Component: PropTypes.any.isRequired,
  list: PropTypes.array.isRequired,
  defaultSelected: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  wrapperClasses: PropTypes.string,
};
