import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Modal } from '../Modal/Modal';
import { Btn } from '../Btn/Btn';
import preview1 from './preview1.png';
import { STOP_MODAL_CONTENT } from '../../store/types';
import { sm } from '../../constants/breakpoints';
import { modalStop, catalogPage } from '../../constants/pagelinks';

export const StopModal = () => {
  const { pathname } = useLocation();
  const storeArray = useSelector((state) => state.stopModalContent);
  const screenWidth = useSelector((state) => state.screenSize);
  const [text, setText] = useState([]);
  const [combinedArrays, setCombinedArrays] = useState([]);

  const combineArrays = (arr) => {
    const deleteArr = arr?.delete || [];
    const changeArr = arr?.change || [];

    return [...deleteArr, ...changeArr];
  };

  // форматировать текст из массива
  const formatText = () => {
    if (!combinedArrays.length) return;

    const arr = combinedArrays.map((elem) => elem?.name);
    const { length } = arr;
    if (!length) return;

    const fixedArr = []; // массив форматированных названий
    arr.forEach((text, i) => {
      // если это единственный элемент или предпоследний в массиве
      if (length === 1 || i === length - 2) {
        fixedArr.push(text); // записать без изменений
      } else if (i === length - 1) {
        fixedArr.push(`и ${text}`); // последний элемент с 'и' вначале
      } else {
        fixedArr.push(`${text},`); // все остальные с запятой на конце
      }
    });

    setText(fixedArr);
  };

  useEffect(() => {
    // объединить массивы недоступных и изменившихся товаров
    setCombinedArrays(combineArrays(storeArray));

    // если есть массив данных -> записать его в sessionStorage и создать массив
    if (Object.values(storeArray).length) {
      sessionStorage.setItem(STOP_MODAL_CONTENT, JSON.stringify(storeArray));
    } else {
      // если массив пустой и модалка открыта -> закрыть её
      // if (pathname.indexOf(modalStop) !== -1) history.goBack();
    }
  }, [storeArray]);

  useEffect(() => {
    formatText();
  }, [combinedArrays]);

  return (
    <Modal contentClasses="stop-modal__content">
      <h3 className="stop-modal__title">
        К&nbsp;сожалению, {screenWidth <= sm && <br />} блюда закончились
      </h3>

      <ul className="stop-modal__list">
        {combinedArrays?.length
          ? combinedArrays.map(({ name, image }, i) => (
            <li className="stop-modal__item" key={i}>
              <img
                className="stop-modal__img"
                src={preview1} // TODO src={image}
                alt={name}
              />
            </li>
          ))
          : null}
      </ul>

      <p className="stop-modal__paragraph">
        {text?.length
          ? text.map((elem, i) => (
            <span className="stop-modal__text" key={i}>
              {elem}
            </span>
          ))
          : null}
      </p>

      <div className="stop-modal__buttons">
        <Btn
          classes="stop-modal__btn"
          text="Выбрать замену"
          tag="Link"
          to={catalogPage}
          outlined
          centred
        />
        <Btn
          classes="stop-modal__btn"
          text="Продолжить оформление"
          tag="Link"
          to={`${pathname.split(modalStop)[0]}`}
          // onClick={() => history.goBack()}
          primary
          centred
        />
      </div>
    </Modal>
  );
};
