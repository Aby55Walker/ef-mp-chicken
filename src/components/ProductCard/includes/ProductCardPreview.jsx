import React, { useEffect, useState } from 'react';
import image from '../../../assets/images/catalog/product-1.png';

import { CardMarkers } from './CardMarkers';
import { randomInteger } from '../../../functions/randomInteger';
import { Sticker } from '../../ScreenSticker/Sticker';
import { randomSticker } from '../../../hooks/useRandomSticker';

const outsideOffset = '17px';
const innerOffset = '48px';

const stickerPositions = [
  {
    left: `-${outsideOffset}`,

    top: innerOffset,
  },
  {
    left: innerOffset,
    top: innerOffset,
  },
  {
    left: innerOffset,
    top: innerOffset,
  },
  {
    top: `-${outsideOffset}`,
    right: innerOffset,
  },
  {
    right: innerOffset,
    top: innerOffset,
  },
  {
    right: `-${outsideOffset}`,
    top: innerOffset,
  },
];

export const ProductCardPreview = ({
  img = randomSticker(),
  name,
  markers,
}) => {
  const [style, setStyle] = useState();
  const [withSticker, setWithSticker] = useState(!randomInteger(0, 2));
  // вычисление верятности появления стикера
  // выбор положения стикера
  useEffect(() => {
    if (!withSticker) return;
    // если есть маркеры, то стикер может быть только слева
    const positionLimit = markers.length ? (stickerPositions.length - 1) / 2 : stickerPositions.length - 1;
    const index = randomInteger(0, positionLimit);
    setStyle(stickerPositions[index]);
  }, []);

  return (
    <div className="product-card__preview">
      <img
        draggable="false"
        src={img}
        className="product-card__img"
        alt={name}
      />
      { withSticker && <Sticker onClick={() => setWithSticker(false)} style={style} />}
      <CardMarkers markers={markers} classes="product-card__markers" />
    </div>
  );
};
