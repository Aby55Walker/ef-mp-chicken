import React from 'react';
import cn from 'classnames';
import Hot from '../../../assets/icons/markers/marker_hot.svg';

export const CardMarkers = ({ markers = [], classes }) => {
  const markersClasses = cn(`card-markers ${classes || ''}`);

  return (
    <div className={markersClasses}>
      {markers.map(({ image, name }) => {
        // TODO заменить заглушку на картинку из API когда будет готова
        // const src = image;
        const src = Hot;
        return (
          <picture key={name} title={name} className="card-markers__marker">
            <img src={src} alt={name} />
          </picture>
        );
      })}
    </div>
  );
};
