import React from 'react';
import classNames from 'classnames';

import { useRandomClass } from '../../hooks/hooks';
import { breakfast } from '../../constants/catalog';

import { ReactComponent as Add } from '../../assets/icons/add-to-cart.svg';
import { ReactComponent as AddBordered } from '../../assets/icons/add-to-cart_bordered.svg';
import { ReactComponent as Clocks } from '../../assets/icons/clocks.svg';
import img from '../../assets/images/catalog/product-1.png';
import { ProductCardPreview } from './includes/ProductCardPreview';

const themes = [
  'product-card_gradient',
  'product-card_yellow',
  'product-card_violet',
  'product-card_pink',
];

export const ProductCard = (props) => {
  const {
    data: {
      id,
      name = '',
      description = '',
      weight = 100,
      price = '',
      pictures = {}, // TODO заменить на картинки из API (пока что их нет)
      markers = [],
      stocks = [], // TODO скидки
      classes = '',
    },
    gridItemClasses,
    startedAt = '',
    endedAt = '',
    slug = '',
    ...other
  } = props;

  // возвращает рандомный класс из массива классов
  const themeClass = useRandomClass(themes);

  // определяет доступна ли категория в данное время
  const isAvailableNow = () => {
    // если нет времени конца и начала, то всегд доступно
    if (!startedAt && !endedAt) return true;
    //
    const currTime = new Date().toLocaleTimeString();
    return currTime > startedAt && currTime < endedAt;
  };

  // выбирает иконку https://prnt.sc/tnb30c
  const addIcon = () => {
    if (!isAvailableNow()) return <Clocks className="product-card__clock-icon" />;
    return themeClass ? <AddBordered /> : <Add />;
  };

  const productCardClasses = classNames(`product-card ${classes || ''}`, {
    [themeClass]: !(slug === breakfast),
    'product-card_white': slug === breakfast,
  });

  return (
    <div {...other} className={productCardClasses}>
      <ProductCardPreview img={img} markers={markers} />
      <div className="product-card__info">
        <span className="product-card__name">{name}</span>
        <div className="product-card__footer">
          <span className="product-card__weight">{weight} г</span>
          <div className="product-card__price">
            <span>{price.slice(0, -3)} ₽</span>
            <button type="button" className="product-card__action">
              {addIcon()}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
