// Spoiler

import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import { ReactComponent as IconArray } from '../../assets/icons/chevron_up.svg';
import { spoilerAnimationDuration } from '../../constants/animation';

export const Spoiler = (props) => {
  const {
    open = false, // раскрыт ли блок изначально
    title = '', // заголовок блока (всегда отображается)
    subtitle = false, // подзаголовок, отображается в свёрнутом виде рядом со стрелкой
    Icon = null,
    description,
    text, // текст под заголовком, отображается всегда
    classes = '', // дополнительные классы для родительского блока (section)
    contentClasses = '', // дополнительные классы для контента
    children = null, // элементы, которые можно скрывать
    ...other // прочие параметры, присваиваются родительскому блоку (section)
  } = props;

  // state
  const [opened, changeOpen] = useState(open); // {boolean} раскрыт ли блок
  const [height, setHeight] = useState(open ? 'auto' : 0); // {string || number} высота блока
  const [maxHeight, setMaxHeight] = useState(0); // максимальная высота блока
  const refContent = useRef(null); // контент, высоту которого нужно анимировать

  // записать высоту блока после того, как компонент будет создан
  useEffect(() => {
    setTimeout(() => {
      if (!refContent.current) return;
      const currentHeight = refContent.current.scrollHeight; // scrollHeight, чтобы получать высоту и для скрытого блока
      setMaxHeight(currentHeight);
      setHeight(opened ? currentHeight : 0);
    }, 0);
  }, []);

  // при нажатии на кнопку показать/скрыть блок
  const handleChangeOpen = () => {
    const newValue = !opened;

    changeOpen(newValue);
    setHeight(newValue ? maxHeight : 0);
  };

  return (
    <section className={`spoiler ${classes}`} {...other}>
      {/* Заголовок блока */}
      <header className="spoiler__header" onClick={handleChangeOpen}>
        <div className="spoiler__header-content">
          <h2
            className="spoiler__title"
            dangerouslySetInnerHTML={{ __html: title }}
          />

          {Icon}

          <CSSTransition
            in={subtitle && !opened}
            timeout={spoilerAnimationDuration}
            classNames="fade"
            unmountOnExit
            appear
          >
            <span
              className="spoiler__subtitle"
              dangerouslySetInnerHTML={{ __html: subtitle }}
            />
          </CSSTransition>

          <button className="spoiler__btn-open">
            <IconArray
              className={`spoiler__array${opened ? ' spoiler__array_open' : ''}`}
            />
          </button>
        </div>

        {description && typeof description === 'object'
          ? description.map((item) => <p className="spoiler__description">{item}</p>)
          : description}

        {text && <p className="spoiler__text">{text}</p>}
      </header>

      {/* Контент, который можно скрыть */}
      <div
        className="spoiler__toggled-content"
        ref={refContent}
        style={{ maxHeight: height }}
      >
        <div className={`spoiler__content ${contentClasses}`}>{children}</div>
      </div>
    </section>
  );
};

Spoiler.propTypes = {
  open: PropTypes.bool, // раскрыт ли блок изначально
  title: PropTypes.string.isRequired, // заголовок блока (всегда отображается)
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  classes: PropTypes.string, // дополнительные классы для родительского блока (section)
  contentClasses: PropTypes.string, // дополнительные классы для контента
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]), // элементы, которые можно скрывать
};
