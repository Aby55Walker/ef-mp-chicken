import React, { useEffect } from 'react'
import { Btn } from '../../Btn/Btn'
import Image1 from '../../../assets/images/promotion-1-detail.png'

export const StoryContent = (props) => {
  const {
    data,
    path = '/', // ссылка на акцию
    image = false, // отображается ли картинка на экране
    description = data?.description,
    media = data?.picture?.detail,
    title = data?.title,
    to = data?.activity?.to,
    slug = data?.slug,
  } = props

  useEffect(() => {
    props.action('play')
    return () => props.action('pause')
  }, [])

  return (
    <div className='story-content'>
      {/* фотоная картинка на 1 экране */}
      {image && <img className='story-content__img' src={Image1} alt={title} />}

      {/* TODO это просто для примера, можно убрать после проверки */}
      {/* <img className='story-content__img' src={Image1} alt={title} /> */}

      {/* заголовок на 2 экране */}
      {!image && <h4 className='story-content__title'>{title}</h4>}

      {/* текст на втором экране */}
      {!image && description && (
        <div
          className='story-content__text'
          dangerouslySetInnerHTML={{ __html: description }}
        ></div>
      )}

      {/* срок действия акции на 2 экране */}
      {!image && to && (
        <b className='story-content__valid-until'>Акция действует до {to} г.</b>
      )}

      {/* ссылка, походая на кнопку на всех экранах */}
      {slug && (
        <Btn
          classes='story-content__btn'
          primary
          centred
          tag='Link'
          to={path}
          text='Выбрать'
        />
      )}
    </div>
  )
}
