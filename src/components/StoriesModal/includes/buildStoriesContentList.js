import React from 'react'
import { StoryContent } from './StoryContent'

export const buildStoriesContentList = (storiesArray) => {
  let allStories = []

  if (!storiesArray.length) return allStories

  storiesArray.forEach((item) => {
    allStories.push([
      {
        content: ({ action, isPaused }) => (
          <StoryContent
            action={action}
            isPaused={isPaused}
            image
            data={item}
            {...item.content}
          />
        ),
      },
      {
        content: ({ action, isPaused }) => (
          <StoryContent
            action={action}
            isPaused={isPaused}
            data={item}
            {...item.content}
          />
        ),
      },
    ])
  })

  return allStories
}
