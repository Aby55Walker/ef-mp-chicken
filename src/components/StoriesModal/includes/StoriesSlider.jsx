import React, { useEffect, useState, useRef } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { lg } from '../../../constants/breakpoints'
import { Slider } from '../../Slider/Slider'
import { ReactComponent as Arrow } from '../../../assets/icons/chevron_up.svg'
import { Story } from '../../Story/Story'
import { buildStoriesContentList } from './buildStoriesContentList'

export const StoriesSlider = (props) => {
  const { data = [], ...other } = props

  const { screenWidth } = useSelector((state) => state.screenSize)
  let { slideIndex } = useParams()
  const [currentSlide, setCurrentSlide] = useState(slideIndex)
  const refSlider = useRef()
  const history = useHistory()

  const storiesContents = buildStoriesContentList(data)

  useEffect(() => {
    // TODO поискать другой способ режить проблему на десктопе
    refSlider.current.swiper.detachEvents()
  }, [])

  // закрыть модалку если все сторис просмотрены
  const onAllStoriesEnd = (slideIndex) => {
    if (slideIndex === data.length - 1) {
      closeModal()
      return
    }
    refSlider.current.swiper.slideNext()
  }

  const onSlideChange = (i) => {
    setCurrentSlide(i)
  }

  const closeModal = () => {
    history.goBack()
  }

  return (
    <>
      <Slider
        {...other}
        ref={refSlider}
        onSlideChange={onSlideChange}
        sliderClasses={'stories-slider'}
        settings={{
          slidesPerView: 1,
          initialSlide: currentSlide,
        }}
        prevButton={
          screenWidth >= lg
            ? (props) => (
                <button
                  {...props}
                  className={'stories-slider__btn stories-slider__btn_prev'}
                >
                  <Arrow />
                </button>
              )
            : null
        }
        nextButton={
          screenWidth >= lg
            ? (props) => (
                <button
                  {...props}
                  className={'stories-slider__btn stories-slider__btn_next'}
                >
                  <Arrow />
                </button>
              )
            : null
        }
      >
        {storiesContents.length
          ? storiesContents.map((x, i) => (
              <div className={'stories-slider__story'} key={i}>
                <Story
                  isPaused={+currentSlide !== i}
                  stories={x}
                  onAllStoriesEnd={() => onAllStoriesEnd(i)}
                />
              </div>
            ))
          : []}
      </Slider>
    </>
  )
}
