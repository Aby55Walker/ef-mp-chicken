import React, { useEffect, useRef, useState } from "react";
import classNames from "classnames";
import { ReactComponent as DropdownArrow } from "../../assets/icons/arrow_dropdown.svg";

export const Dropdown = (props) => {
  const {
    items = [],
    onChange,
    label, // название поля (отображается сверху)
    classes,
    initialValue = null, // boolean, если задано, то выведется первый вариант из массива items
    ...other
  } = props;

  // refs
  const dropdownContent = useRef(null);
  const dropDown = useRef(null);

  // states
  const [height, setHeight] = useState(0);
  const [isOpen, setOpen] = useState(false);
  const [inputText, setInputText] = useState(""); // отображающийся текст
  const [activeItem, setActiveItem] = useState(null); // индекс текущего выбранного значения

  // классы
  const dropdownClasses = classNames(`dropdown ${classes ? ` ${classes}` : ""}`, {
    dropdown_open: isOpen,
  });
  const inputClasses = classNames(`dropdown-input ${classes ? ` ${classes}` : ""}`, {
    "dropdown-input_open": isOpen || inputText,
    active: isOpen,
  });
  const dropdownArrowClasses = classNames("dropdown-input__arrow", {
    "dropdown-input__arrow_open": isOpen,
  });

  // закрытие по клику в любое место
  useEffect(() => {
    window.addEventListener("click", closeOnWindowClick);
    return () => window.removeEventListener("click", closeOnWindowClick);
  }, []);

  // определяет высоту контента для анимации
  useEffect(() => {
    setHeight(isOpen ? dropdownContent.current.offsetHeight : 0);
  }, [isOpen]);

  // ставит начальное значние, если передан пропс initialValue
  useEffect(() => {
    if (initialValue) {
      if (items?.[0]?.text) {
        setInputText(items[0].text);
      } else if (typeof items?.[0] === "string") {
        setInputText(items[0]);
      }
    }
  }, [items]);

  // methods
  const toggleOpen = () => {
    setOpen(() => {
      setOpen(!isOpen);
    });
  };

  const closeOnWindowClick = (e) => {
    let { target } = e;
    while (target && target !== dropDown.current) {
      target = target.parentNode;
    }
    if (!target) setOpen(false);
  };

  const handleClick = (text, value, i) => {
    setInputText(text);
    if (onChange) onChange(value);
    // выделение выбранного значения
    setActiveItem(i);
    setOpen(false);
  };

  return (
    <div {...other} ref={dropDown} className={dropdownClasses}>
      {/* инпут дропдауна */}
      <label className={inputClasses} onClick={toggleOpen}>
        <span className="dropdown-input__label">{label}</span>
        <span className="dropdown-input__text">{inputText}</span>
        <DropdownArrow className={dropdownArrowClasses} />
      </label>

      {/* выпадающий список */}
      <div className="dropdown__content-wrapper" style={{ maxHeight: `${height}px` }}>
        <div ref={dropdownContent} className="dropdown__content">
          {items.map((item, i) => {
            const classes = ["dropdown__item"];
            if (i === activeItem) classes.push("active");

            let text = "";
            let value = "";
            if (typeof item === "object") {
              text = item?.text || "";
              value = item?.value || "";
            } else if (typeof item === "string") {
              text = item;
              value = item;
            } else {
              return null;
            }

            return (
              <div key={i} className={classes.join(" ")} onClick={() => handleClick(text, value, i)}>
                {text}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
