import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setOrderTypeDelivery, setOrderTypePickup } from '../../store/actions'
import { Link } from 'react-router-dom'
import { catalogPage } from '../../constants/pagelinks'
import cn from 'classnames'

export const OrderType = (props) => {
  const {
    classes,
    deliveryClasses,
    pickupClasses,
    deliveryLink = catalogPage,
    pickupLink = catalogPage,
    Tag = Link,
    ...other
  } = props

  const dispatch = useDispatch()
  const { isOrderTypeDelivery } = useSelector((state) => state.orderType)

  const deliveryBtnClasses = cn(
    `order-type__btn order-type__btn_delivery ${deliveryClasses || ''}`,
    {
      active: isOrderTypeDelivery,
    }
  )

  const pickupBtnClasses = cn(
    `order-type__btn order-type__btn_pickup ${pickupClasses || ''}`,
    {
      active: !isOrderTypeDelivery,
    }
  )

  const orderTypeClasses = cn(`order-type ${classes || ''}`, {})

  return (
    <div {...other} className={orderTypeClasses}>
      <Tag
        to={deliveryLink}
        onClick={() => dispatch(setOrderTypeDelivery())}
        className={deliveryBtnClasses}
      >
        Привезите мне
      </Tag>
      <Tag
        to={pickupLink}
        onClick={() => dispatch(setOrderTypePickup())}
        className={pickupBtnClasses}
      >
        Заберу сам
      </Tag>
    </div>
  )
}
