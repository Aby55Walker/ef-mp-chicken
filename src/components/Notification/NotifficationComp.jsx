import React from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { ReactComponent as Done } from '../../assets/icons/done.svg';
import { ReactComponent as Error } from '../../assets/icons/error.svg';

export const NotificationComp = () => {
  const { notificationText, notificationTypeIsError } = useSelector(
    (state) => state.notification,
  );

  const notificationClasses = classNames('notification', {
    notification_error: notificationTypeIsError,
  });

  return (
    <div className={notificationClasses}>
      {notificationTypeIsError ? (
        <Error className="notification__icon" />
      ) : (
        <Done className="notification__icon" />
      )}

      <span className="notification__text">{notificationText}</span>
    </div>
  );
};
