import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { Transition } from '../Transition/Transition';
import { NotificationComp } from './NotifficationComp';

export const Notification = () => {
  const {
    notificationIsVisible,
    notificationText,
  } = useSelector((state) => state.notification);
  const dispatch = useDispatch();

  return (
    <Transition classNames="slide-from-top" toggle={notificationIsVisible && typeof notificationText !== 'object'}>
      <div className="notification__wrapper">
        <NotificationComp />
      </div>
    </Transition>
  );
};
