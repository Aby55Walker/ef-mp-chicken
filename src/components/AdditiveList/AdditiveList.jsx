import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import { Additive } from '../Additive/Additive';

export const AdditiveList = (props) => {
  const {
    classes,
    data = [],
    maxPieces,
    maxItems,
    onChange,
    groupName,
    ...other
  } = props;

  // количество выбранных кусочков
  const [currentPiecesSum, setCurrentPiecesSum] = useState(0);
  // количество выбранных карточек
  const [chosenItemsAmount, setChosenItemAmount] = useState(0);

  const [counts, setCounts] = useState({
    maxPieces,
    maxItems,
    minPieces: 0,
    allValues: {}, // все выбранные модификаторы, их количество и цена
  });

  const countChangeHandler = (val, id, price, name) => {
    setCounts((prevState) => ({
      ...prevState,
      maxPieces,
      allValues: { // все выбранные модификаторы, их количество и цена
        ...prevState.allValues,
        [id]: {
          val, price, name, groupName,
        },
      },
    }));
  };

  useEffect(() => {
    // считает сколько карточек выбрано
    setChosenItemAmount(Object.values(counts.allValues).filter((item) => item.val).length);
    // суммирует все выбранные добавки
    setCurrentPiecesSum(
      Object.keys(counts.allValues)
        .reduce((acc, curr) => acc + counts.allValues[curr].val, 0),
    );
  }, [counts]);

  useEffect(() => {
    if (onChange) {
      onChange(
        counts.allValues,
        { sum: currentPiecesSum, groupName },
        // передаёт выбранное количество кучсочков (для соусов жто maxItems)
        maxPieces ? +currentPiecesSum === +maxPieces : +currentPiecesSum === +maxItems,
      );
    }
  }, [counts, currentPiecesSum]);

  const blockClasses = cn(`additive-list ${classes || ''}`, {});

  return (
    <ul className={blockClasses} {...other}>
      {data.map((item, i) => (
        <li className="additive-list__item" key={item.id}>
          <Additive
            data={item}
            available={maxPieces - currentPiecesSum}
            counts={counts}
            maxItems={maxItems}
            chosenItemsAmount={chosenItemsAmount}
            initialValue={0}
            onCountChange={countChangeHandler}
          />
        </li>
      ))}
    </ul>
  );
};
