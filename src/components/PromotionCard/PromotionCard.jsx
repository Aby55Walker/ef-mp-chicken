import React from 'react'
import classNames from 'classnames'
import previewImg from '../../assets/images/promotion-1-preview.png'
import { Link, useLocation } from 'react-router-dom'
// import detailImg from '../../assets/images/promotion-1-detail.png'
// import testImg from '../../assets/images/main-card-1.jpg'

export const PromotionCard = (props) => {
  const {
    classes,
    data,
    index = 0,
    title = data?.title,
    description = data?.description,
    from = data?.activity?.from,
    to = data?.activity?.to,
    preview = data?.picture?.preview || previewImg,
    detail = data?.picture?.detail,
    children,
    ...other
  } = props

  const location = useLocation()
  const path = `${location.pathname}/stories-modal`

  const promoClasses = classNames(`promotion-card ${classes || ''}`, {})

  return (
    <div className={promoClasses} {...other}>
      {children ? (
        children
      ) : (
        <>
          <Link
            className={'promotion-card__preview'}
            to={`${path}/${index}`}
            title={title}
          >
            <img
              className={'promotion-card__img'}
              src={previewImg} // TODO брать картинку из data
              alt='акция'
            />
          </Link>
        </>
      )}
    </div>
  )
}
