// AdditiveCounter
import React, { useState } from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import { ReactComponent as PlusIcon } from '../../../assets/icons/plus.svg';
import { Counter } from '../../Counter/Counter';
import { AdditiveModal } from './AdditiveModal';
import { lg } from '../../../constants/breakpoints';

export const AdditiveCounter = (props) => {
  const {
    classes,
    full,
    openModal = false,
    onCloseModal,
    available,
    data,
    onChange,
    counts: {
      maxPieces,
      maxItems,
      minPieces,
      allValues,
    },
    ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const [count, setCount] = useState(0);
  const blockClasses = cn(`additive-counter ${classes || ''}`, { 'additive-counter--full': full || count });

  const onIncrement = (x) => {
    let transformedCount = 0;
    if (maxPieces) {
      transformedCount = x < 3 || available === 3 ? x + 2 : x;
    } else {
      transformedCount = x;
    }
    setCount(transformedCount);
    if (onChange) onChange(transformedCount);
  };
  const onDecrement = (x) => {
    let transformedCount = 0;
    if (maxPieces) {
      transformedCount = x < 3 ? 0 : x;
    } else {
      transformedCount = x;
    }
    setCount(transformedCount);
    if (onChange) onChange(transformedCount);
  };

  const onClose = (val) => {
    if (typeof val === 'number') {
      setCount(val);
      if (onChange) onChange(val);
    }
    if (onCloseModal) onCloseModal();
  };

  const onChangeValue = (x) => {
    if (typeof x === 'number') {
      setCount(x);
    }
  };

  const calcMaxPieces = () => (maxPieces ? (available + count || maxPieces - count) : maxItems);
  const calcDisabledPlus = () => (maxPieces ? (available < 3 && count === 0) || available === 0 : count === maxItems);

  return (
    <>
      <div className={blockClasses} {...other}>
        {full || count
          ? (
            <Counter
              amount={count}
              stateFull={false}
              disabledPlus={calcDisabledPlus()}
              noClean
              onDecrement={onDecrement}
              onIncrement={onIncrement}
              classes={blockClasses}
            />
          )
          : (
            <PlusIcon />
          )}
      </div>
      <AdditiveModal
        onClose={onClose}
        onChangeValue={onChangeValue}
        open={openModal}
        data={data}
        count={count}
        min={minPieces}
        max={calcMaxPieces()}
        invalidValues={maxPieces ? [
          1,
          2,
          available + count - 1, // предпоследнее значене
          available + count - 2, // значение перед предпоследним
        ] : []}
      />
    </>
  );
};
