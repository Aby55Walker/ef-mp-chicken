// AdditiveModal
import React, { useRef, useEffect, useState } from 'react';
import cn from 'classnames';
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock';
import { ReactComponent as ClearIcon } from '../../../assets/icons/clear.svg';
import { Transition } from '../../Transition/Transition';
import tempImage from '../../../assets/images/catalog/product-3.png';
import { RCSlider } from '../../RCSlider/RCSlider';

export const AdditiveModal = (props) => {
  const {
    open,
    onClose,
    data,
    classes,
    count = 0,
    onChangeValue,
    maxPieces,
    min = 3,
    max = 20,
    invalidValues,
    validValues,
    ...other
  } = props;

  const {
    name,
    price,
    img = data.image?.preview,
  } = data;

  const [value, setValue] = useState(count);
  const modalRef = useRef();

  useEffect(() => {
    const sideModal = document.querySelector('.side-modal');
    if (open) {
      disableBodyScroll(modalRef.current);

      if (sideModal) {
        sideModal.style = 'height: auto;';
      }
    }

    return () => {
      clearAllBodyScrollLocks();
      if (sideModal) {
        sideModal.style = '';
      }
    };
  }, [open]);

  const blockClasses = cn(`additive-modal ${classes || ''}`, { });

  const changeHandler = (val) => {
    setValue(val);
  };

  return (
    <Transition
      toggle={open}
      classNames="fade"
      onExited={() => onClose(value)}
    >
      <div className={blockClasses} {...other} ref={modalRef}>
        <div className="additive-modal__overlay" onClick={onClose} />
        <div className="additive-modal__content">
          <ClearIcon className="additive-modal__close" onClick={onClose} />

          <div className="additive-modal__header">
            <div className="additive-modal__img-wp">
              <img className="additive-modal__img" src={tempImage} alt={name} />
            </div>
            <h4 className="additive-modal__title">{name}</h4>
            <span className="additive-modal__price">+{Math.floor(price)}&#160;&#8381;/кус</span>
          </div>

          <b className="additive-modal__text">Выберите от&#160;{min} до&#160;{max} кусочков</b>

          <RCSlider min={3} invalidValues={invalidValues} validValues={validValues} max={max} classes="additive-modal__slider" count={count} onChange={changeHandler} />

        </div>
      </div>

    </Transition>

  );
};
