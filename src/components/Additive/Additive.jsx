import React, { useState } from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import tempImage from '../../assets/images/catalog/product-3.png';
import { AdditiveCounter } from './includes/AdditiveCounter';
import { lg } from '../../constants/breakpoints';

export const Additive = (props) => {
  const {
    classes,
    data = [],
    counts, // объект с количеством доступных кусочков и добавок
    onCountChange,
    initialValue = 0,
    available,
    maxItems,
    chosenItemsAmount, /// количество выбранных глазировок
    ...other
  } = props;

  const {
    name,
    pictures: { detail } = {},
    id,
    price,
  } = data;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const [hover, setHover] = useState(false);
  const [openModal, changeOpenModal] = useState(false);
  const [value, setValue] = useState(0);

  const onMouseEnter = (e) => {
    if (available === 0 && !value) return;
    setHover(screenWidth > lg);
  };
  const onMouseLeave = (e) => {
    setHover(false);
  };

  const openModalHandle = () => {
    if (screenWidth <= lg && !openModal) {
      changeOpenModal(true);
    }
  };

  const onCloseModal = () => {
    changeOpenModal(false);
  };

  const changeHandler = (val) => {
    setValue(val);
    if (onCountChange) onCountChange(val, id, price * val, name);
  };

  const counterClasses = cn('', {
    disabled: (maxItems === chosenItemsAmount || available === 0) && !value,
  });

  const blockClasses = cn(`additive ${classes || ''}`, {
    disabled: (maxItems === chosenItemsAmount || available === 0) && !value,
  });

  return (
    <div
      className={blockClasses}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={openModalHandle}
      {...other}
    >
      <div className="additive__img-wp">
        <img className="additive__img" src={tempImage} alt={name} />
      </div>
      <h4 className="additive__title">{name}</h4>
      <span className="additive__price">+{Math.floor(price)} ₽/шт</span>

      <AdditiveCounter
        value={initialValue}
        counts={counts}
        onChange={changeHandler}
        classes={counterClasses}
        full={hover}
        data={data}
        available={available}
        openModal={openModal}
        onCloseModal={onCloseModal}
      />
    </div>
  );
};
