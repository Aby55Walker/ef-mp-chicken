import React, { useEffect, useRef, useState } from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import { Slider } from '../Slider/Slider';
import { useSmoothScroll } from '../../hooks/useSmoothScroll';
import { CatalogNavItem } from './CatalogNavItem';
import { useStickyElem } from '../../functions/makeElementSticky';
import { lg } from '../../constants/breakpoints';

const sliderSettings = {
  spaceBetween: 6,
  slidesPerView: 'auto',
  freeMode: true,
};

export const CatalogNav = (props) => {
  const {
    activeItem, classes, data = [], onScrollStart, onScrollStop,
  } = props;

  const navRef = useRef(null);
  const { screenWidth } = useSelector((state) => state.screenSize);
  const [swiper, setSwiper] = useState(null);

  const stickyClass = useStickyElem(navRef.current, 0);
  const scrollStartHandler = () => {
    if (onScrollStart) onScrollStart();
  };
  const scrollStopHandler = () => {
    if (onScrollStop) onScrollStop();
  };
  useSmoothScroll(scrollStartHandler, scrollStopHandler);

  useEffect(() => {
    if (!swiper) return;
    swiper.slideTo(data.findIndex((category) => category.name === activeItem));
  }, [activeItem]);

  const navClasses = cn(`catalog-nav ${classes || ''}`, {
    sticky: stickyClass,
  });

  const renderList = data.map((category) => (
    <CatalogNavItem
      key={category.name}
      category={category}
      activeItem={activeItem}
    />
  ));

  return (
    <div
      className="catalog-nav__wrapper"
      style={{
        height: stickyClass ? `${navRef.current.offsetHeight}px` : '',
      }}
    >
      <nav ref={navRef} className={navClasses}>
        <ul className="catalog-nav__list">
          {/* без слайдера для мобилок */}
          <Slider
            onInit={(swiperInstance) => setSwiper(swiperInstance)}
            sliderClasses="catalog-nav__slider"
            settings={sliderSettings}
          >
            {renderList}
          </Slider>

        </ul>
      </nav>
    </div>
  );
};
