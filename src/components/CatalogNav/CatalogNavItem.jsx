import React, { useEffect, useRef } from 'react';
import cn from 'classnames';

export const CatalogNavItem = ({ activeItem, category }) => {
  const linkClass = cn('catalog-nav__link', {
    active: activeItem === category.name,
  });

  return (
    <li className="catalog-nav__item">
      <a className={linkClass} href={`#${category.name}`}>
        {category.name}
      </a>
    </li>
  );
};
