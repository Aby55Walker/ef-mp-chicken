// RCSlider
import React, { useState, useEffect, useRef } from 'react';
import cn from 'classnames';
import Slider, { Handle } from 'rc-slider';
import { ReactComponent as PlusIcon } from '../../assets/icons/plus.svg';
import { ReactComponent as MinusIcon } from '../../assets/icons/minus.svg';

export const RCSlider = (props) => {
  const {
    min = 0,
    max = 1,
    step = 1,
    validValues = [], // допустимые значения (без zero и max)
    invalidValues = [], // недопустимые значения
    zero = 0,
    count = min || zero,
    onChange,
    classes,
    ...other
  } = props;

  const refHandle = useRef();
  const [tipStyle, setTipStyle] = useState({});
  const [value, setValue] = useState(count);
  const [marks, setMarks] = useState({ [zero]: zero, [min]: min, [max]: max });

  const getMarksFromValidValues = () => {
    let values = validValues;
    values.unshift(zero); // добавить zero в начало
    values.push(max); // добавить max в конец
    values = values.map((x) => [x, x]);

    return Object.fromEntries(values);
  };

  const getMarksFromInvalidValues = () => {
    let values = [];
    for (let i = zero; i <= max; i++) {
      values.push(i);
    }
    values = values.filter((item) => !invalidValues.includes(item));
    values = values.map((x) => [x, x]);

    return Object.fromEntries(values);
  };

  const updTipStyle = () => {
    const newStyle = refHandle?.current?.handle?.style;

    if (newStyle) {
      const { left, right } = newStyle;
      setTipStyle({
        left: `${left}`,
        right: `${right}`,
      });
    }
  };

  const changeHandle = (x) => {
    setValue(x);
  };

  useEffect(() => {
    if (validValues.length) {
      setMarks(getMarksFromValidValues());
    } else if (invalidValues.length) {
      setMarks(getMarksFromInvalidValues());
    }
    updTipStyle();
  }, []);

  useEffect(() => {
    setTimeout(() => { updTipStyle(); }, 0);
    if (onChange) onChange(value);
  }, [value]);

  const handleMinus = () => {
    const result = value - 1;
    if (result >= zero && invalidValues.indexOf(result) === -1) {
      setValue(result);
    }
  };

  const handlePlus = () => {
    const result = value + 1;
    if (result <= max && invalidValues.indexOf(result) === -1) {
      setValue(result);
    }
  };

  const handle = ({
    value, dragging, index, ...restProps
  }) => <Handle value={value} {...restProps} ref={refHandle} />;

  const wrapClasses = cn(`${classes || ''}`, {});

  return (
    <div className={wrapClasses}>
      <div className="rc-slider-wp">
        <button className="rc-slider__btn rc-slider__minus ">
          <MinusIcon />
        </button>

        <div className="rc-slider__inner">
          <span className="rc-slider__tip" style={tipStyle}>
            {value}
          </span>
          <Slider
            {...other}
            className="rc-slider"
            min={zero}
            max={max}
            handle={handle}
            onChange={changeHandle}
            step={validValues.length || invalidValues.length ? null : step}
            marks={marks}
            value={value}
          />
        </div>

        <button className="rc-slider__btn rc-slider__plus">
          <PlusIcon />
        </button>
      </div>
    </div>
  );
};
