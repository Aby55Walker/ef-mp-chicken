import React from 'react';
import classNames from 'classnames';

export const CircleRadioBtn = (props) => {
  const {
    value,
    text,
    time,
    prompt, // серый текст в скобках
    onChange,
    name,
    register, // из useForm
    disabled,
    active,
    children,
    classes,
    hide,
    ...other
  } = props;

  const changeHandler = ({ target }) => {
    if (disabled) return;
    if (onChange) onChange(target);
  };

  const labelClasses = classNames(`circle-radio ${classes || ''}`, {
    active,
    'circle-radio_disabled': disabled,
  });
  const inputClasses = classNames('circle-radio__input', {
    active,
  });
  const circleClasses = classNames('circle-radio__circle', {
    active,
  });

  return (
    <label {...other} className={labelClasses}>
      <input
        ref={register ? register({ required: true }) : undefined}
        className={inputClasses}
        checked={active}
        onChange={changeHandler}
        value={value}
        name={name}
        disabled={disabled}
        type="radio"
      />
      <span className={circleClasses} />
      <span className="circle-radio__decs">
        <span className="circle-radio__text">{text}</span>
        <span className="circle-radio__prompt"> {prompt} </span>
        {time && <span className="choose-rest__time">{time}</span>}
      </span>
    </label>
  );
};
