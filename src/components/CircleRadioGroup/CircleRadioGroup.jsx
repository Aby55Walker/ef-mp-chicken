import React, { useState } from 'react'
import { CircleRadioBtn } from '../CircleRadioBtn/CircleRadioBtn'
import { useForm } from 'react-hook-form'
import { useFormParams } from '../../constants/validate'

export const CircleRadioGroup = (props) => {
  const {
    triggerValidation,
    onChange,
    items,
    name,
    register,
    initialValue,
    classes,
    unselected,
    ...other
  } = props

  const [selected, setSelected] = useState(unselected ? '' : initialValue)

  const changeHandler = (value) => {
    setSelected(value)
    triggerValidation(name)
    if (onChange) onChange(value)
  }

  return (
    <div
      {...other}
      className={`circle-radio-croup${classes ? ' ' + classes : ''}`}
    >
      {items.map(({ value, text, prompt, hide }) => {
        // не рендерить элементы, с параметром hide: true
        if (hide) return null
        else {
          return (
            <CircleRadioBtn
              key={value}
              value={value}
              text={text}
              name={name}
              prompt={prompt}
              register={register}
              selected={selected}
              onChange={changeHandler}
            />
          )
        }
      })}
    </div>
  )
}
