import React from 'react';
import img from '../../assets/images/coupon-1.png';
import { Btn } from '../Btn/Btn';
import { InternalLink } from '../InternalLink/InternalLink';
import { catalogPage } from '../../constants/pagelinks';

export const Coupon = (props) => {
  const {
    data,
  } = props;

  return (
    <>
      <div className="coupon">
        <div className="coupon__preview">
          <img className="coupon__img" src={img} alt="Купон" />
        </div>

        <div className="coupon__info">
          <p className="coupon__code">3819</p>

          <div className="coupon__desc">
            <p>
              Хочешь сытно позавтракать, но время есть только на глоток кофе?{' '}
              Тогда этот купон специально для тебя!
            </p>
            <p>
              Начни утро с капучино и доната
              по супер цене!
            </p>
          </div>

          <div className="coupon__discount">
            <span className="coupon__price coupon__price_old">320 ₽</span>
            <span className="coupon__price">180 ₽</span>
          </div>
        </div>

        <InternalLink
          prodId={1}
          img={img}
          title="Стрипсы: 4 сочных кусочка"
          desc="Выберите глазировку"
        />

      </div>
      <div className="coupon__btn-wrapper">
        <Btn tag="Link" to={catalogPage} text="В корзину" fullWidth centred primary />
      </div>
    </>

  );
};
