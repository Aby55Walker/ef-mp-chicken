import React from 'react';
import { SideModal } from '../SideModal/SideModal';
import { HelpSuccess } from './includes/HelpSuccess';

export const HelpSuccessModal = () => (
  <SideModal>
    <HelpSuccess />
  </SideModal>
);
