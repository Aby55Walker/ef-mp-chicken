import React from 'react';
import { useSelector } from 'react-redux';
import { lg } from '../../../constants/breakpoints';
import sticker from '../../../assets/images/sticker-smile.png';

export const HelpSuccess = (props) => {
  const {
    ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);

  return (
    <div className="help-success" {...other}>
      <div className="help-success__sticker-wp">
        <img className="help-success__sticker" src={sticker} alt="Спасибо!" />
      </div>

      <h3 className="help-success__title">
        {screenWidth > lg ? <span>Спасибо!<br /></span> : ''}
        Мы&nbsp;ответим вам в&nbsp;течении 10&nbsp;минут
      </h3>
    </div>
  );
};
