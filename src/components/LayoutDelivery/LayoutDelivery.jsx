import React from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { FooterBottom } from '../Footer/includes/FooterBottom';
import { Header } from '../Header/Header';
import { lg } from '../../constants/breakpoints';
import { HeaderDelivery } from './includes/HeaderDelivery';
import { Recipient } from './includes/Recipient/Recipient';
import { DeliveryAddress } from './includes/DeliveryAddress/DeliveryAddress';
import { PaymentMethod } from './includes/PaymentMethod/PaymentMethod';
import { Pickup } from './includes/Pickup/Pickup';

export const LayoutDelivery = (props) => {
  const { path = '/' } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const showSlideModal = useSelector((state) => state.showSlideModal);
  const isOrderTypeDelivery = useSelector((state) => state.orderType?.isOrderTypeDelivery);
  const isOrderTypePickup = useSelector((state) => state.orderType?.isOrderTypePickup);

  const pageClass = classNames('page page-delivery', {
    'page--slide': showSlideModal,
  });
  const match = useRouteMatch();

  return (
    <div className={pageClass}>
      {screenWidth <= lg && <Header />}
      <HeaderDelivery path={match.url} />

      <main className="page__content">
        <div className="container">
          <Route
            path={match.url}
            render={() => (
              <Switch>
                <Route path={`${match.url}/address`}>
                  {isOrderTypeDelivery && !isOrderTypePickup ? (
                    <DeliveryAddress path={match.url} classes="page-delivery__inner" />
                  ) : (
                    <Pickup path={match.url} classes="page-delivery__inner" />
                  )}
                </Route>

                <Route path={`${match.url}/payment`}>
                  <PaymentMethod classes="page-delivery__inner" />
                </Route>

                <Route path={`${match.url}/`}>
                  <Recipient path={match.url} classes="page-delivery__inner" />
                </Route>
              </Switch>
            )}
          />
        </div>
      </main>

      <footer>
        <FooterBottom />
      </footer>
    </div>
  );
};
