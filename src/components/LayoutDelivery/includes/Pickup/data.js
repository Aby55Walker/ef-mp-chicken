import { pickupOrderType, restaurantOrderType } from '../../../../constants/orderTypes';

export const whereEat = [
  {
    value: `${pickupOrderType}`,
    text: 'В зале',
  },
  {
    value: `${restaurantOrderType}`,
    text: 'Возьму с собой',
  },
];

export const whatTime = [
  {
    value: 'soon',
    text: 'Сразу как будет готово',
    // prompt: '(~17:30)'
  },
  {
    value: 'selectedTime',
    text: 'К выбранному времени',
  },
];
