/* eslint-disable camelcase */
import React, { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Btn } from '../../../Btn/Btn';
import { Address } from '../../../Address/Address';
import { Help } from '../../../Help/Help';
import { useFormParams } from '../../../../constants/validate';
import { md } from '../../../../constants/breakpoints';
import { whereEat } from './data';
import { ReactComponent as IconPin } from '../../../../assets/icons/pin.svg';
import { ByWhatTime } from '../../../ByWhatTime/ByWhatTime';
import { WithRadio } from '../../../HOCS/WithRadio';
import { CircleRadioBtn } from '../../../CircleRadioBtn/CircleRadioBtn';
import { cartPreorderRequest } from '../../../../store/modules/cart/cartPreorder/actions';
import {
  setReadyTimeOrderRegistration,
  setRestaurantIdOrderRegistration,
  setTypeOrderRegistration,
  setAddressOrderRegistration,
} from '../../../../store/modules/orderRegistration/actions';
import { restaurantsRequest, setRestaurantIndexId } from '../../../../store/modules/Restaurants/actions';

export const Pickup = (props) => {
  const { classes = '', path, ...other } = props;

  const refForm = useRef();
  const dispatch = useDispatch();
  const history = useHistory();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const { cartId } = useSelector((state) => state.cart.addItem);
  const preorder = useSelector((state) => state.cart.preorder.data);
  const { index } = useSelector((state) => state.restaurants.selectedRestaurant);
  const restaurants = useSelector((state) => state.restaurants.data);
  const { id } = useSelector((state) => state.restaurants.selectedRestaurant);
  const [day, setDay] = useState(null);
  const [time, setTime] = useState(null);
  const [selectedRestaurant, setSelectedRestaurant] = useState({});
  const {
    handleSubmit, register, formState, triggerValidation,
  } = useForm(useFormParams);

  useEffect(() => {
    dispatch(cartPreorderRequest({ cartId })); // получить данные для блока "Ко скольки готовить"
    if (!restaurants.length) dispatch(restaurantsRequest()); // получить список ресторанов
  }, []);

  useEffect(() => {
    const data = restaurants.length ? (index ? restaurants[index] : restaurants[0]) : {};
    const { address, work_status, id } = data;

    dispatch(setRestaurantIndexId({ index: index || 0, id }));

    setSelectedRestaurant({ address, work_status });
  }, [restaurants, index]);

  const onDayChange = (x) => {
    setDay(x);
  };
  const onTimeChange = (x) => {
    setTime(x);
  };

  const onSubmit = (data) => {
    const { orderType } = data;

    // тип заказа
    if (orderType) dispatch(setTypeOrderRegistration(+orderType));

    dispatch(setReadyTimeOrderRegistration(day && time ? `${day} ${time}` : null));
    dispatch(setRestaurantIdOrderRegistration(id));
    dispatch(
      setAddressOrderRegistration({
        address: selectedRestaurant.address,
        type: 'office', // тип адреса при самовывозе
      }),
    );

    // перейти на стр Способ оплаты
    history.push(`${path}/payment`);
  };

  return (
    <section className={`pickup${` ${classes}`}`} {...other}>
      <form noValidate ref={refForm} onSubmit={handleSubmit(onSubmit)} className="pickup__form page-delivery__flex-wrap">
        <h2 className="pickup__title pickup__title-address page-delivery__title">Самовывоз из&nbsp;ресторана</h2>
        <div className="pickup__radio-group pickup__address-wp">
          <Address
            editLink="/modal/choose-rest"
            address={selectedRestaurant?.address || 'Выбрать ресторан'}
            edit
            buildingIcon={<IconPin />}
            classes="pickup__address"
          />
        </div>

        <ByWhatTime
          preorder={preorder}
          disabledToday={selectedRestaurant?.work_status || false}
          register={register}
          triggerValidation={triggerValidation}
          classes="pickup__time-wp pickup__radio-group"
          onDayChange={onDayChange}
          onTimeChange={onTimeChange}
        />

        <h2 className="pickup__title page-delivery__title">Где будете есть?</h2>
        <div className="pickup__radio-group">
          <WithRadio
            list={whereEat}
            name="orderType"
            register={register}
            Component={CircleRadioBtn}
            triggerValidation={triggerValidation}
          />
        </div>

        <Btn
          disabled={!formState.isValid || !selectedRestaurant?.address}
          onClick={handleSubmit(onSubmit)}
          centred
          fullWidth
          primary
          classes="pickup__next page-delivery__next"
          text="Продолжить"
        />

        <Help type={2} text={screenWidth <= md && 'Что-то не получается?'} classes="pickup__help page-delivery__help" />
      </form>
    </section>
  );
};
