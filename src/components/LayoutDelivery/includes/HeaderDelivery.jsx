import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import {
  Route, Link, useLocation, useRouteMatch,
} from 'react-router-dom';
import { Logo } from '../../Logo/Logo';
import { lg } from '../../../constants/breakpoints';
import deleteSlashAtTheEnd from '../../../functions/deleteSlashAtTheEnd';
import { ReactComponent as IconDone } from '../../../assets/icons/done.svg';

export const HeaderDelivery = (props) => {
  const location = useLocation();
  const match = useRouteMatch(location.pathname);
  const { loggedIn } = useSelector((state) => state.auth);
  const isOrderTypeDelivery = useSelector(
    (state) => state.orderType?.isOrderTypeDelivery,
  );

  const [breadcrumbs, setBreadcrumbs] = useState([
    { text: 'Получатель', link: '/' },
    { text: 'Доставка', link: '/address' },
    { text: 'Способ оплаты', link: '/payment' },
  ]);

  useEffect(() => {
    setBreadcrumbs([
      breadcrumbs[0],
      // "Доставка", если пользователь авторизован и выбрал доставку, а не самовывоз
      {
        text: loggedIn && isOrderTypeDelivery ? 'Доставка' : 'Самовывоз',
        link: '/address',
      },
      breadcrumbs[2],
    ]);
  }, [loggedIn, isOrderTypeDelivery]);

  const { classes = '', path = location.pathname, ...other } = props;

  const store = useSelector((state) => state);
  const headerClass = classNames(
    `header-delivery ${classes || ''}`,
    {},
  );
  const [index, setIndex] = useState(0);

  useEffect(() => {
    changeIndex();
  });

  // определить index тукущей ссылки в хлебных крошках
  const changeIndex = () => {
    const arr = match.url.split('/delivery');
    const last = arr[arr.length - 1];
    const currentCount = breadcrumbs.findIndex(
      (el, i) => el.link === deleteSlashAtTheEnd(last),
    );

    setIndex(currentCount || 0);
  };

  return (
    <header className={headerClass} {...other}>
      <div className="container header-delivery__container">
        {store.screenWidth >= lg && (
          <Link to="/" className="header-delivery__logo">
            <Logo />
          </Link>
        )}

        <nav className="header-delivery__breadcrumbs">
          <ol className="header-delivery__list">
            <Route
              path={path}
              render={() => (
                <>
                  {breadcrumbs.map(({ text, link }, i) => {
                    const primary = index >= i || i === 0;
                    const done = index > i;
                    const Tag = index > i ? Link : 'p';

                    const itemClass = classNames('header-delivery__item', {
                      'header-delivery__item_primary': primary,
                      'header-delivery__item_done': done,
                    });
                    const nameClass = classNames('header-delivery__name', {
                      'header-delivery__name_primary': primary,
                      'header-delivery__name_done': done,
                    });
                    const textClass = classNames('header-delivery__text', {
                      'header-delivery__text_primary': primary,
                    });

                    return (
                      <li key={i} className={itemClass}>
                        <Tag
                          className={textClass}
                          to={
                            index > i
                              ? `${deleteSlashAtTheEnd(path)}${link}`
                              : null
                          }
                          exact={index > i ? 'true' : 'false'}
                        >
                          <span className="header-delivery__status">
                            {done ? (
                              <IconDone className="header-delivery__icon" />
                            ) : (
                              <span className="header-delivery__count">
                                {i + 1}
                              </span>
                            )}
                          </span>
                          <span className={nameClass}>{text}</span>
                        </Tag>
                      </li>
                    );
                  })}
                </>
              )}
            />
          </ol>
        </nav>
      </div>
    </header>
  );
};
