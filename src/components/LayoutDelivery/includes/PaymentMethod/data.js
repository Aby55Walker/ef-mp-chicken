// методы оплаты для самовывоза
export const data = [
  {
    value: 'cart',
    text: 'Оплата картой',
  },
  {
    value: 'applePay',
    text: 'Apple Pay',
  },
  {
    value: 'googlePay',
    text: 'Google Pay',
  },
];
