import React, { useEffect, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Btn } from '../../../Btn/Btn';
import { Help } from '../../../Help/Help';
import { useFormParams } from '../../../../constants/validate';
import { md } from '../../../../constants/breakpoints';
import { loadGooglePay } from '../../../../functions/loadGooglePay';
import { loadApplePay } from '../../../../functions/loadApplePay';
import checkAvailabilityGooglePay from '../../../../functions/checkAvailabilityGooglePay';
import checkAvailabilityApplePay from '../../../../functions/checkAvailabilityApplePay';
import {
  paymentMethod,
  addPaymentGooglePay,
  addPaymentApplePay,
} from '../../../../store/actions';
import { WithRadio } from '../../../HOCS/WithRadio';
import { CircleRadioBtn } from '../../../CircleRadioBtn/CircleRadioBtn';
import { cartCheckoutRequest } from '../../../../store/modules/cart/cartCheckout/actions';
import { setRestaurantIdOrderRegistration } from '../../../../store/modules/orderRegistration/actions';
import { currentOrderPage } from '../../../../constants/pagelinks';

export const PaymentMethod = (props) => {
  const { classes = '', ...other } = props;

  const { delivery, pickup } = useSelector((state) => state.paymentMethod);
  const orderRegistration = useSelector((state) => state.orderRegistration);
  const { screenWidth } = useSelector((state) => state.screenSize);
  const { isOrderTypeDelivery } = useSelector((state) => state.orderType);
  const dispatch = useDispatch();
  const history = useHistory();
  const refForm = useRef();
  const {
    handleSubmit, register, formState, triggerValidation,
  } = useForm(
    useFormParams,
  );

  useEffect(() => {
    // *** загрузить скрипт для GooglePay
    // loadGooglePay(() => {
    //   // если GooglePay поддерживается добавить этот пункт в способы оплаты
    //   checkAvailabilityGooglePay((value) => {
    //     if (value) dispatch(addPaymentGooglePay());
    //   });
    // });

    // // *** загрузить скрипт для ApplePay
    // loadApplePay(() => {
    //   checkAvailabilityApplePay((value) => {
    //     if (value) dispatch(addPaymentApplePay());
    //   });
    // });
  }, []);

  const onSubmit = ({ payment }) => {
    const finalData = {
      ...orderRegistration,
      payment_type: payment,
    };

    // оформить заказ
    dispatch(cartCheckoutRequest(
      finalData,
      history,
      // currentOrderPage,
    ));
  };

  const onChange = (value) => {
    if (value) dispatch(paymentMethod(value));
  };

  return (
    <section className={`payment-method${` ${classes}`}`} {...other}>
      <h2 className="payment-method__title page-delivery__title">
        Способ оплаты
      </h2>

      <form
        ref={refForm}
        onSubmit={handleSubmit(onSubmit)}
        className="payment-method__form"
      >
        {isOrderTypeDelivery ? (
          <WithRadio
            triggerValidation={triggerValidation}
            list={Object.values(delivery)} // способы оплаты для доставки
            name="payment"
            register={register}
            onChange={onChange}
            Component={CircleRadioBtn}
          />
        ) : (
          <WithRadio
            triggerValidation={triggerValidation}
            list={Object.values(pickup)} // способы оплаты для самовывоза
            name="payment"
            register={register}
            onChange={onChange}
            Component={CircleRadioBtn}
          />
        )}

        <div className="page-delivery__flex-wrap">
          <Btn
            disabled={!formState.isValid}
            onClick={handleSubmit(onSubmit)}
            centred
            fullWidth
            primary
            classes="payment-method__next page-delivery__next"
            text="Перейти к оплате"
          />

          <Help
            type={2}
            text={screenWidth <= md && 'Что-то не получается?'}
            classes="payment-method__help page-delivery__help"
          />
        </div>

      </form>
    </section>
  );
};
