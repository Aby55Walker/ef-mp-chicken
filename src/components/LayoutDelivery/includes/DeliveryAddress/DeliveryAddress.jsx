/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Btn } from '../../../Btn/Btn';
import { Help } from '../../../Help/Help';
import { md } from '../../../../constants/breakpoints';
import { AddressForm } from '../../../AddressForm/AddressForm';
import { radioButtons, circleRadioBtnsWithoutTime } from '../../../AddAddress/data';
import { ByWhatTime } from '../../../ByWhatTime/ByWhatTime';
import { setAddressOrderRegistration, setReadyTimeOrderRegistration, setTypeOrderRegistration } from '../../../../store/modules/orderRegistration/actions';
import { cartPreorderRequest } from '../../../../store/modules/cart/cartPreorder/actions';
import { deliveryOrderType } from '../../../../constants/orderTypes';

export const DeliveryAddress = (props) => {
  const match = useRouteMatch();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const addresses = useSelector((state) => state.user.data.addresses);
  const { cartId } = useSelector((state) => state.cart.addItem);
  const preorder = useSelector((state) => state.cart.preorder.data);
  const dispatch = useDispatch();
  const history = useHistory();
  const [day, setDay] = useState(null);
  const [time, setTime] = useState(null);

  const { path = match.url, classes = '', ...other } = props;

  useEffect(() => {
    dispatch(cartPreorderRequest({ cartId }));
  }, []);

  const onSubmit = (data) => {
    const {
      address, comment, flat, floor, isIntercomBroken, porch, room, type,
    } = data;

    dispatch(
      setAddressOrderRegistration({
        address,
        type,
        // eslint-disable-next-line object-shorthand
        ...(flat && { flat: flat }),
        // eslint-disable-next-line object-shorthand
        ...(porch && { porch: porch }),
        // eslint-disable-next-line object-shorthand
        ...(floor && { floor: +floor }),
        // eslint-disable-next-line object-shorthand
        ...(room && { room: room }),
        ...(isIntercomBroken && { is_intercom_broken: isIntercomBroken === 'true' }),
        ...(isIntercomBroken && { call_intercom: !(isIntercomBroken === 'true') }),
        // eslint-disable-next-line object-shorthand
        ...(comment && { comment: comment }),
      }),
    );

    dispatch(setTypeOrderRegistration(deliveryOrderType));

    dispatch(setReadyTimeOrderRegistration(day && time ? `${day} ${time}` : null));

    // перейти на стр Способ оплаты
    history.push(`${path}/payment`);
  };

  const onDayChange = (x) => {
    setDay(x);
  };
  const onTimeChange = (x) => {
    setTime(x);
  };

  const DeliveryAddressFooter = (disabled) => (
    <div className="page-delivery__flex-wrap delivery-address__buttons">
      <Btn disabled={disabled?.disabled} centred fullWidth primary classes="delivery-address__next page-delivery__next" text="Продолжить" />
      <Help type={2} text={screenWidth <= md && 'Что-то не получается?'} classes="delivery-address__help page-delivery__help" />
    </div>
  );

  return (
    <section className={`delivery-address${` ${classes}`}`} {...other}>
      <h2 className="delivery-address__title page-delivery__title">Адрес доставки</h2>

      <AddressForm
        radioButtons={radioButtons}
        onSubmit={onSubmit}
        circleRadioBtns={circleRadioBtnsWithoutTime}
        initialValues={addresses.length ? addresses[0] : false}
      >
        <ByWhatTime
          preorder={preorder}
          classes="delivery-address__by-what-time"
          onDayChange={onDayChange}
          onTimeChange={onTimeChange}
        />

        <DeliveryAddressFooter />
      </AddressForm>
    </section>
  );
};
