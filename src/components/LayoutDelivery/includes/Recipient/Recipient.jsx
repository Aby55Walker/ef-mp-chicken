/* eslint-disable camelcase */
import React, { useEffect, useRef } from 'react';
import {
  useRouteMatch, Link, useLocation, useHistory,
} from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { Btn } from '../../../Btn/Btn';
import { Input } from '../../../Input/Input';
import { Help } from '../../../Help/Help';
import {
  telRules,
  nameRules,
  useFormParams,
} from '../../../../constants/validate';
import { catalogPage, modalHelpOrder } from '../../../../constants/pagelinks';
import { setContactOrderRegistration } from '../../../../store/modules/orderRegistration/actions';
import { md } from '../../../../constants/breakpoints';
import deleteSlashAtTheEnd from '../../../../functions/deleteSlashAtTheEnd';
import { makeUnmaskedPhone } from '../../../../functions/makeUnmaskedPhone';

export const Recipient = (props) => {
  const match = useRouteMatch();
  const location = useLocation();
  const history = useHistory();
  const refForm = useRef();
  const dispatch = useDispatch();

  const {
    classes = '',
    path = match.url,
    ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const userName = useSelector((state) => state.user.data?.first_name);
  const userPhone = useSelector((state) => state.user.data?.phone);
  const authPhone = useSelector((state) => state.auth.phone);
  const { sum_total, items } = useSelector((state) => state.cart.content.data);
  const fixedPrice = sum_total ? sum_total.replace(/[^0-9\.]+/g, '').slice(0, -3) : '';

  const {
    handleSubmit,
    register,
    errors,
    triggerValidation,
    clearError,
    formState,
  } = useForm(useFormParams); // валидация формы

  useEffect(() => {
    triggerValidation(); // fix бага с input tel
  }, []);

  const onSubmit = (data) => {
    const { firstName, tel } = data;

    dispatch(setContactOrderRegistration(
      {
        name: firstName,
        phone: tel && `${makeUnmaskedPhone(tel)}`,
      },
    ));
    history.push(`${path}/address`);
  };

  const onChange = (e, name) => {
    clearError([name]);
    triggerValidation(name);
  };

  const onClear = (name) => {
    clearError([name]);
  };

  return (
    <section className={`recipient${` ${classes}`}`} {...other}>
      <div className="recipient__order">
        <div className="recipient__order-header">
          <h3 className="recipient__order-title">Ваш заказ</h3>
          <Link className="recipient__change" to={catalogPage}>
            Изменить
          </Link>
        </div>

        {items?.length
          ? (
            <p className="recipient__wrap">
              <span className="">{items.length} товаров на&nbsp;сумму</span>
              <span className="">{fixedPrice}&#160;&#8381;</span>
            </p>
          ) : null}

        <p className="recipient__wrap recipient__discount">
          <span>Скидка по&nbsp;купонам</span>
          <span>-400&#160;&#8381;</span>
        </p>

        <p className="recipient__wrap recipient__sum">
          <span>Сумма к&nbsp;оплате</span>
          <span className="">{fixedPrice}&#160;&#8381;</span>
        </p>
      </div>

      <h2 className="recipient__title page-delivery__title">Контактное лицо</h2>

      <form
        ref={refForm}
        onSubmit={handleSubmit(onSubmit)}
        className="recipient__form page-delivery__flex-wrap"
      >
        <Input
          ref={register(nameRules)}
          value={userName}
          name="firstName"
          error={errors?.firstName}
          errorText={errors?.firstName?.message}
          label="Имя"
          classes="recipient__input"
          onChange={(e) => onChange(e, 'firstName')}
          onClear={() => onClear('firstName')}
        />

        <Input
          ref={register(telRules)}
          mask="tel"
          disabled={!!userPhone || authPhone}
          value={userPhone || authPhone}
          name="tel"
          error={errors?.tel}
          errorText={errors?.tel?.message}
          label="Телефон"
          classes="recipient__input"
          onChange={(e) => onChange(e, 'tel')}
          onClear={() => onClear('tel')}
        />

        <Btn
          disabled={!formState.isValid}
          centred
          fullWidth
          primary
          classes="recipient__next page-delivery__next"
          text="Продолжить"
        />

        <Help
          type={2}
          to={`${deleteSlashAtTheEnd(location.pathname)}${modalHelpOrder}`}
          text={screenWidth <= md && 'Что-то не получается?'}
          classes="recipient__help page-delivery__help"
        />
      </form>
    </section>
  );
};
