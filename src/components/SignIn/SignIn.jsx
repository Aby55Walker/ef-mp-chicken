import React, { useEffect, useRef, useState } from 'react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Btn } from '../Btn/Btn';
import { Input } from '../Input/Input';
import { telRules, useFormParams } from '../../constants/validate';
import { modalAnimationDuration } from '../../constants/animation';

export const SignIn = (props) => {
  const {
    loading,
    onSubmit,
    title = 'Вход или регистрация',
    btnText = 'Получить код',
    ...other
  } = props;

  const refForm = useRef();

  const {
    handleSubmit,
    register,
    errors,
    triggerValidation,
    clearError,
    formState,
  } = useForm(useFormParams); // валидация формы

  useEffect(() => {
    setTimeout(() => {
      if (refForm
        && refForm.current
        && refForm.current.elements
        && refForm.current.elements[0]) refForm.current.elements[0].focus();
    }, modalAnimationDuration);
  }, []);

  const submitHandler = ({ phone }, e) => {
    e.preventDefault();
    if (onSubmit) onSubmit(phone);
  };

  const onChange = (e, name) => {
    clearError([name]);
    triggerValidation(name);
  };

  const onClear = (name) => {
    clearError([name]);
  };

  return (
    <>
      <h2 className="side-modal__title">{title}</h2>

      <form ref={refForm} onSubmit={handleSubmit(submitHandler)} {...other}>
        <Input
          ref={register(telRules)}
          mask="tel"
          name="phone"
          error={errors?.phone}
          errorText={errors?.phone?.message}
          label="Телефон"
          classes="sign-in__tel"
          onChange={(e) => onChange(e, 'phone')}
          onClear={() => onClear('phone')}
        />

        <Btn
          text={btnText}
          primary
          disabled={!formState.isValid}
          type="submit"
          centred
          fullWidth
          loading={loading}
        />
      </form>
    </>
  );
};
