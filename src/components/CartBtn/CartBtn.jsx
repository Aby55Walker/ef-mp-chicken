import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import { ReactComponent as CartIcon } from '../../assets/icons/cart.svg';
import { modalCart } from '../../constants/pagelinks';

export const CartBtn = (props) => {
  const {
    time = 29,
    status = 'Готовится заказ D-25',
    price = 880,
  } = props;
  const { pathname } = useLocation();

  return (
    <div className="cart-btn">
      <Link to={`${pathname}${modalCart}`} className="cart-btn__link" />
      <div className="cart-btn__left">

        {status
          ? <span className="cart-btn__status">{status}</span>
          : (
            <div className="cart-btn__order-price">
              <CartIcon className="cart-btn__icon" />
              <span className="cart-btn__price">{price} ₽</span>
            </div>
          )}
      </div>
      <div className="cart-btn-right">
        <span className="cart-btn__time">~{time} мин.</span>
      </div>
    </div>
  );
};
