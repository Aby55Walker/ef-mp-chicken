import React from 'react'
import { Link } from 'react-router-dom'
import s from './AppLink'

const AppLink = ({ to, text, fontSize = '14px', styles }) => {
  return (
    <Link styles={styles} className={s.link} to={to}>
      {text}
    </Link>
  )
}
