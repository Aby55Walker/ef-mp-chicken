// Checkbox
import React, { useState } from 'react'
import { ReactComponent as IconCheckbox } from '../../assets/icons/checkbox.svg'
import classNames from 'classnames'

export const Checkbox = ({
  text = '',
  classes = '',
  checked = false,
  disabled = false,
  onChange = null,
  register,
  name,
}) => {
  // hook для изменения состояния input (выбран / не выбран)
  const [checkedCheckbox, changeCheckbox] = useState(checked) // { boolean }

  // классы
  const labelClass = classNames(`checkbox${classes ? ' ' + classes : ''}`, {
    checkbox_disabled: disabled,
  })
  const iconClass = classNames(`checkbox__icon`, {
    checkbox__icon_show: checkedCheckbox,
    checkbox__icon_disabled: disabled,
  })

  // при изменении input
  const handleChange = () => {
    changeCheckbox(!checkedCheckbox)
    if (onChange) onChange(!checkedCheckbox)
  }

  // при нажатии на Enter когда label находится в фокусе
  const simulateClick = (e) => {
    if (e.key === 'Enter') handleChange()
  }

  return (
    <label
      className={labelClass}
      tabIndex='0'
      onKeyDown={(e) => simulateClick(e)}
    >
      {/* input, скрытый при помощи стилей */}
      <input
        type='checkbox'
        className='checkbox__input'
        name={name}
        ref={register}
        checked={checkedCheckbox}
        onChange={handleChange}
      />

      {/* иконка отображает выбран checkbox или нет */}
      <span className='checkbox__icon-wp'>
        <IconCheckbox className={iconClass} />
      </span>

      {/* текст */}
      <span
        className='checkbox__text'
        dangerouslySetInnerHTML={{ __html: text }}
      ></span>
    </label>
  )
}
