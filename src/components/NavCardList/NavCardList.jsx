import React, { useEffect, useState } from 'react';
import { useController, useLayoutEffect } from 'react-scroll-parallax';
import classNames from 'classnames';
import { NavCard } from './includes/NavCard';
import { MasonryGrid } from '../MasonryGrid/MasonryGrid';
import { list } from './list';

export const NavCardList = () => {
  const [isTransition, setIsTransition] = useState(false);

  const blockClasses = classNames('nav-card-list', {
    'nav-card-list--transition': isTransition,
  });

  const { parallaxController } = useController();

  const gridLoadHandler = () => {
    parallaxController.update();
  };

  useEffect(() => {
    if (!/iPhone|iPad|iPod/i.test(navigator.userAgent)) setIsTransition(true);
  }, []);

  return (
    <div className={blockClasses}>
      <MasonryGrid
        onLoad={gridLoadHandler}
        classes="nav-card-list__masonry-grid"
      >
        {list.map((item, i) => (
          <NavCard key={i} {...item} to="/" />
        ))}
      </MasonryGrid>
    </div>
  );
};
