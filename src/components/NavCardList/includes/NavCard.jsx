import React, { useEffect, useState } from 'react';
import { Parallax } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

import { randomInteger } from '../../../functions/randomInteger';
import { getRandomTransform } from '../../../functions/getRandomTransform';
import {
  minWidth,
  maxWidth,
  maxHoverTranslate,
  minHoverTranslate,
} from '../../../constants/cardsTransform';

export const NavCard = ({ text, to, image }) => {
  const [style, setStyle] = useState({});
  const [parallaxHeight, setParallaxHeight] = useState(0);

  useEffect(() => {
    setStyle({
      ...setStyle,
      zIndex: text.length ? 3 : 'auto',
      width: `${randomInteger(minWidth, maxWidth)}%`,
      transform: getRandomTransform(minHoverTranslate, maxHoverTranslate),
    });
    setParallaxHeight(randomInteger(20, 180));
  }, []);

  // const mouseOverHandler = () => {
  //   setStyle({
  //     ...style,
  //     transform: getRandomTransform(minHoverTranslate, maxHoverTranslate)
  //   })
  // }

  return (
    <Parallax
      y={[`${parallaxHeight}px`, `${-parallaxHeight}px`]}
      className="parallax-item"
      tagOuter="div"
    >
      <Link style={style} to={to} className="nav-card">
        <img src={image} className="nav-card__image" alt="main-card" />
        {text.map((item, i) => (
          <p key={i} className="nav-card__text">
            {item}
          </p>
        ))}
      </Link>
    </Parallax>
  );
};
