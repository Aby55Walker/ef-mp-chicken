import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

export const CityPicker = (props) => {
  const { classes } = props

  const cityPickerClasses = classNames(`city-picker ${classes || ''}`, {})

  return <span className={cityPickerClasses}>Ижевск</span>
}

CityPicker.propTypes = {
  classes: PropTypes.string,
}
