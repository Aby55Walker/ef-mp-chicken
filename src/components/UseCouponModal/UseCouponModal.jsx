import React, { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { SideModal } from '../SideModal/SideModal';
import { AddCouponContainer } from '../../containers/coupons/AddCouponContainer';
import { catalogPage, modalCart } from '../../constants/pagelinks';
import { Btn } from '../Btn/Btn';
import { lg } from '../../constants/breakpoints';

export const UseCouponModal = () => {
  const { pathname } = useLocation();
  const history = useHistory();
  const [redirectInCart, setRedirectInCart] = useState(false);
  const { screenWidth } = useSelector((state) => state.screenSize);
  const modalExitHandler = () => {
    // если нажата кнопка в корзину, то после закрытия модалки произоёт переход по ссылке
    if (redirectInCart) {
      history.push(
        screenWidth < lg
          ? `${pathname}${modalCart}`
          : catalogPage,
      );
    }
  };

  const closeModalHandler = (closeModal) => {
    closeModal();
    setRedirectInCart(true);
  };

  return (
    <SideModal onExited={modalExitHandler}>
      {(closeModal) => (
        <>
          <h2 className="use-coupon__title">Активировать свой купон</h2>
          <AddCouponContainer closeModal={() => closeModalHandler(closeModal)} id={0} />
        </>
      )}
    </SideModal>
  );
};
