export const radioButtons = [
  {
    value: 'flat',
    text: 'Квартира',
  },
  {
    value: 'house',
    text: 'Частн. дом',
  },
  {
    value: 'office',
    text: 'Офис',
  },
];

export const intercomList = [
  {
    value: 'false',
    text: 'позвонить в домофон',
    // prompt: '(~17:30)'
  },
  {
    value: 'true',
    text: 'домофон не работает',
  },
];

export const circleRadioBtnsWithoutTime = [
  {
    value: 'callIntercom',
    text: 'Позвонить в домофон',
  },
  {
    value: 'intercomIsBroken',
    text: 'Домофон не работает',
  },
];
