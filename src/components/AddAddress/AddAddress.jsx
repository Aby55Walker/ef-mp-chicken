import React from 'react';
import { Route } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { SideModal } from '../SideModal/SideModal';
import { Btn } from '../Btn/Btn';

// data
import { radioButtons, intercomList } from './data';
import { AddressForm } from '../AddressForm/AddressForm';

export const AddAddress = (props) => {
  const { onSubmit } = props;

  const { loading } = useSelector((state) => state.user);
  const path = '/*/side-modal/add-address';

  const submitHandler = (data) => {
    console.log('Add address data', data);
    if (onSubmit) onSubmit(data);
  };

  return (
    <Route
      path={path}
      render={() => (
        <SideModal contentClasses="add-address__modal-content">
          <h2 className="side-modal__title">Адрес доставки</h2>

          <div className="add-address">
            <AddressForm
              radioButtons={radioButtons}
              onSubmit={submitHandler}
              circleRadioBtns={intercomList}
            >
              <Btn
                type="submit"
                text="Добавить"
                primary
                centred
                loading={loading}
                fullWidth
                disabled
              />
            </AddressForm>
          </div>
        </SideModal>
      )}
    />
  );
};
