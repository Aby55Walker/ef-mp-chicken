// InternalLink
import React from 'react';
import classNames from 'classnames';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { ReactComponent as ErrorIcon } from '../../assets/icons/error.svg';
import { ReactComponent as DoneIcon } from '../../assets/icons/done.svg';
import { ReactComponent as ArrowIcon } from '../../assets/icons/chevron_up.svg';
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd';
import { productId } from '../../constants/urlParams';

export const InternalLink = (props) => {
  const {
    img,
    title,
    desc,
    required = true, // обязательно ли выбирать добавки
    success = false, // выбраны ли добавки
    classes,
    prodId,
    ...other
  } = props;

  const { pathname, search } = useLocation();
  const nextPath = `${deleteSlashAtTheEnd(`${pathname}`)}/choice`;

  const blockClasses = classNames(`internal-link ${classes || ''}`, {});
  const iconClasses = classNames('internal-link__icon', {});
  const iconWpClasses = classNames('internal-link__icon-wp', {
    'internal-link__icon-wp--error': required && !success,
    'internal-link__icon-wp--success': success,
  });

  return (
    <Link
      {...other}
      className={blockClasses}
      to={{
        pathname: nextPath,
        search: `${productId}=${prodId}`,
      }}
    >
      <div className="internal-link__img-wp">
        <img className="internal-link__img" src={img} alt={title} />
      </div>

      <div className="internal-link__content">
        <h4 className="internal-link__title">
          {title}&#160;
          <span className={iconWpClasses}>
            {success
              ? <DoneIcon className={iconClasses} />
              : <ErrorIcon className={iconClasses} />}
          </span>
        </h4>
        {desc && <p className="internal-link__desc">{desc}</p>}
      </div>

      <div className="internal-link__arrow-wp">
        <ArrowIcon className="internal-link__arrow" />
      </div>
    </Link>
  );
};
