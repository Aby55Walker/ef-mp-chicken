import React, { useRef, useEffect, useState } from 'react';
import cn from 'classnames';
import { useLocation } from 'react-router-dom';
import { Btn } from '../../Btn/Btn';
// import Image from '../../../assets/images/recomend-product-1.png';
import { ReactComponent as Plus } from '../../../assets/icons/plus.svg';

export const RecommendProduct = (props) => {
  const {
    classes = '',
    data = [],
    id = data?.id,
    name = data?.name,
    image = data?.pictures?.preview,
    price = data?.price,
    ...other
  } = props;

  const [lineCount, setLineCount] = useState(1);
  const refTitle = useRef();
  const { pathname } = useLocation();

  useEffect(() => {
    setTimeout(() => {
      calcRowCount();
    }, 200);
  }, []);

  const blockClasses = cn(`recommend-product ${classes || ''}`, {});
  const addClasses = cn('recommend-product__add', {});
  const titleClasses = cn('recommend-product__title', {
    'recommend-product__title_small': lineCount >= 3,
  });

  const calcRowCount = () => {
    if (!refTitle || !refTitle.current) return;

    const styles = window.getComputedStyle(refTitle.current);
    const lineHeight = styles.getPropertyValue('line-height');
    const lineHeightInt = parseInt(lineHeight);
    const height = refTitle.current.offsetHeight;

    const lines = height / lineHeightInt;
    setLineCount(lines);
  };

  return (
    <div {...other} className={blockClasses}>
      <div className="recommend-product__img-wp">
        <img className="recommend-product__img" src={image} alt={name} />
      </div>

      <div className="recommend-product__content">
        <h3 className={titleClasses}>
          <span ref={refTitle} className="recommend-product__title-text">
            {name}
          </span>
        </h3>

        <Btn
          outlined
          centred
          text={`${parseInt(price)}&#160;&#8381;`}
          classes={addClasses}
          tag="Link"
          to={{
            pathname: `${pathname}/side-modal/product`,
            search: `productId=${id}`,
            state: { modalIsOpen: true },
          }}
          leftIcon={<Plus />}
        />
      </div>
    </div>
  );
};
