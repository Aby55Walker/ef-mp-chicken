import React from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import { RecommendProduct } from './includes/RecommendProduct';
import { lg } from '../../constants/breakpoints';
import { Slider } from '../Slider/Slider';

export const RecommendProducts = (props) => {
  const { classes = '', data = [], ...other } = props;
  const { screenWidth } = useSelector((state) => state.screenSize);
  const blockClasses = cn('recommend-products', {});
  const titleClasses = cn(`recommend-products__title ${classes || ''}`, {});
  const listClasses = cn(`recommend-products__list ${classes || ''}`, {});
  const sliderClasses = cn(`recommend-products__slider ${classes || ''}`, {});

  return (
    <div {...other} className={blockClasses}>
      <h3 className={titleClasses}>Рекомендуем к&nbsp;заказу</h3>

      {screenWidth >= lg ? (
        <Slider
          {...other}
          sliderClasses={sliderClasses}
          settings={{
            slidesPerView: 'auto',
            spaceBetween: 8,
            grabCursor: true,
          }}
        >
          {data.map((param, index) => <RecommendProduct data={param} key={index} />)}
        </Slider>
      ) : (
        <ul className={listClasses}>
          {data.map((param, index) => (
            <li className="recommend-products__item" key={index}>
              <RecommendProduct data={param} />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
