import React, { useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import cn from 'classnames';
import { ReactComponent as Done } from '../../assets/icons/done.svg';
import { couponRules, useFormParams } from '../../constants/validate';
import { Btn } from '../Btn/Btn';
import { Input } from '../Input/Input';

export const AddCoupon = (props) => {
  const {
    done = false,
    onSubmit,
    loading = false,
    classes,
    btnProps = {
      outlined: true,
      style: { width: '126px' },
    },
    code = '',
    requestError,
    wrapperClasses,
    onInput,
    onReset,
  } = props;

  const {
    handleSubmit, register, formState, triggerValidation, errors,
  } = useForm(
    useFormParams,
  );

  const [inputVal, setInputVal] = useState(code);

  const refForm = useRef();

  const changeHandler = (val, name) => {
    setInputVal(val);
    triggerValidation(name);
    if (onInput) onInput(val);
  };

  const submitHandler = (data) => {
    if (onSubmit) onSubmit(data);
  };

  const clickHandler = (e) => {
    if (done) {
      setInputVal('');
    }
    if (onReset) onReset();
  };

  const inputClasses = cn('add-coupon__input', {
    'add-coupon__input_done': done,
  });

  const addCouponClasses = cn(`add-coupon ${classes || ''}`);
  const inputWrapperClasses = cn(`add-coupon__input-wrapper ${wrapperClasses || ''}`);

  return (
    <div className={addCouponClasses}>
      <form
        noValidate
        ref={refForm}
        onSubmit={handleSubmit(submitHandler)}
        className="add-coupon__form"
      >
        <div className={inputWrapperClasses}>
          <Input
            disabled={done}
            classes={inputClasses}
            ref={register(couponRules)}
            error={errors.coupon || requestError}
            errorText={(errors.coupon && errors.coupon.message) || requestError}
            name="coupon"
            label=""
            helper
            noClear
            helperText={done && 'Купон успешно активирован'}
            placeholder="Введите номер купона"
            LeftIcon={done && <Done className="add-coupon__done-icon" />}
            onChange={(val) => changeHandler(val, 'coupon')}
            value={inputVal}
          />
          <Btn
            onClick={clickHandler}
            type="submit"
            disabled={!inputVal || !formState.isValid}
            classes="add-coupon__btn"
            loading={loading}
            text={done ? 'Сбросить' : 'Применить'}
            centred
            {...btnProps}
          />
        </div>
      </form>
    </div>
  );
};
