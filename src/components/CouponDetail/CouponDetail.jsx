import React from 'react';
import cn from 'classnames';
import { useHistory } from 'react-router-dom';
import tempImage from '../../assets/images/catalog/product-2.png';
import { Spoiler } from '../Spoiler/Spoiler';
import { ReactComponent as ErrorIcon } from '../../assets/icons/error.svg';
import { AdditiveList } from '../AdditiveList/AdditiveList';
import { Btn } from '../Btn/Btn';

export const CouponDetail = (props) => {
  const { classes, data = [], ...other } = props;
  const history = useHistory();
  const blockClasses = cn(`product ${classes || ''}`, {});
  const spoilerClasses = cn(`product__spoiler ${classes || ''}`, {});
  const {
    weight,
    name,
    description,
    img = data?.pictures?.detail,
    modifiers = [],
  } = data;

  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div className={blockClasses} {...other}>
        <div className="product__img-wp">
          <img className="product__img" src={tempImage} alt={name} />
        </div>
        <h3 className="product__title">{name}</h3>
        <p className="product__desc">{description}</p>
        <span className="product__weight">{weight}</span>

        {modifiers.map(({ groupName, groupItems }, index) => (
          <Spoiler
            key={index}
            title={groupName}
            text="Выберите до 4 отдельных глазировок для 20 кусочков"
            Icon={<ErrorIcon className="product__error-icon" />}
            classes={spoilerClasses}
          >
            <AdditiveList data={groupItems} />
          </Spoiler>
        ))}
      </div>

      <div className="product-price product-price_dark">
        <Btn onClick={goBack} text="Применить" fullWidth centred primary disabled />
      </div>
    </>
  );
};
