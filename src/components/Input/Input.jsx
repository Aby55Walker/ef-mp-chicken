// Input

import React, { useState, useRef, useEffect } from 'react';
import classNames from 'classnames';
import { IMaskInput } from 'react-imask';
import { ReactComponent as IconClearText } from '../../assets/icons/clear_text.svg';

// export const Input = (props) => {
export const Input = React.forwardRef((props, ref) => {
  const {
    value = '', // значение input
    label = '', // название поля (отображается сверху)
    placeholder = '',
    name = null,
    mask = null,
    type = 'text',
    LeftIcon,
    onClear,
    noClear, // убирает кнопку очистки
    square = false, // input в виде квадрата
    required = false,
    error = false, // значение введено с ошибкой
    errorText = '', // сообщение об ошибке
    classes = '', // дополнительные классы для label (родительский элемент)
    disabled = false,
    onChange = null, // при изменении значения input
    onBlur = null, // при потере фокуса input
    icon = null, // иконка для input (отображается справа на месте кнопки очистки поля)
    helper,
    helperText = null, // вспомогательный текст, отображается под input
    ...other // прочие параметры, присваиваются input
  } = props;

  // state
  const [inputValue, setInputValue] = useState(value); // {string} значение input
  const [focus, changeFocus] = useState(false); // {boolean} находится ли элемент в фокусе
  const initialValue = mask && mask === 'tel' ? '+7 9' : '';

  const InputTag = mask ? IMaskInput : 'input';

  // классы
  const labelClass = classNames(`input${classes ? ` ${classes}` : ''}`, {
    input_disabled: disabled,
    input_square: square,
  });
  const inputClass = classNames('input__fluid', {
    'input__fluid--error': error,
    input__fluid_square: square,
    'input_no-label': !label,
    'input_no-clear': noClear,
  });
  const clearClass = classNames('input__clear input__icon-wp', {
    input__clear_show: inputValue && focus,
  });
  const titleClass = classNames('input__title', {
    'input__title--top': inputValue || focus || initialValue,
    'input__title--error': error,
  });

  // при изменении значения input
  const handleChange = (e) => {
    const { value } = e.target;

    let fixedValue = value;
    if (square) {
      fixedValue = value ? value.charAt(0) : value;
      fixedValue = fixedValue.replace(/\D/, '');
    }

    setInputValue(fixedValue);
    if (onChange) onChange(fixedValue);
  };

  // при потере фокуса input
  const handleBlur = (e) => {
    changeFocus(false);
    if (onBlur) onBlur(e);
  };

  // очистить поле
  const clear = () => {
    setInputValue('');

    if (mask && mask === 'tel') {
      setInputValue(initialValue);
    } else {
      setInputValue('');
    }

    if (onChange) {
      if (mask && mask === 'tel') {
        onChange(initialValue);
      } else {
        onChange('');
      }
    }
    if (onClear) onClear();
  };

  // при нажатии на Enter когда .input__clear находится в фокусе
  const simulateClickOnClear = (e) => {
    if (e.key === 'Enter') clear();
  };

  // обновляет value в форме после обновления данных из api
  useEffect(() => {
    setInputValue(value);
  }, [value]);

  return (
    <label
      className={labelClass}
      onFocus={() => changeFocus(true)}
      onBlur={(e) => handleBlur(e.target.value)}
    >

      <div className="input__inner-wp">
        {LeftIcon && LeftIcon}
        {/* input */}
        <InputTag
          autoComplete="off"
          mask={
            mask
              ? (mask === 'tel' && '+{7} 900 000-00-00')
              || (mask === 'date' && Date)
              : null
          }
          min={mask && mask === 'date' ? new Date(1920, 0, 1) : null}
          max={mask && mask === 'date' ? new Date() : null}
          className={inputClass}
          type={type}
          ref={ref}
          {...(mask && ref ? { inputRef: ref } : {})}
          name={name}
          required={required}
          disabled={disabled}
          value={inputValue || initialValue}
          placeholder={(placeholder && focus) || !label ? placeholder : null}
          onChange={handleChange}
          {...other}
        />

        {/* очистить input при клике на этот элемент */}
        {!icon && !disabled && !square && !noClear && (
          <span
            className={clearClass}
            // tabIndex={inputValue && '0'}
            onMouseDown={clear}
            onKeyDown={(e) => simulateClickOnClear(e)}
          >
            <IconClearText className="input__icon" />
          </span>
        )}

        {/* текст ошибки */}
        {error && errorText && (
          <span
            className="input__text input__error"
            dangerouslySetInnerHTML={{ __html: errorText }}
          />
        )}
        {/* вспомогательный текст под input */}
        {helperText && (
          <span
            className="input__text input__helper"
            dangerouslySetInnerHTML={{ __html: helperText }}
          />
        )}
      </div>

      {/* название input (label) */}
      {label && <span className={titleClass}>{label}</span>}

      {/* иконка */}
      {icon && <span className="input__icon-wp">{icon}</span>}

    </label>
  );
});
