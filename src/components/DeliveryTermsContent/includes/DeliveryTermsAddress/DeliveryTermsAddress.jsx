import React from 'react';

export const DeliveryTermsAddress = (props) => {
  const {
    name, address, onClick, ...other
  } = props;

  const clickHandler = () => {
    if (onClick) onClick();
  };

  return (
    <li
      {...other}
      onClick={clickHandler}
      className="terms-address"
      title={`Показать на карте ${name}`}
    >
      <h3 className="terms-address__name">{name}</h3>
      <p className="terms-address__address">{address}</p>
    </li>
  );
};
