import React from 'react';
import { DeliveryTermsAddress } from './DeliveryTermsAddress/DeliveryTermsAddress';

export const DeliveryTermsAddressList = (props) => {
  const { list = [], onClick, ...other } = props;

  const clickHandler = (addressIndex) => {
    if (onClick) onClick(addressIndex);
  };

  if (!list.length) return null;

  return (
    <ul {...other}>
      {list.map((item, i) => (
        <DeliveryTermsAddress
          key={item.id}
          onClick={() => clickHandler(i)}
          name={item.name}
          address={item.address}
        />
      ))}
    </ul>
  );
};
