import React from 'react';
import { DeliveryTermsAddressListContainer } from '../../containers/DeliveryTermsAddressListContainer';

export const DeliveryTermsContent = (props) => {
  const {
    points = [],
    data,
    restsData = [],
    ...other
  } = props;

  const {
    title,
    other: {
      min_sum_delivery: price,
      avg_delivery_time: time,
    } = {},
  } = data;

  return (
    <section {...other} className="terms delivery-terms-content">
      {title && <h1 className="terms__title">{title}</h1>}

      <div className="terms__desk">
        <div className="terms__desk-content">
          <strong className="terms__lead">{price}</strong>
          <p className="terms__text">Минимальная сумма заказа</p>
        </div>

        <div className="terms__desk-content">
          <strong className="terms__lead">{time}</strong>
          <p className="terms__text">Среднее время доставки</p>
        </div>
      </div>

      <DeliveryTermsAddressListContainer
        className="terms__list"
        points={points}
        list={restsData}
      />
    </section>
  );
};
