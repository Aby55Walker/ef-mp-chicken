import React, { useRef, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import MarkerYellow from '../../assets/icons/map-marker_yellow.svg';
import MarkerPink from '../../assets/icons/map-marker_pink.svg';
import IconPinYellow from '../../assets/pin_yellow.png';
import IconPinPink from '../../assets/pin_pink.png';
import styles from './styles';
import { loadGoogleMap } from '../../functions/loadGoogleMap';
import {
  mapDestroy,
  mapSetInstance,
  mapSetMarkers,
} from '../../store/modules/map/actions';
import { useCSSProperty } from '../../hooks/hooks';

// TODO убрать дефолтные значения, они будут приходить по api
const defaultPoligons = [
  { lat: 56.860272, lng: 53.197304 },
  { lat: 56.843557, lng: 53.20113 },
  { lat: 56.849073, lng: 53.25423 },
  { lat: 56.863919, lng: 53.278291 },
  { lat: 56.87072, lng: 53.250235 },
];

const defaultPoints = [
  { lat: 56.86088, lng: 53.222691, title: 'Адрес 1' },
  { lat: 56.85186, lng: 53.206596, title: 'Адрес 2' },
];

export const Map = (props) => {
  const {
    zoom = 13,
    center = { lat: 56.851872, lng: 53.206573 },
    polygons = [],
    points = [],
    classes,
    autocomplete = true,
    noPolygon,
    hideUI,
    ...other
  } = props;

  const dispatch = useDispatch();

  const [loaded, setLoaded] = useState(false);
  const [map, setMap] = useState(null);
  const { isOrderTypeDelivery } = useSelector((state) => state.orderType);
  // выюор цвета маркеров и полигона
  const accentColor = useCSSProperty('--accent');
  const mapMarkers = {
    circle: isOrderTypeDelivery ? MarkerYellow : MarkerPink,
    pin: isOrderTypeDelivery ? IconPinYellow : IconPinPink,
  };

  const mapRef = useRef();
  const inputRef = useRef();
  const infoWindowRef = useRef();

  // параметры для карты
  const mapParam = {
    zoom,
    center: {
      lat: center.lat,
      lng: center.lng,
    },
    styles,
    disableDefaultUI: hideUI,
  };

  useEffect(() => {
    loadGoogleMap().then(() => {
      setLoaded(true);
      setMap(new window.google.maps.Map(mapRef.current, mapParam));
    });
    return () => dispatch(mapDestroy());
  }, []);

  useEffect(() => {
    if (noPolygon) {
      if (loaded && map && points.length) createMap();
    } else if (loaded && polygons.length && map && points.length) createMap();
  }, [loaded, map, polygons, points]);

  const createMap = () => {
    // записывает карту в store
    dispatch(mapSetInstance(map));

    // параметры для полигонов

    const polygonsParam = {
      paths: polygons,
      strokeColor: accentColor,
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: accentColor,
      fillOpacity: 0.2,
    };

    // *** точки на карте
    const markers = points.map((data, i) => new window.google.maps.Marker({
      position: data,
      icon: mapMarkers.circle,
      map,
    }));
    // добавляет markers в store
    dispatch(mapSetMarkers(markers));

    // ***полигоны
    if (polygons) {
      const place = new window.google.maps.Polygon(polygonsParam);
      place.setMap(map);
    }

    // *** окно с информацией о точке на карте
    const infoWindow = new window.google.maps.InfoWindow();
    const infoWindowContent = infoWindowRef.current;
    infoWindow.setContent(infoWindowContent);
    if (!infoWindowRef.current) return;

    markers.forEach((mark, i) => {
      mark.addListener('click', () => {
        const title = mark?.title;
        if (title) {
          infoWindowContent.textContent = title;
          infoWindow.open(map, mark);
        }
      });
    });

    // *** автокомплит
    if (autocomplete) {
      const input = inputRef.current;
      const autocomplete = new window.google.maps.places.Autocomplete(input);
      autocomplete.bindTo('bounds', map);

      const marker = new window.google.maps.Marker({
        map,
        icon: mapMarkers.circle,
        anchorPoint: new window.google.maps.Point(0, -29),
      });

      // *** обработчик выбора в предложенных вариантах автокомплита
      autocomplete.addListener('place_changed', () => {
        infoWindow.close();
        marker.setVisible(false);
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        }

        if (place.geometry.viewport) {
          map.fitBounds(place.geometry.viewport);
        } else {
          map.setCenter(place.geometry.location);
          map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        let address = '';
        if (place.address_components) {
          address = [
            (place.address_components[0]
              && place.address_components[0].short_name)
              || '',
            (place.address_components[1]
              && place.address_components[1].short_name)
              || '',
            (place.address_components[2]
              && place.address_components[2].short_name)
              || '',
          ].join(' ');
        }

        if (address) {
          infoWindowContent.textContent = address;
          infoWindow.open(map, marker);
        }
      });
    }
  };

  return (
    <div className={`map-wp ${classes || ''}`} {...other}>
      {loaded && autocomplete && (
        <>
          <input
            ref={inputRef}
            autoComplete="off"
            className="input__fluid delivery-terms__search"
            type="text"
            name="address"
            placeholder="Улица, дом"
          />
          <span ref={infoWindowRef} />
        </>
      )}

      <div ref={mapRef} className="map" />
    </div>
  );
};

Map.propTypes = {
  autocomplete: PropTypes.bool,
  center: PropTypes.objectOf({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  classes: PropTypes.string,
  hideUI: PropTypes.bool,
  points: PropTypes.arrayOf(PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
    title: PropTypes.string,
  })).isRequired,
  polygons: PropTypes.any,
  zoom: PropTypes.number,
};
