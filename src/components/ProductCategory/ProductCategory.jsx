import React from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { MasonryGrid } from '../MasonryGrid/MasonryGrid';
import { ProductCard } from '../ProductCard/ProductCard';

export const ProductCategory = React.forwardRef((props, ref) => {
  const {
    data: {
      products = [],
      slug,
      ended_at: endedAt,
      started_at: startedAt,
      name = '',
    } = {},
    ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const { pathname } = useLocation();

  return (
    <div {...other} ref={ref} className="product-category">
      <h2 className="product-category__title">{name}</h2>
      <MasonryGrid classes="product-category__grid">
        {products.map((product, i) => (
          <Link
            key={product.id}
            to={{
              pathname: `${pathname}/side-modal/product`,
              search: `productId=${product.id}`,
              state: { modalIsOpen: true },
            }}
          >
            <ProductCard
              slug={slug}
              endedAt={endedAt}
              startedAt={startedAt}
              key={product.id}
              gridItemClasses={
                i === 0 && screenWidth < 568
                  ? 'masonry-grid__item_full-width'
                  : ''
              }
              data={product}
            />
          </Link>

        ))}
      </MasonryGrid>
    </div>
  );
});
