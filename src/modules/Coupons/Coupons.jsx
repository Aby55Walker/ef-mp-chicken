import React, { useEffect } from 'react';
import cn from 'classnames';
import { useRouteMatch, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { CouponsSlider } from '../../components/Sliders/CouponsSlider/CouponsSlider';
import { Btn } from '../../components/Btn/Btn';
import { couponsPage, useCouponModal } from '../../constants/pagelinks';
import { md } from '../../constants/breakpoints';
import { CouponCard } from '../../components/CouponCard/CouponCard';
import { NotAuth } from '../../components/NotAuth/NotAuth';
import { couponsRequest } from '../../store/modules/coupons/actions';

export const Coupons = (props) => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const { url } = useRouteMatch();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const { loggedIn } = useSelector((state) => state.auth);
  const { type } = useSelector((state) => state.orderType);
  const coupons = useSelector((state) => state.coupons.data);

  useEffect(() => {
    // получить список купонов
    if (loggedIn) dispatch(couponsRequest(type));
  }, [type]);

  const notAuthCard = (
    <CouponCard
      classes="coupons-slider__card coupons__not-auth-card"
      Button={(
        <Btn
          text="Зарегистрироваться"
          transparent
          fullWidth
          centred
          classes="coupon-card__btn"
          tag="Link"
          to={`${pathname}/side-modal/registration`}
        />
      )}
    >
      <NotAuth
        title="Зарегистрируйтесь, чтобы начать получать купоны"
        withBtn={false}
      />
    </CouponCard>
  );

  const couponsClasses = cn('coupons', {
    'coupons_not-auth': !loggedIn,
  });

  return (
    <div className={couponsClasses}>
      <h2 className="coupons__title">Купоны</h2>

      {loggedIn ? <CouponsSlider data={coupons} /> : notAuthCard}

      {loggedIn && (
        <div className="coupons__buttons">
          <Btn
            outlined
            text="Все купоны"
            tag="Link"
            to={couponsPage}
            fullWidth={screenWidth < md}
            centred
          />
          {screenWidth < md && <hr className="coupons__divider" />}
          <Btn
            outlined
            text="Применить свой купон"
            tag="Link"
            to={`${url}${useCouponModal}`}
            fullWidth={screenWidth < md}
            centred
          />
        </div>
      )}
    </div>
  );
};
