// ShortRegistration
import React from 'react';
import {
  Route, Switch, useRouteMatch,
} from 'react-router-dom';
import { SideModal } from '../../components/SideModal/SideModal';
import { Help } from '../../components/Help/Help';
import { SignInContainer } from '../../containers/auth/SignInContainer';
import { AuthCodeIntroductionContainer } from '../../containers/auth/AuthCodeIntroductionContainer';
import { HelpProblemsContainer } from '../../containers/help/HelpProblemsContainer';
import { shortAuthPage } from '../../constants/pagelinks';

export const ShortRegistration = () => {
  const match = useRouteMatch(`*${shortAuthPage}`);

  const HelpBlock = () => (
    <Help
      to={`${match.url}/help`}
      classes="registration__help"
      text="Что-то не получается?"
      small
    />
  );

  return (
    <SideModal>
      <Switch>
        <Route exact path={`${match.url}/`}>
          <SignInContainer title="Подтвердите телефон" path={shortAuthPage} />
          <HelpBlock />
        </Route>

        <Route exact path={`${match.url}/code-introduction`}>
          <AuthCodeIntroductionContainer
            path={shortAuthPage}
            registeredRelocation="/delivery/1"
            goToCheckout
          />
          <HelpBlock />
        </Route>

        <Route exact path={`${match.url}/help`}>
          <HelpProblemsContainer />
        </Route>
      </Switch>
    </SideModal>
  );
};
