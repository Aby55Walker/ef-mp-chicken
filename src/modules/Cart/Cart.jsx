import React, { useRef } from 'react';
import cn from 'classnames';
import { useStickyElem } from '../../functions/makeElementSticky';
import { RecommendProducts } from '../../components/RecommendProducts/RecommendProducts';
import { CartCouponsContainer } from '../../containers/cart/CartCouponsContainer';
import { CartItemContainer } from '../../containers/cart/CartItemContainer';
import { OrderBtn } from '../../components/OrderBtn/OrderBtn';

export const Cart = (props) => {
  const {
    classes = '',
    data = {},
    isSticky = true,
    mainClass = 'cart', // класс компонента
    ...other
  } = props;

  const cartRef = useRef();

  const stickyClass = useStickyElem(cartRef.current, 0);
  // TODO добавить состояние пустой корзины
  const cartClasses = cn(`${mainClass} ${classes || ''}`, {
    sticky: stickyClass && isSticky,
  });

  return (
    <div {...other} ref={cartRef} className={cartClasses}>

      {data?.items?.length ? (
        <>
          <h2 className="cart__title">Корзина</h2>
          <ul className="cart__products cart__child">
            {data.items.map((param, index) => (
              <li key={index}>
                <CartItemContainer data={param} />
              </li>
            ))}
          </ul>
        </>
      ) : (
        <span className="cart__empty-text">
          Выберите блюда <br /> и добавьте их к заказу
        </span>
      )}

      {data?.upsale?.length
        ? (
          <RecommendProducts
            data={data.upsale}
            classes="cart__child"
          />
      )
        : null}

      {data?.items?.length
        ? <CartCouponsContainer />
        : null}

      {data?.items?.length
        ? <OrderBtn classes="cart__order-btn" />
        : null }
    </div>
  );
};
