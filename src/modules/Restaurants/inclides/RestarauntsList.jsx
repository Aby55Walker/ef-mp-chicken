import React from 'react';
import { Restaurant } from './Restaurant/Restaurant';

export const RestaurantsList = ({ list = [], userCoords, loading }) => {
  if (!list.length) {
    return (
      <h3 className="restaurants__nothing-found">
        {loading ? 'Загрузка ресторанов...' : 'Ничего не найдено'}
      </h3>
    );
  }

  return (
    <div className="grid restaurants__grid">
      {list.map((restData) => (
        <div key={restData.id} className="grid__item">
          <Restaurant userCoords={userCoords} restData={restData} />
        </div>
      ))}
    </div>
  );
};
