import React from 'react'
import { Input } from '../../../components/Input/Input'

export const RestaurantsSearch = (props) => {
  const { onChange, ...rest } = props

  const changeHandler = (value) => {
    if (onChange) onChange(value)
  }

  return (
    <Input
      {...rest}
      onChange={changeHandler}
      name='restSearch'
      label='Район, улица'
      type='search'
    />
  )
}
