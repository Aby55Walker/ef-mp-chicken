import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { ReactComponent as MapIcon } from '../../../../assets/icons/map.svg';
import { showNotification } from '../../../../store/actions';
import { weekDays } from '../../../../constants/weekDays';

export const Restaurant = (props) => {
  const {
    restData: {
      name,
      address,
      work_status: status,
      work_mode: workMode,
      restaurant_coords: restCoords,
      next_work_day: nextWorkDay,
      day,
    },
    userCoords = [],
    ...other
  } = props;

  const [mapPath, setMapPath] = useState('');
  const dispatch = useDispatch();

  // собирает ссылку для построения маршрута
  useEffect(() => {
    if (userCoords.length && restCoords && restCoords.length) {
      const link = `https://yandex.ru/maps/?rtext=${userCoords.join(
        ',',
      )}~${restCoords.join(',')}&rtt=comparison`;
      setMapPath(link);
    }
  }, [restCoords]);

  // строки времени работы
  const workStart = workMode[day]?.start;
  const workEnd = workMode[day]?.end;
  const closedUntil = workMode[nextWorkDay] ? `до ${workMode[nextWorkDay]?.start} ${weekDays[nextWorkDay]} ` : 'до неопределённого времени';
  const workTime = `${workStart} - ${workEnd}`;

  const openStatusClasses = classNames('restaurant__open-status', {
    'restaurant__open-status_closed': !status,
  });

  const clickHandler = (e) => {
    if (!restCoords) {
      e.preventDefault();
      dispatch(
        showNotification({
          text: 'Разрешите определение геопозиции для построения маршрута',
          isError: true,
        }),
      );
    }
  };

  return (
    <div {...other} className="restaurant">
      <div className="restaurant__header">
        <a
          href={mapPath}
          onClick={clickHandler}
          title="Построить маршрут"
          target="_blank"
          rel="noreferrer"
        >
          <h3 className="restaurant__title">{name}</h3>
        </a>
        <a
          href={mapPath}
          onClick={clickHandler}
          title="Построить маршрут"
          target="_blank"
          rel="noreferrer"
          className="restaurant__map-icon-wrapper"
        >
          <MapIcon className="restaurant__map-icon" />
        </a>
      </div>

      <div className={openStatusClasses}>
        {status ? 'Открыто' : 'Закрыто'} {status ? workEnd : closedUntil}
      </div>
      {/* адрес */}
      <div className="restaurant__info">{address}</div>
      {/* время работы */}
      {workStart && workEnd && (
        <div className="restaurant__info">
          Доставка и самовывоз{' '}
          <span className="restaurant__work-time">{workTime}</span>
        </div>
      )}
    </div>
  );
};
