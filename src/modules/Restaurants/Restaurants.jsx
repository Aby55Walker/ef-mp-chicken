import React, { useEffect, useState } from 'react';
import { RestaurantsSearch } from './inclides/RestarauntsSearch';
import { RestaurantsList } from './inclides/RestarauntsList';
import { useGeolocation } from '../../hooks/hooks';

export const Restaurants = (props) => {
  const { data, loading, ...other } = props;

  const userCoords = useGeolocation();
  const [list, setList] = useState([]);

  useEffect(() => {
    setList(data);
  }, [data]);

  const changeHandler = (value) => {
    // фильтрация ресторанов
    const lowerCaseValue = value.toLowerCase().trim();
    setList(
      data.filter(
        (item) => item.name.toLowerCase().includes(lowerCaseValue)
          || item.address.toLowerCase().includes(lowerCaseValue),
      ),
    );
  };

  if (!data.length) return null;

  return (
    <div {...other} className="restaurants">
      {/* заголовок страницы */}
      <h1 className="restaurants__title">Рестораны</h1>
      {/* поиск */}
      <RestaurantsSearch
        onChange={changeHandler}
        classes="restaurants__search"
      />
      {/* список ресторанов */}
      {list.length && (
        <RestaurantsList
          loading={loading}
          list={list}
          userCoords={userCoords}
        />
      )}
    </div>
  );
};
