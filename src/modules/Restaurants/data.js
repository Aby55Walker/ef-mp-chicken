export default [
  {
    building: 'ТЦ «ЦУМ»',
    open: true,
    openTime: '10:00',
    closeTime: '00:00',
    address: 'ул. К. Маркса, 244',
    coords: ['56.866485', '53.220314'],
  },
  {
    building: 'ТЦ «Талисман»',
    open: false,
    openTime: '09:00',
    closeTime: '23:00',
    address: 'ул. К. Маркса, 32',
    coords: ['56.848321', '53.278485'],
  },
  {
    building: 'ТЦ «ЦУМ»',
    open: true,
    openTime: '12:00',
    closeTime: '23:30',
    address: 'ул. К. Маркса, 23',
    coords: ['56.847024', '53.213610'],
  },
  {
    building: 'ТЦ «ЦУМ»',
    open: false,
    openTime: '08:00',
    closeTime: '22:00',
    address: 'ул. К. Маркса, 244',
    coords: ['56.875827', '53.207518'],
  },
]
