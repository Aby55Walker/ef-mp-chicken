import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { SideModal } from '../../components/SideModal/SideModal';
import { Help } from '../../components/Help/Help';
import { RegistrationFormContainer } from '../../containers/auth/RegistrationFormContainer';
import { SignInContainer } from '../../containers/auth/SignInContainer';
import { AuthCodeIntroductionContainer } from '../../containers/auth/AuthCodeIntroductionContainer';
import { HelpProblemsContainer } from '../../containers/help/HelpProblemsContainer';
import { authPage } from '../../constants/pagelinks';

export const Registration = () => {
  const match = useRouteMatch(`*${authPage}`);

  const HelpBlock = () => (
    <Help
      to={`${match.url}/help`}
      classes="registration__help"
      text="Что-то не получается?"
      small
    />
  );

  return (
    <Route
      path={`*${authPage}`}
      render={() => (
        <SideModal>
          <Switch>
            <Route exact path={`${match.url}/`}>
              <SignInContainer />
              <HelpBlock />
            </Route>

            <Route exact path={`${match.url}/code-introduction`}>
              <AuthCodeIntroductionContainer />
              <HelpBlock />
            </Route>

            <Route exact path={`${match.url}/profile`}>
              <RegistrationFormContainer />
              <HelpBlock />
            </Route>

            <Route exact path={`${match.url}/help`}>
              <HelpProblemsContainer />
            </Route>
          </Switch>
        </SideModal>
      )}
    />
  );
};
