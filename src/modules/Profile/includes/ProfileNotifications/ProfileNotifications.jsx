// ProfileNotifications

import React from 'react'

import { Spoiler } from '../../../../components/Spoiler/Spoiler'
import { Checkbox } from '../../../../components/Checkbox/Checkbox'

export const ProfileNotifications = (props) => {
  const { is_accept_notifications = false } = props.data

  const { open = true } = props

  return (
    <Spoiler title='Уведомления' open={open} classes='profile-notifications'>
      <Checkbox
        text={
          'Даю согласие на&nbsp;получение СМС и&nbsp;e&#8209;mail уведомлений'
        }
        checked={is_accept_notifications}
        disabled={is_accept_notifications}
        classes='profile-notifications__checkbox'
      />
    </Spoiler>
  )
}
