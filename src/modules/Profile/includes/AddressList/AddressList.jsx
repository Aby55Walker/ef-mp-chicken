import React from 'react'
import { Spoiler } from '../../../../components/Spoiler/Spoiler'
import { Address } from '../../../../components/Address/Address'
import { ReactComponent as Add } from '../../../../assets/icons/plus.svg'
import { Link, useLocation } from 'react-router-dom'
import { ReactComponent as Office } from '../../../../assets/icons/office.svg'

export const AddressList = (props) => {
  const { addressList = [], ...other } = props

  const { pathname } = useLocation()

  const addressDescriptions = addressList.map((adr) => {
    let addressString = ''

    if (adr.type === 'flat') {
      addressString = `Кв. ${adr.flat}, ${adr.porch} подъезд, ${adr.floor} этаж`
    } else if (adr.type === 'office') {
      addressString = `Оф. ${adr.room}`
    }
    return [
      addressString,
      `${adr.is_intercom_broken ? 'Без домофона' : 'С домофоном'}`,
      adr.comment,
    ]
  })

  return (
    <Spoiler title='Адрес доставки' open={true}>
      <div {...other} className='address-list'>
        {addressList.map((address, i) => (
          <Address
            address={address.address}
            text={addressDescriptions[i]}
            index={i}
            key={i}
            buildingIcon={<Office />}
          />
        ))}

        <Link
          to={`${pathname}/side-modal/add-address`}
          className='address-list__add'
        >
          <Add className='address-list__add-icon' />
          <span className='address-list__add-text'>Добавить адрес</span>
        </Link>
      </div>
    </Spoiler>
  )
}
