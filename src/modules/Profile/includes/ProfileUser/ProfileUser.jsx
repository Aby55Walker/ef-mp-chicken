import React from 'react'
import { useRouteMatch, Link } from 'react-router-dom'

import { ReactComponent as Edit } from '../../../../assets/icons/edit.svg'
import { Spoiler } from '../../../../components/Spoiler/Spoiler'
import { Input } from '../../../../components/Input/Input'
import { Btn } from '../../../../components/Btn/Btn'

import { ReactComponent as IconCalendar } from '../../../../assets/icons/calendar.svg'

import { useForm } from 'react-hook-form'

import {
  useFormParams,
  emailRules,
  nameRules,
  telRules,
  dateRules,
} from '../../../../constants/validate'
import { makeMaskedPhone } from '../../../../functions/makeMaskedPhone'

export const ProfileUser = (props) => {
  const {
    first_name = '',
    last_name = '',
    birthday = '',
    email = '',
    phone = '',
  } = props.data

  const { open = true, classes = '', onSubmit } = props

  const { url } = useRouteMatch()
  const {
    handleSubmit,
    register,
    errors,
    triggerValidation,
    formState,
  } = useForm(useFormParams) // валидация формы

  const submitHandler = (data, e) => {
    if (onSubmit) onSubmit(data)
  }

  const changeHandler = (name) => {
    triggerValidation(name)
  }

  return (
    <Spoiler
      title='Личные данные'
      open={open}
      classes={`profile-user ${classes}`}
    >
      <form
        onSubmit={handleSubmit(submitHandler)}
        className='profile-user__form'
      >
        <div className='profile-user__input'>
          <Input
            ref={register(nameRules)}
            error={errors.firstName}
            errorText={errors.firstName && errors.firstName.message}
            required
            name='firstName'
            value={first_name}
            label='Имя'
            onChange={() => changeHandler('firstName')}
            classes='profile-user__first_name'
          />
        </div>

        <div className='profile-user__input'>
          <Input
            required
            value={last_name}
            ref={register(nameRules)}
            error={errors.lastName}
            errorText={errors.lastName && errors.lastName.message}
            name='lastName'
            label='Фамилия'
            onChange={() => changeHandler('lastName')}
            classes='profile-user__last_name'
          />
        </div>

        <div className='profile-user__input'>
          <Link
            className='profile-user__change-phone'
            title='Изменить телефон'
            to={`${url}/side-modal/change-phone`}
          >
            <Edit />
          </Link>
          <Input
            required
            mask='tel'
            ref={register(telRules)}
            error={errors.phone}
            errorText={errors.phone && errors.phone.message}
            type='tel'
            disabled
            value={makeMaskedPhone(phone)}
            label='Телефон'
            onChange={() => changeHandler('phone')}
            name='phone'
            classes='profile-user__phone'
          />
        </div>

        <div className='profile-user__input'>
          <Input
            required
            ref={register(emailRules)}
            type='email'
            name='email'
            value={email}
            error={errors.email}
            errorText={errors.email && errors.email.message}
            label='E-mail'
            onChange={() => changeHandler('email')}
            classes='profile-user__email'
          />
        </div>

        <div className='profile-user__input'>
          <Input
            required
            value={birthday}
            label='Дата рождения'
            ref={register(dateRules)}
            error={errors.birthday}
            errorText={errors.birthday && errors.birthday.message}
            mask='date'
            name='birthday'
            onChange={() => changeHandler('birthday')}
            disabled
            icon={<IconCalendar />}
            classes='profile-user__birthday'
          />
        </div>

        <Btn
          disabled={!formState.isValid}
          classes='profile-user__save'
          text='Сохранить'
          primary
          fullWidth
          centred
        />
      </form>
    </Spoiler>
  )
}
