// Profile

import React from 'react';

import { useSelector } from 'react-redux';
import { ProfileUser } from './includes/ProfileUser/ProfileUser';
import { ProfileNotifications } from './includes/ProfileNotifications/ProfileNotifications';
import { AddressList } from './includes/AddressList/AddressList';

import UserFakeData from './UserFakeData.json';

export const Profile = (props) => {
  const {
    classes = '', userData = {}, onSubmit, ...other
  } = props;

  const { addresses } = useSelector((state) => state.user.data);

  const submitHandler = (data) => {
    if (onSubmit) onSubmit(data);
  };

  return (
    <div className={`profile${classes ? ` ${classes}` : ''}`} {...other}>
      <ProfileUser onSubmit={submitHandler} data={userData} />
      <ProfileNotifications data={UserFakeData.data} />
      <AddressList addressList={addresses} />
    </div>
  );
};
