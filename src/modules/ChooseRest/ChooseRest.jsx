import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { CommonModal } from "../../components/CommonModal/CommonModal";
import { Map } from "../../components/Map/Map";
import { WithRadio } from "../../components/HOCS/WithRadio";
import { CircleRadioBtn } from "../../components/CircleRadioBtn/CircleRadioBtn";
import { mapSetActiveMarker } from "../../store/modules/map/actions";
import { setRestaurantIndexId } from "../../store/modules/Restaurants/actions";
import { Btn } from "../../components/Btn/Btn";
import { md } from "../../constants/breakpoints";
import { weekDays } from "../../constants/weekDays";

export const ChooseRest = (props) => {
  const { data = [] } = props;

  const dispatch = useDispatch();
  const history = useHistory();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const [mapPoints, setMapPoints] = useState([]);
  const [radioList, setRadioList] = useState([]);
  const [selectedRestaurant, selectRestaurant] = useState();

  // трансформирует данные для маркеров
  useEffect(() => {
    setMapPoints(
      data.map((rest) => ({
        lat: +rest.restaurant_coords[0],
        lng: +rest.restaurant_coords[1],
        title: rest.address,
      }))
    );
  }, [data]);
  // трансформирует данные для списка ресторанов

  useEffect(() => {
    setRadioList(
      data.map((rest) => {
        const { work_mode: workMode, day, next_work_day: nextWorkDay } = rest;

        const workStart = workMode[day]?.start;
        const workEnd = workMode[day]?.end;
        const closedUntil = workMode[nextWorkDay]
          ? `Закрыто до ${workMode[nextWorkDay]?.start} ${weekDays[nextWorkDay]} `
          : "Закрыто до неопределённого времени";
        const workTime = `${workStart} - ${workEnd}`;
        return {
          value: rest.id.toString(),
          text: rest.address,
          time: workEnd && workStart ? workTime : closedUntil,
        };
      })
    );
  }, [data]);

  const changeHandler = (value, index) => {
    selectRestaurant({ value, index });
    dispatch(mapSetActiveMarker(index));
  };

  // записать id ресторана и закрыть модалку
  const setRestaurant = () => {
    if (typeof selectedRestaurant !== "undefined") {
      const { value, index } = selectedRestaurant;
      dispatch(setRestaurantIndexId({ index, id: +value }));
      history.goBack();
    }
  };

  return (
    <CommonModal contentClasses="choose-rest__modal-content">
      <div className="choose-rest__content">
        {screenWidth >= md && <h3 className="choose-rest__title">Выберите ресторан</h3>}

        <WithRadio
          list={radioList}
          Component={CircleRadioBtn}
          classes="choose-rest__radio-btn"
          wrapperClasses="choose-rest__radio-wrapper"
          onChange={changeHandler}
        />

        <Btn text="Приготовить заказ тут" primary centred style={{ marginTop: "auto" }} onClick={setRestaurant} />
      </div>

      {mapPoints.length && <Map classes="choose-rest__map-wp" hideUI noPolygon autocomplete={false} polygons={[]} points={mapPoints} />}
    </CommonModal>
  );
};
