import React, { useEffect } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { OrderStatus } from '../../components/OrderStatus/OrderStatus';
import { NothingYet } from '../../components/NothingYet/NothingYet';
import { Btn } from '../../components/Btn/Btn';
import { Help } from '../../components/Help/Help';
import { YourOrder } from './includes/YourOrder';
import { AddressCurrentOrder } from './includes/AddressCurrentOrder';
import { md } from '../../constants/breakpoints';
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd';
import { currentOrderRequest } from '../../store/modules/currentOrder/actions';
import { catalogPage } from '../../constants/pagelinks';

export const CurrentOrder = (props) => {
  const { classes = '', ...other } = props;
  const location = useLocation();
  const dispatch = useDispatch();

  const { screenWidth } = useSelector((state) => state.screenSize);
  const { isOrderTypeDelivery, isOrderTypePickup } = useSelector((state) => state.orderType);
  const { data } = useSelector((state) => state.currentOrder);

  const blockClass = classNames(`current-order ${classes || ''}`, {});

  useEffect(() => {
    dispatch(currentOrderRequest());
  }, [dispatch]);

  return (
    <>
      {data.length ? (
        <div className={blockClass}>
          {data.map((params) => (
            <React.Fragment key={params.id}>
              {/* TODO правильно обрабатывать тип доставки после доработок бэкендера */}
              <OrderStatus isOrderTypeDelivery={params.type} data={params} classes="current-order__info-block" {...other} />
              <YourOrder data={params} openSpoiler={data.length === 1} />
              {params.type && (
              <AddressCurrentOrder data={params} openSpoiler={data.length === 1} />
              )}
            </React.Fragment>
          ))}

          <Btn
            text="Перейти в меню"
            primary={isOrderTypeDelivery}
            outlined={isOrderTypePickup}
            centred
            classes="current-order__go-to-menu"
            tag="Link"
            to={catalogPage}
          />

          {screenWidth <= md && (
            <Help
              type={2}
              to={`${deleteSlashAtTheEnd(location.pathname)}/side-modal/help/order-questions`}
              text="Хотите отменить заказ?"
              classes="current-order__help"
            />
          )}
        </div>
      ) : (
        <NothingYet classes={blockClass} {...other} />
      )}
    </>
  );
};
