import React from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { Spoiler } from '../../../components/Spoiler/Spoiler';
import { md } from '../../../constants/breakpoints';
import { Address } from '../../../components/Address/Address';
import { ReactComponent as IconOffice } from '../../../assets/icons/office.svg';

export const AddressCurrentOrder = (props) => {
  const {
    classes = '', data = {}, openSpoiler = false, ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const blockClass = classNames(
    `address-current-order${classes ? ` ${classes}` : ''}`,
    {},
  );

  return (
    <Spoiler
      title="Адрес доставки"
      open={openSpoiler && screenWidth >= md}
      classes={blockClass}
      {...other}
    >
      <Address
        text={['Оф. 601', 'С домофоном', 'Первая дверь направо от лифта']}
        address="10 лет Октября, 57"
        edit={false}
        buildingIcon={<IconOffice />}
      />
    </Spoiler>
  );
};
