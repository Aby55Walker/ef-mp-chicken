import React from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { Spoiler } from '../../../components/Spoiler/Spoiler';
import { md } from '../../../constants/breakpoints';
import getFormattedPrice from '../../../functions/getFormattedPrice';

export const YourOrder = (props) => {
  const {
    classes = '',
    data = {},
    amount = data?.amount && getFormattedPrice(data.amount),
    items = data?.items,
    openSpoiler = false,
    ...other
  } = props;

  const { screenWidth } = useSelector((state) => state.screenSize);
  const blockClass = classNames(`your-order${classes ? ` ${classes}` : ''}`, {});

  return (
    <Spoiler
      title="Ваш заказ"
      subtitle={!(screenWidth >= md) && `${amount}&#160;&#8381;`}
      open={openSpoiler && screenWidth >= md}
      classes={blockClass}
    >
      <ul className="your-order__list">
        {items.map(({ name = '', quantity = '', price = '' }, index) => (
          <li key={index} className="your-order__item">
            <div className="your-order__wrap">
              <h3 className="your-order__name">{name}</h3>
              <strong className="your-order__sum">{getFormattedPrice(price)}&#160;&#8381;</strong>
            </div>
            <span className="your-order__desc your-order__quantity">
              {quantity} шт.
            </span>
          </li>
        ))}
      </ul>

      <p className="your-order__wrap your-order__full-price">
        <span>Сумма к&nbsp;оплате</span>
        <span>{amount}&#160;&#8381;</span>
      </p>

      <p className="your-order__wrap your-order__desc your-order__discount">
        <span>Скидка по&nbsp;купонам</span>
        <span>-400&#160;&#8381;</span>
      </p>
    </Spoiler>
  );
};
