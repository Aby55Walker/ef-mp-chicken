import React, { useEffect } from 'react';

import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { NothingYet } from '../../components/NothingYet/NothingYet';
import { OrderList } from '../../components/OrderList/OrderList';
// import FakeData from '../../components/OrderList/FakeData.json';
import { orderHistoryRequest } from '../../store/modules/orderHistory/actions';

export const OrderHistory = (props) => {
  const { classes = '', ...other } = props;

  const blockClass = classNames(`order-history${classes ? ` ${classes}` : ''}`, {});

  const dispatch = useDispatch();

  const { data } = useSelector((state) => state.orderHistory);

  useEffect(() => {
    dispatch(orderHistoryRequest());
  }, [dispatch]);

  return (
    <>
      {
        data.length ? (
          <OrderList array={data} classes={blockClass} {...other} />
        ) : (
          <NothingYet
            classes={blockClass}
            lastOrder={false}
            text="И&nbsp;это отлиный повод заказать что&#8209;нибудь вкусненькое!"
            {...other}
          />
        )
      }
    </>
  );
};
