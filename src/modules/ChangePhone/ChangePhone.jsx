import React from 'react';
import {
  Route, Switch, useHistory, useRouteMatch,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { SideModal } from '../../components/SideModal/SideModal';
import { SignIn } from '../../components/SignIn/SignIn';

import { Help } from '../../components/Help/Help';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';
import {
  userSetNewPhone,
  userUpdateData,
} from '../../store/modules/user/actionCreators';
import { ChangePhoneCodeIntroductionContainer } from '../../containers/profile/ChangePhoneCodeIntroductionContainer';
import { profilePage } from '../../constants/pagelinks';
import { fetchCodeRequest } from '../../store/modules/auth/actions';

export const ChangePhone = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch('*/side-modal/change-phone');
  const { phone, loading } = useSelector((state) => state.auth);
  const { data } = useSelector((state) => state.user);

  const sendNewPhoneSubmitHandler = (enteredPhone) => {
    dispatch(userSetNewPhone(enteredPhone));
    dispatch(
      fetchCodeRequest(
        {
          phone: makeUnmaskedPhone(phone),
          'new-phone': makeUnmaskedPhone(enteredPhone),
        },
        history,
        `${match.url}/new-phone-confirm`,
      ),
    );
  };

  const confirmNewPhoneSubmitHandler = (code) => {
    dispatch(
      userUpdateData(
        {
          ...data,
          code,
          phone: makeUnmaskedPhone(data.newPhone),
        },
        history,
      ),
    );
  };

  return (
    <Route
      path="*/side-modal/change-phone"
      render={() => (
        <SideModal>
          <Switch>
            <Route exact path={`${match.url}/`}>
              <SignIn
                btnText="Изменить телефон"
                title="Новый телефон"
                loading={loading}
                onSubmit={sendNewPhoneSubmitHandler}
              />
            </Route>

            <Route exact path={`${match.url}/new-phone-confirm`}>
              <ChangePhoneCodeIntroductionContainer
                onCodeSuccessLink={profilePage}
                matchUrl={match.url}
                onSubmit={confirmNewPhoneSubmitHandler}
              />
            </Route>
          </Switch>

          <Help
            classes="registration__help"
            text="Что-то не получается?"
            small
          />
        </SideModal>
      )}
    />
  );
};
