import { ParallaxProvider } from 'react-scroll-parallax';
import React, { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { throttle } from 'lodash';
import { screenHeight, screenWidth } from './store/actions';

import './styles/main.scss';

import { RouterSwitch } from './RouterSwitch';

function App() {
  const dispatch = useDispatch();
  const resize = () => {
    dispatch(screenWidth(document.documentElement.clientWidth));
    dispatch(screenHeight(document.documentElement.clientHeight));
  };
  const throttled = throttle(resize, 1000);

  useEffect(() => {
    window.addEventListener('resize', throttled);
    return () => window.removeEventListener('resize', throttled);
  }, []);

  return (
    <Router>
      <ParallaxProvider scrollContainer={document.querySelector('.page')}>
        <RouterSwitch />
      </ParallaxProvider>
    </Router>
  );
}

export default App;
