//

// текст сообщений
const messageRequired = 'Обязательное поле';
const messageOnlyLetters = 'Только буквы';
const messageEmail = 'Некорректный формат email';
const messageTel = 'Некорректный формат телефона';
const messageDate = 'Некорректный формат даты';
// адрес
const messageFlat = 'квартиры';
const messagePorch = 'подъезда';
const messageFloor = 'этажа';
const messageAddress = 'Введите адрес в формате: улица, номер дома';

// регулярные выражения
export const patternOnlyLetters = /^[A-Za-zА-ЯЁа-яё]+$/i;
export const patternOnlyNumbers = /^[0-9]+$/i;
export const patternEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/i;
export const patternTel = /[\+][\d]{1}[\s][\d]{3}[\s][\d]{3}[\-][\d]{2}[\-][\d]{2}$/i;
export const patternDate = /[\d]{2}[\.][\d]{2}[\.][\d]{4}$/i;
// адрес
export const patternAddress = /^[а-яА-Яa-zA-Z0-9\s]+,\s?[\d]+[а-яА-Яa-zA-Z]?$/;
export const patternFlat = /^[0-9]{1,4}[а-яА-Яa-zA-Z]?$/i;
export const patternPorch = /^[0-9]{1,2}$/;
export const patternFloor = /^[0-9]{1,3}$/;
export const patternCoupon = /^[0-9]{4}$/;

// настройки для плагина валидации
// export const useFormParams = { mode: 'onBlur' };
export const useFormParams = { mode: 'onBlur', reValidateMode: 'onBlur' };

export const telLength = 16;
export const dateLength = 10;

// правила валидации для полей
export const emailRules = {
  required: messageRequired,
  pattern: { value: patternEmail, message: messageEmail },
};
export const nameRules = {
  required: messageRequired,
  pattern: { value: patternOnlyLetters, message: messageOnlyLetters },
};
export const codeRules = {
  required: true,
  maxLength: 1,
  minLength: 1,
  pattern: { value: patternOnlyNumbers },
};
export const telRules = {
  required: messageRequired,
  minLength: { value: telLength, message: messageTel },
  pattern: { value: patternTel, message: messageTel },
};
export const dateRules = {
  required: messageRequired,
  minLength: { value: dateLength, message: messageDate },
  pattern: { value: patternDate, message: messageDate },
};
// адрес
export const addressRules = {
  required: messageRequired,
  pattern: { value: patternAddress, message: messageAddress },
};
export const flatRules = {
  required: 'квартира',
  pattern: { value: patternFlat, message: messageFlat },
};
export const porchRules = {
  required: 'подъезд',
  pattern: { value: patternPorch, message: messagePorch },
};
export const floorRules = {
  required: 'этаж',
  pattern: { value: patternFloor, message: messageFloor },
};

export const couponRules = {
  required: true,
  pattern: { value: patternCoupon, message: 'Неверный формат ввода' },
};
