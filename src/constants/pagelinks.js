export const mainPage = '/';
export const stocksPage = '/stocks';
export const couponsPage = '/coupons';
export const promotionsPage = '/promotions';
export const useCoupon = '/side-modal/use-coupon';

export const restaurantsPage = '/restaurants';
export const deliveryTermPage = '/delivery-terms';
export const aboutUsPage = '/about-us';
export const aboutChainPage = '/pages/about-chain';
export const contactsPage = '/pages/contacts';
export const authPage = '/side-modal/registration';
export const shortAuthPage = '/side-modal/short-registration';
export const personalAccountPage = '/personal-account';
export const profilePage = `${personalAccountPage}/profile`;
export const currentOrderPage = `${personalAccountPage}/current-order`;
export const historyOrderPage = `${personalAccountPage}/order-history`;
export const catalogPage = '/catalog';

// модалки
export const modalCart = '/modal/cart';
export const modalStop = '/modal/stop-modal';
export const modalProduct = '/side-modal/product';
export const modalCoupon = '/side-modal/coupon';
export const modalHelp = '/side-modal/help';
export const modalHelpOrder = '/side-modal/help/order-questions';
export const modalHelpSuccess = '/side-modal/help-success';
export const useCouponModal = '/side-modal/use-coupon';
