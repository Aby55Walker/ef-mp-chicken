export const minWidth = 65
export const maxWidth = 110

export const minHoverTranslate = 10
export const maxHoverTranslate = 24
