export const catalogData = {
  data: {
    avg_delivery_time: 30,
    min_sum_delivery: 1000,
    categories: [
      {
        slug: '2asfasf',
        name: 'asdacac',
        started_at: '15:00:00',
        ended_at: '21:00:00',
        products: [
          {
            id: 7,
            name: 'ыва',
            description: 'afsadf',
            weight: 1123123,
            price: '1.00',
            pictures: {
              preview: 'http://api.chickenbox.test/asdaf',
              detail: 'http://api.chickenbox.test/asdaf',
            },
            markers: [
              {
                value: 'MARKER 1',
                action: 'wqd',
              },
              {
                value: 'MARKER 2',
                action: 'wqd2',
              },
            ],
            stocks: [],
          },
        ],
      },
      {
        slug: 'asfsaf3',
        name: 'asdacac',
        started_at: '21:00:00',
        ended_at: '22:00:00',
        products: [
          {
            id: 6,
            name: 'asdasd',
            description: 'afsadf',
            weight: 1123123,
            price: '1.00',
            pictures: {
              preview: 'http://api.chickenbox.test/asdaf',
              detail: 'http://api.chickenbox.test/asdaf',
            },
            markers: [
              {
                value: 'MARKER 1',
                action: 'wqd',
              },
              {
                value: 'MARKER 2',
                action: 'wqd2',
              },
            ],
            stocks: [],
          },
        ],
      },
      {
        slug: '4asff',
        name: 'asdacac',
        started_at: null,
        ended_at: null,
        products: [
          {
            id: 8,
            name: 'asdasd',
            description: 'afsadf',
            weight: 1123123,
            price: '1.00',
            pictures: {
              preview: 'http://api.chickenbox.test/asdaf',
              detail: 'http://api.chickenbox.test/asdaf',
            },
            markers: [],
            stocks: [],
          },
        ],
      },
      {
        slug: '1afasd',
        name: 'd12d',
        started_at: '11:00:00',
        ended_at: '14:00:00',
        products: [
          {
            id: 5,
            name: 'ыва',
            description: 'afsadf',
            weight: 1123123,
            price: '1.00',
            pictures: {
              preview: 'http://api.chickenbox.test/asdaf',
              detail: 'http://api.chickenbox.test/asdaf',
            },
            markers: [
              {
                value: 'MARKER 1',
                action: 'wqd',
              },
            ],
            stocks: [
              {
                slug: 'asdasdadsas',
                title: 'asdasdads',
                description: '<p>фывфы3</p>',
                activity: {
                  from: null,
                  to: null,
                },
                picture: {
                  preview:
                    'http://api.chickenbox.test/images/XuyB6kD9xHvYrdSvhgNMTG75mPiOzzNfeesUw93e.png',
                  detail:
                    'http://api.chickenbox.test/images/XuyB6kD9xHvYrdSvhgNMTG75mPiOzzNfeesUw93e.png',
                },
                discounts: {
                  name: 'tass',
                  unit: 'ruble',
                  value: 213,
                },
              },
            ],
          },
        ],
      },
    ],
  },
}
