import {
  DECREMENT,
  INCREMENT,
  SHOW_SLIDE_MODAL,
  SCREEN_WIDTH,
  STOP_MODAL_CONTENT,
  SET_PAYMENT_METHOD,
  ADD_PAYMENT_GOOGLE_PAY,
  ADD_PAYMENT_APPLE_PAY,
  SET_ORDER_TYPE_DELIVERY,
  SET_ORDER_TYPE_PICKUP,
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION,
  SET_NOTIFICATION_TIMEOUT_ID,
  SCREEN_HEIGHT,
} from './types';

export const increment = () => ({ type: INCREMENT });
export const decrement = () => ({ type: DECREMENT });

export const showSlideModal = (open) => ({ type: SHOW_SLIDE_MODAL, open });

export const screenWidth = (payload) => ({ type: SCREEN_WIDTH, payload });
export const screenHeight = (payload) => ({ type: SCREEN_HEIGHT, payload });

export const stopModalContent = (arr) => ({ type: STOP_MODAL_CONTENT, arr });

export const showNotification = ({ text, isError, withTimer = true }) => ({
  type: SHOW_NOTIFICATION,
  payload: {
    text,
    isError,
    withTimer,
  },
});
export const hideNotification = () => ({ type: HIDE_NOTIFICATION });
export const setNotificationTimeoutID = (payload) => ({
  type: SET_NOTIFICATION_TIMEOUT_ID,
  payload,
});

export const paymentMethod = (selected) => ({
  type: SET_PAYMENT_METHOD,
  selected,
});
export const addPaymentGooglePay = (method) => ({
  type: ADD_PAYMENT_GOOGLE_PAY,
  method,
});
export const addPaymentApplePay = (method) => ({
  type: ADD_PAYMENT_APPLE_PAY,
  method,
});

export const setOrderTypeDelivery = () => (dispatch) => {
  const root = document.documentElement;

  root.style.setProperty('--accent', '#fde936');
  root.style.setProperty('--accent-rgb', '253, 233, 54');
  root.style.setProperty('--accent-hover', '#fdec54');
  root.style.setProperty('--accent-hover-rgb', '253, 236, 84');
  root.style.setProperty('--accent-pressed', '#d7c62e');
  root.style.setProperty('--accent-pressed-rgb', '215, 198, 46');
  root.style.setProperty('--text-contrast', '#121212');

  dispatch({ type: SET_ORDER_TYPE_DELIVERY });
};
export const setOrderTypePickup = () => (dispatch) => {
  const root = document.documentElement;

  root.style.setProperty('--accent', '#e0207d');
  root.style.setProperty('--accent-rgb', '224, 32, 125');
  root.style.setProperty('--accent-hover', '#fc61b0');
  root.style.setProperty('--accent-hover-rgb', '252, 97, 176');
  root.style.setProperty('--accent-pressed', '#be1b6a');
  root.style.setProperty('--accent-pressed-rgb', '190, 27, 106');
  root.style.setProperty('--text-contrast', 'rgba(255, 255, 255, 0.9)');

  dispatch({ type: SET_ORDER_TYPE_PICKUP });
};
