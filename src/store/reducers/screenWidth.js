// screenWidth

import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../types'

const initState = {
  screenWidth: document.documentElement.clientWidth,
  screenHeight: document.documentElement.clientHeight,
}

export const screenSizeReducer = (state = initState, action) => {
  switch (action.type) {
    case SCREEN_WIDTH:
      return { ...state, screenWidth: action.payload }
    case SCREEN_HEIGHT:
      return { ...state, screenHeight: action.payload }
    default:
      return state
  }
}
