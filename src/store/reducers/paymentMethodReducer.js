// paymentMethod

import {
  applePayment, onlinePayment, cardPayment, cashPayment, googlePayPayment,
} from '../../constants/paymentMethods';

import {
  SET_PAYMENT_METHOD,
  ADD_PAYMENT_GOOGLE_PAY,
  ADD_PAYMENT_APPLE_PAY,
} from '../types';

const defaultValue = {
  selected: '',
  // способы оплаты для доставки
  delivery: {
    paymentApplePay: {
      value: applePayment,
      text: 'Apple Pay',
      hide: true,
    },
    paymentGooglePay: {
      value: googlePayPayment,
      text: 'Google Pay',
      hide: true,
    },
    paymentCardOnline: {
      value: onlinePayment,
      text: 'Оплата картой',
    },
    paymentCardCourier: {
      value: cardPayment,
      text: 'Оплата картой курьеру',
    },
    paymentCashCourier: {
      value: cashPayment,
      text: 'Оплата наличными курьеру',
    },
  },
  // способы оплаты для самовывоза
  pickup: {
    paymentApplePay: {
      value: applePayment,
      text: 'Apple Pay',
      hide: true,
    },
    paymentGooglePay: {
      value: googlePayPayment,
      text: 'Google Pay',
      hide: true,
    },
    paymentCardOnline: {
      value: onlinePayment,
      text: 'Оплата картой',
    },
  },
};

export const paymentMethodReducer = (state = defaultValue, action) => {
  switch (action.type) {
    case SET_PAYMENT_METHOD:
      return {
        ...state,
        selected: action.selected,
      };
    case ADD_PAYMENT_GOOGLE_PAY:
      return {
        selected: state.selected,
        delivery: {
          ...state.delivery,
          paymentGooglePay: {
            ...state.delivery.paymentGooglePay,
            hide: false,
          },
        },
        pickup: {
          ...state.pickup,
          paymentGooglePay: {
            ...state.pickup.paymentGooglePay,
            hide: false,
          },
        },
      };
    case ADD_PAYMENT_APPLE_PAY:
      return {
        selected: state.selected,
        delivery: {
          ...state.delivery,
          paymentApplePay: {
            ...state.delivery.paymentApplePay,
            hide: false,
          },
        },
        pickup: {
          ...state.pickup,
          paymentApplePay: {
            ...state.pickup.paymentApplePay,
            hide: false,
          },
        },
      };
    default:
      return state;
  }
};
