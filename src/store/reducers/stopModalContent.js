// stopModalContent

import { STOP_MODAL_CONTENT } from '../types';

const getSavedValue = () => {
  const data = sessionStorage.getItem(STOP_MODAL_CONTENT);
  return data ? JSON.parse(data) : [];
};

export const stopModalContent = (state = getSavedValue(), action) => {
  switch (action.type) {
    case STOP_MODAL_CONTENT:
      return action.arr;
    default:
      return state;
  }
};
