import {
  HIDE_NOTIFICATION,
  SET_NOTIFICATION_TIMEOUT_ID,
  SHOW_NOTIFICATION,
} from '../types';

const initialState = {
  notificationIsVisible: false,
  notificationText: '',
  notificationTypeIsError: false,
  notificationTimoutID: null,
  withTimer: true,
};

export const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_NOTIFICATION:
      return {
        ...state,
        notificationIsVisible: true,
        notificationText: action.payload?.text,
        notificationTypeIsError: action.payload?.isError,
        withTimer: action.payload.withTimer,
      };
    case HIDE_NOTIFICATION:
      return {
        ...state,
        notificationIsVisible: false,
      };
    case SET_NOTIFICATION_TIMEOUT_ID:
      return {
        ...state,
        notificationTimoutID: action.payload,
      };
    default:
      return state;
  }
};
