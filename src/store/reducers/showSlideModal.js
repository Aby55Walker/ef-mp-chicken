// showSlideModal

import { SHOW_SLIDE_MODAL } from '../types'

export const showSlideModal = (state = false, action) => {
  if (action.type === SHOW_SLIDE_MODAL) {
    return action.open
  } else {
    return state
  }
}
