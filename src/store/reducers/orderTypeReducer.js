import { SET_ORDER_TYPE_DELIVERY, SET_ORDER_TYPE_PICKUP } from '../types'

const initialState = {
  isOrderTypeDelivery: true,
  isOrderTypePickup: false,
  type: 'delivery', // || restaurant
}

export const orderTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ORDER_TYPE_DELIVERY:
      return {
        isOrderTypeDelivery: true,
        isOrderTypePickup: false,
        type: 'delivery',
      }
    case SET_ORDER_TYPE_PICKUP:
      return {
        isOrderTypeDelivery: false,
        isOrderTypePickup: true,
        type: 'restaurant',
      }
    default:
      return state
  }
}
