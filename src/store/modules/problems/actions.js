import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const PROBLEMS = '[problems]';
export const SEND_PROBLEMS = '[send problems]';
export const SEND_PROBLEMS_REQUEST = `${SEND_PROBLEMS} ${API_REQUEST}`;
export const PROBLEMS_REQUEST = `${PROBLEMS} ${API_REQUEST}`;
export const SET_PROBLEM_NAME = '[set problem name]';
export const SET_PROBLEM_TYPE = '[set problem type]';

// получить список проблем
export const problemsRequest = () => ({
  type: PROBLEMS_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchProblems,
      entity: PROBLEMS,
    },
  },
});

// записать выбранный вариант проблемы
export const setProblemName = (body) => ({
  type: SET_PROBLEM_NAME,
  payload: {
    data: body,
    meta: {
      entity: PROBLEMS,
    },
  },
});

// записать выбранный вариант проблемы
export const setProblemType = (body) => ({
  type: SET_PROBLEM_TYPE,
  payload: {
    data: body,
    meta: {
      entity: PROBLEMS,
    },
  },
});

export const sendProblemsRequest = (body, history, link) => ({
  type: SEND_PROBLEMS_REQUEST,
  payload: {
    data: body,
    meta: {
      apiMethod: api.sendProblems,
      entity: SEND_PROBLEMS,
      history,
      link,
    },
  },
});
