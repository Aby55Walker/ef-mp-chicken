import {
  PROBLEMS, SET_PROBLEM_NAME, SET_PROBLEM_TYPE,
} from './actions';
import { API_FETCHING, API_SUCCESS } from '../api/actions';

const initialState = {
  data: [],
  loading: false,
  problemName: '',
  problemType: null,
};

export const problemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROBLEM_NAME:
      return {
        ...state,
        problemName: action.payload.data,
      };
    case SET_PROBLEM_TYPE:
      return {
        ...state,
        problemType: action.payload.data,
      };
    case `${PROBLEMS} ${API_SUCCESS}`:
      return {
        ...state,
        data: action.payload.data,
      };
    case `${PROBLEMS} ${API_FETCHING}`:
      return { ...state };
    default:
      return state;
  }
};
