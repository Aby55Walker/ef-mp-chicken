import { ORDER_HISTORY } from './actions';
import { API_SUCCESS } from '../api/actions';

const initState = {
  data: [],
};

export const orderHistoryReducer = (state = initState, action) => {
  switch (action.type) {
    case `${ORDER_HISTORY} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };
    default: return state;
  }
};
