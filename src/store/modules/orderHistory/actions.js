import api from '../../../api/api';
import { API_REQUEST } from '../api/actions';

export const ORDER_HISTORY = '[order-history]';
export const ORDER_HISTORY_REQUEST = `${ORDER_HISTORY} ${API_REQUEST}`;

export const orderHistoryRequest = () => ({
  type: ORDER_HISTORY_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchOrderHistory,
      entity: ORDER_HISTORY,
    },
  },
});
