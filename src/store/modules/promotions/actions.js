import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const PROMOTIONS = '[promotions]';
export const PROMOTIONS_REQUEST = `${PROMOTIONS} ${API_REQUEST}`;

export const promotionsRequest = () => ({
  type: PROMOTIONS_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchPromotions,
      entity: PROMOTIONS,
    },
  },
});
