import { PROMOTIONS } from './actions';
import { API_FETCHING, API_SUCCESS } from '../api/actions';

const initialState = {
  data: {},
  loading: false,
};

export const promotionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${PROMOTIONS} ${API_SUCCESS}`:
      return {
        ...state,
        data: action.payload.data,
      };
    case `${PROMOTIONS} ${API_FETCHING}`:
      return { ...state, loading: action.payload.data };
    default:
      return state;
  }
};
