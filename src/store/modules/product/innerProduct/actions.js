import { API_REQUEST } from '../../api/actions';
import api from '../../../../api/api';

export const INNER_PRODUCT = '[inner product]';
export const INNER_PRODUCT_REQUEST = `${INNER_PRODUCT} ${API_REQUEST}`;
export const INNER_PRODUCT_RESET = `${INNER_PRODUCT} RESET`;

export const innerProductRequest = (productId) => ({
  type: INNER_PRODUCT_REQUEST,
  payload: {
    data: productId,
    meta: {
      entity: INNER_PRODUCT,
      apiMethod: api.fetchProduct,
    },
  },
});

export const INNER_PRODUCT_SET_COMMENTS = `${INNER_PRODUCT} SET COMMENTS`;
export const innerProductSetComments = ({ selected, price, id }) => ({
  type: INNER_PRODUCT_SET_COMMENTS,
  payload: {
    data: {
      selected,
      price,
      id,
    },
  },
});

export const INNER_PRODUCT_SET_MODIFIERS = `${INNER_PRODUCT} SET MODIFIERS`;
export const innerProductSetModifiers = ({ list, price, id }) => ({
  type: INNER_PRODUCT_SET_MODIFIERS,
  payload: {
    data: {
      list,
      price,
      id,
    },
  },
});

export const INNER_PRODUCT_SET_ALL_CHOSEN = `${INNER_PRODUCT} SET ALL_CHOSEN`;
export const innerProductSetAllChosen = ({ val, id }) => ({
  type: INNER_PRODUCT_SET_ALL_CHOSEN,
  payload: {
    data: {
      val,
      id,
    },
  },
});

export const innerProductReset = () => ({ type: INNER_PRODUCT_RESET });
