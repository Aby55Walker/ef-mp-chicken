import { API_SUCCESS } from '../../api/actions';
import {
  INNER_PRODUCT,
  INNER_PRODUCT_RESET,
  INNER_PRODUCT_SET_ALL_CHOSEN,
  INNER_PRODUCT_SET_COMMENTS,
  INNER_PRODUCT_SET_MODIFIERS,
} from './actions';

const initialState = {
  data: {},
  loading: false,
  state: {},
};

export const innerProductReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${INNER_PRODUCT} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };

    case INNER_PRODUCT_SET_COMMENTS:
      return {
        ...state,
        state: {
          ...state.state,
          [action.payload.data.id]: {
            ...state.state[action.payload.data.id],
            comments: action.payload.data,
          },
        },
      };

    case INNER_PRODUCT_SET_MODIFIERS:
      return {
        ...state,
        state: {
          ...state.state,
          [action.payload.data.id]: {
            ...state.state[action.payload.data.id],
            modifiers: action.payload.data,
          },
        },
      };

    case INNER_PRODUCT_SET_ALL_CHOSEN:
      return {
        ...state,
        state: {
          ...state.state,
          [action.payload.data.id]: {
            ...state.state[action.payload.data.id],
            allChosen: action.payload.data,
          },
        },
      };

    case INNER_PRODUCT_RESET: return { ...state, data: {} };
    default: return state;
  }
};
