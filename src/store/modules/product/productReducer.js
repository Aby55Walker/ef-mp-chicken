import {
  PRODUCT, PRODUCT_RESET, SET_PRODUCT_STATE, COUPON,
} from './actions';
import { API_SUCCESS } from '../api/actions';

const initialState = {
  data: {},
  loading: false,
  productState: {
    comments: {
      price: 0,
      selected: [],
    },
  },
};

export const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${COUPON} ${API_SUCCESS}`:
    case `${PRODUCT} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };
    case SET_PRODUCT_STATE: {
      return { ...state, productState: { ...state.productState, ...action.payload.data } };
    }

    case PRODUCT_RESET: return initialState;
    default: return state;
  }
};
