import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const PRODUCT = '[product]';
export const PRODUCT_REQUEST = `${PRODUCT} ${API_REQUEST}`;
export const PRODUCT_RESET = `${PRODUCT} RESET`;

// получить детальную информацию о товаре
// [productId] - ID товара
export const productRequest = (productId) => ({
  type: PRODUCT_REQUEST,
  payload: {
    data: productId,
    meta: {
      entity: PRODUCT,
      apiMethod: api.fetchProduct,
    },
  },
});

export const productReset = () => ({ type: PRODUCT_RESET });

export const SET_PRODUCT_STATE = `${PRODUCT} SET STATE`;

export const setProductState = ({ comments, modifiers }) => ({
  type: SET_PRODUCT_STATE,
  payload: {
    data: {
      comments,
      modifiers,
    },
  },
});

export const COUPON = '[coupon]';
export const COUPON_REQUEST = `${COUPON} ${API_REQUEST}`;
// получить детальную информацию о купоне
// [code] - Код купона
// [type] - Тип купона(корзины) (пример: доставка/самовывоз - delivery/restaurant)
export const couponFetch = ({ code, type }) => ({
  type: COUPON_REQUEST,
  payload: {
    data: { code, type },
    meta: {
      entity: COUPON,
      // apiMethod: api.fetchCoupon,
      method: api.fetchCoupon,
    },
  },
});
