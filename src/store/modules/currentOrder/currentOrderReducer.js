import { CURRENT_ORDER } from './actions';
import { API_FETCHING, API_SUCCESS } from '../api/actions';

const initialState = {
  data: {},
  loading: false,
};

export const currentOrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${CURRENT_ORDER} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };
    case `${CURRENT_ORDER} ${API_FETCHING}`:
      return { ...state, loading: action.payload.data };
    default: return state;
  }
};
