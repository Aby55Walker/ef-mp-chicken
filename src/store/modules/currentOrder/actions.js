import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const CURRENT_ORDER = '[current order]';
export const CURRENT_ORDER_REQUEST = `${CURRENT_ORDER} ${API_REQUEST}`;

export const currentOrderRequest = () => ({
  type: CURRENT_ORDER_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchCurrentOrder,
      entity: CURRENT_ORDER,
    },
  },
});
