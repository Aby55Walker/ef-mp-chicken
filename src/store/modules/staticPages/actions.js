import { API_REQUEST } from '../api/actions';

// контакты
export const CONTACTS_PAGE = '[contacts page]';
export const STATIC_PAGE = '[static page]';
export const STATIC_PAGE_REQUEST = `${STATIC_PAGE} ${API_REQUEST}`;
export const STATIC_PAGE_RESET = `${STATIC_PAGE} RESET`;

export const fetchStaticPage = (apiMethod) => ({
  type: STATIC_PAGE_REQUEST,
  payload: {
    meta: {
      apiMethod,
      entity: STATIC_PAGE,
      pageName: CONTACTS_PAGE,
    },
  },
});

export const resetStaticPage = () => ({ type: STATIC_PAGE_RESET });
