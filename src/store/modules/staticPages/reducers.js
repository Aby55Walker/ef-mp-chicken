import { STATIC_PAGE, STATIC_PAGE_RESET } from './actions'
import { API_SUCCESS } from '../api/actions'

const initialState = {
  data: {},
}

export const staticPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${STATIC_PAGE} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data }
    case STATIC_PAGE_RESET: {
      return initialState
    }
    default:
      return state
  }
}
