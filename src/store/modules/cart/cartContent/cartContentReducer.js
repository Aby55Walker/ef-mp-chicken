// cartContentReducer
import { CART_CONTENT, CART_UPDATE_ITEM } from './actions';
import { API_FETCHING, API_SUCCESS } from '../../api/actions';
import { CART_RESET } from '../cartMethods/actions';

const initialState = {
  data: {},
  loading: false,
};

export const cartContentReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${CART_UPDATE_ITEM} ${API_SUCCESS}`:
    case `${CART_CONTENT} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };
    case `${CART_CONTENT} ${API_FETCHING}`:
      return { ...state, loading: action.payload.data };
    case CART_RESET: return initialState;
    default:
      return state;
  }
};
