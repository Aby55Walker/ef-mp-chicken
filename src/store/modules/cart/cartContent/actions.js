import { API_REQUEST } from '../../api/actions';
import api from '../../../../api/api';
import { getFromStorage } from '../../../../functions/localStorage';

export const CART_CONTENT = '[cart content]';
export const CART_CONTENT_REQUEST = `${CART_CONTENT} ${API_REQUEST}`;

export const cartContentRequest = (cartId) => ({
  type: CART_CONTENT_REQUEST,
  payload: {
    data: cartId,
    meta: {
      apiMethod: api.fetchCartContent,
      entity: CART_CONTENT,
    },
  },
});

export const CART_UPDATE_ITEM = '[cart update item]';
export const CART_UPDATE_ITEM_REQUEST = `${CART_UPDATE_ITEM} ${API_REQUEST}`;

export const cartUpdateItem = ({
  itemId, quantity, modifiers, comments, rowId, comboItems,
}) => ({
  type: CART_UPDATE_ITEM_REQUEST,
  payload: {
    data: {
      menu_item_id: itemId,
      row_id: rowId,
      quantity,
      modifiers,
      comments,
      combo_items: comboItems,
    },
    meta: {
      apiMethod: api.cartUpdateItem,
      entity: CART_UPDATE_ITEM,
    },
  },
});
