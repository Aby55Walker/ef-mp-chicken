import { API_REQUEST } from '../../api/actions';
import api from '../../../../api/api';

export const ADD_ITEM_TO_CART = '[add item to cart]';
export const ADD_ITEM_TO_CART_REQUEST = `${ADD_ITEM_TO_CART} ${API_REQUEST}`;

export const addItemToCartRequest = (body, history, link) => ({
  type: ADD_ITEM_TO_CART_REQUEST,
  payload: {
    data: body,
    meta: {
      apiMethod: api.addItemToCart,
      entity: ADD_ITEM_TO_CART,
      history,
      link,
    },
  },
});
