import { ADD_ITEM_TO_CART } from './actions';
import { API_FETCHING, API_SUCCESS } from '../../api/actions';
import { CART_RESET } from '../cartMethods/actions';

const initialState = {
  data: {},
  loading: false,
};

export const addItemToCartReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${ADD_ITEM_TO_CART} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data, cartId: action.payload.data.id };

    case `${ADD_ITEM_TO_CART} ${API_FETCHING}`:
      return { ...state, loading: action.payload.data };
    case CART_RESET: return initialState;

    default:
      return state;
  }
};
