import api from '../../../../api/api';
import { API_REQUEST } from '../../api/actions';

export const CART_CHECKOUT = '[cart checkout]';
export const CART_CHECKOUT_REQUEST = `${CART_CHECKOUT} ${API_REQUEST}`;

// оформить заказ
export const cartCheckoutRequest = (body, history, link) => ({
  type: CART_CHECKOUT_REQUEST,
  payload: {
    data: body,
    meta: {
      method: api.cartCheckout, // вызвается только в cartCheckoutMiddleware
      entity: CART_CHECKOUT,
      history,
      link,
    },
  },
});
