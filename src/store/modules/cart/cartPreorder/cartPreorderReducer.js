import { CART_PREORDER, CART_PREORDER_REQUEST } from "./actions";
import { API_FETCHING, API_SUCCESS } from "../../api/actions";

const initialState = {
  data: {},
  loading: false,
};

export const cartPreorderReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${CART_PREORDER} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };
    case `${CART_PREORDER} ${API_FETCHING}`:
      return { ...state, loading: action.payload.data };
    default:
      return state;
  }
};
