import { API_REQUEST } from "../../api/actions";
import api from "../../../../api/api";

export const CART_PREORDER = "[cart preorder]";
export const CART_PREORDER_REQUEST = `${CART_PREORDER} ${API_REQUEST}`;

export const cartPreorderRequest = (cartId) => ({
  type: CART_PREORDER_REQUEST,
  payload: {
    data: cartId,
    meta: {
      apiMethod: api.fetchCartPreorder,
      entity: CART_PREORDER,
    },
  },
});
