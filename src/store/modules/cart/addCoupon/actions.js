export const CART_COUPON = 'CART_COUPON';
export const CART_COUPON_CREATE = `${CART_COUPON} CREATE`;
export const CART_COUPON_SET_STATUS = `${CART_COUPON} SET_STATUS`;

export const cartCouponCreate = () => ({ type: CART_COUPON_CREATE });
export const cartCouponSetStatus = ({
  index, loading, done, code,
}) => ({
  type: CART_COUPON_SET_STATUS,
  payload: {
    data: {
      index,
      loading,
      done,
      code,
    },
  },
});
