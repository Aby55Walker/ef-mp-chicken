import { CART_COUPON, CART_COUPON_CREATE, CART_COUPON_SET_STATUS } from './actions';
import { CART_RESET } from '../cartMethods/actions';

const initState = {
  couponList: [{
    index: 0,
    done: false,
    loading: false,
  }],
};

export const addCouponInCartReducer = (state = initState, action) => {
  switch (action.type) {
    case CART_COUPON_CREATE:
      return {
        ...state,
        couponList: [
          ...state.couponList,
          {
            index: state.couponList.length,
            done: false,
            loading: false,
          },
        ],
      };
    case CART_COUPON_SET_STATUS:
      return {
        ...state,
        couponList: state.couponList.map((coupon) => {
          if (coupon.index === action.payload.data.index) {
            return {
              ...coupon,
              loading: action.payload.data.loading,
              done: action.payload.data.done,
              code: action.payload.data.code,
            };
          }
          return coupon;
        }),
      };
    case CART_RESET: return initState;
    default: return state;
  }
};
