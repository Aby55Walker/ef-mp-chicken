import { combineReducers } from "redux";
import { addCouponInCartReducer } from "./addCoupon/addCouponInCartReducer";
import { cartContentReducer } from "./cartContent/cartContentReducer";
import { addItemToCartReducer } from "./cartItem/addItemToCartReducer";
import { cartPreorderReducer } from "./cartPreorder/cartPreorderReducer";

export const cartReducer = combineReducers({
  addCoupon: addCouponInCartReducer,
  addItem: addItemToCartReducer,
  content: cartContentReducer,
  preorder: cartPreorderReducer,
});
