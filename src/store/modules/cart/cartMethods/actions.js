export const CART = '[cart]';
export const CART_RESET = `${CART} RESET`;

export const cartReset = () => ({ type: CART_RESET });
