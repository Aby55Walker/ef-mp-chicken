import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const RESTAURANTS = '[restaurants]';
export const RESTAURANT_ID = '[restaurant id]';
export const RESTAURANTS_REQUEST = `${RESTAURANTS} ${API_REQUEST}`;

export const restaurantsRequest = () => ({
  type: RESTAURANTS_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchRestaurants,
      entity: RESTAURANTS,
    },
  },
});

export const setRestaurantIndexId = (body) => ({
  type: RESTAURANT_ID,
  payload: {
    data: body,
  },
});
