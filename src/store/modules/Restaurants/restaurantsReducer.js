import { RESTAURANTS, RESTAURANT_ID } from './actions';
import { API_FETCHING, API_SUCCESS } from '../api/actions';

const initialState = {
  selectedRestaurant: {
    index: null,
    id: null,
  },
  data: [],
  loading: false,
};

export const restaurantsReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESTAURANT_ID:
      return {
        ...state,
        selectedRestaurant: action.payload.data,
      };
    case `${RESTAURANTS} ${API_SUCCESS}`:
      return {
        ...state,
        data: action.payload.data,
      };
    case `${RESTAURANTS} ${API_FETCHING}`:
      return { ...state, loading: action.payload };
    default:
      return state;
  }
};
