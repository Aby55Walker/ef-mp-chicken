import { CATALOG } from './actions'
import { API_FETCHING, API_SUCCESS } from '../api/actions'

const initialState = {
  data: {},
  loading: false,
}

export const catalogReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${CATALOG} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data }

    case `${CATALOG} ${API_FETCHING}`:
      return { ...state, loading: action.payload }

    default:
      return state
  }
}
