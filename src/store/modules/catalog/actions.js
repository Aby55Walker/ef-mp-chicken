import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

export const CATALOG = '[catalog]';
export const CATALOG_REQUEST = `${CATALOG} ${API_REQUEST}`;

export const fetchCatalog = () => ({
  type: CATALOG_REQUEST,
  payload: {
    meta: {
      apiMethod: api.fetchCatalog,
      entity: CATALOG,
    },
  },
});
