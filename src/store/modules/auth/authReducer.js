import {
  AUTH_SET_PHONE,
  AUTH_SET_TOKEN,
  AUTH_LOG_OUT,
  AUTH_CODE_ERROR,
} from './actionTypes'

import { API_ERROR, API_FETCHING, API_SUCCESS } from '../api/actions'
import { AUTH, CHECK_CODE, CODE, REGISTRATION, SET_CODE } from './actions'

const initialState = {
  userToken: '',
  loading: false,
  code: '',
  phone: '',
  loggedIn: false,
  phoneSuccess: false,
  registered: false,
  codeSuccess: false,
  codeError: null,
}

const auth = (state = initialState, action) => {
  const { payload } = action

  switch (action.type) {
    // loading
    case `${AUTH} ${API_FETCHING}`:
    case `${CODE} ${API_FETCHING}`:
    case `${REGISTRATION} ${API_FETCHING}`:
      return { ...state, loading: payload.data }

    // получение данных (заоегестрирован или нет)
    case `${CODE} ${API_SUCCESS}`:
      return {
        ...state,
        registered: payload.data.registered,
      }
    // код
    case SET_CODE:
      return { ...state, code: action.payload.data }
    // проверка кода
    case `${CHECK_CODE} ${API_SUCCESS}`:
      return { ...state, codeError: null }
    case `${CHECK_CODE} ${API_ERROR}`:
      return { ...state, codeError: action.payload.data, code: '' }

    // авторизация
    case `${AUTH} ${API_SUCCESS}`:
      return {
        ...state,
        loggedIn: true,
        codeError: null,
      }
    case `${AUTH} ${API_ERROR}`:
      return {
        ...state,
        codeError: payload.data,
        loggedIn: false,
      }

    case AUTH_LOG_OUT:
      return { ...state, ...initialState }

    case AUTH_SET_PHONE:
      return { ...state, phone: payload }

    case AUTH_SET_TOKEN:
      return { ...state, userToken: payload }

    case AUTH_CODE_ERROR:
      return { ...state, codeError: action.payload }

    default:
      return state
  }
}

export default auth
