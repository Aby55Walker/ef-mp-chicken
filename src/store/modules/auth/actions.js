import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

// получение кода
export const CODE = '[code]';
export const CODE_REQUEST = `${CODE} ${API_REQUEST}`;

// код
export const SET_CODE = `${CODE} SET_CODE`;
export const setCode = (code) => ({
  type: SET_CODE,
  payload: {
    data: code,
  },
});

export const fetchCodeRequest = (body, history, link) => ({
  type: CODE_REQUEST,
  payload: {
    data: body,
    meta: {
      apiMethod: api.sendPhone,
      entity: CODE,
      withNotification: 'code',
      history,
      link,
    },
  },
});

// проверка правильности кода
export const CHECK_CODE = '[check code]';
export const CHECK_CODE_REQUEST = `${CHECK_CODE} ${API_REQUEST}`;

export const checkCodeRequest = (phone, code, history, link) => ({
  type: CHECK_CODE_REQUEST,
  payload: {
    data: { phone, code },
    meta: {
      apiMethod: api.checkCode,
      entity: CHECK_CODE,
      withNotification: 'message',
      history,
      link,
    },
  },
});

// авторизация
export const AUTH = '[auth]';
export const AUTH_REQUEST = `${AUTH} ${API_REQUEST}`;

export const authRequest = (phone, code, history, link) => ({
  type: AUTH_REQUEST,
  payload: {
    data: { phone, code },
    meta: {
      apiMethod: api.sendCode,
      entity: AUTH,
      history,
      link,
    },
  },
});

// регистрация
export const REGISTRATION = '[registration]';
export const REGISTRATION_REQUEST = `${REGISTRATION} ${API_REQUEST}`;

export const registrationRequest = (data, history, link) => ({
  type: REGISTRATION_REQUEST,
  payload: {
    data,
    meta: {
      apiMethod: api.sendRegistrationForm,
      history,
      link,
      withNotification: 'message',
      notificationText: 'Регистрация завершена',
    },
  },
});
