import {
  AUTH_SET_PHONE,
  AUTH_SET_TOKEN,
  AUTH_CODE_SUCCESS,
  AUTH_CODE_ERROR,
  AUTH_LOG_OUT,
} from './actionTypes'

// авторизация ===========
// logout
export const authLogOut = () => ({ type: AUTH_LOG_OUT })

export const authSetPhone = (payload) => ({
  type: AUTH_SET_PHONE,
  payload,
})

// отправка кода подтверждения ==========
export const authCodeError = (payload) => ({
  type: AUTH_CODE_ERROR,
  payload,
})

export const authCodeSuccess = (payload) => ({
  type: AUTH_CODE_SUCCESS,
  payload,
})

export const authSetToken = (payload) => ({
  type: AUTH_SET_TOKEN,
  payload,
})
