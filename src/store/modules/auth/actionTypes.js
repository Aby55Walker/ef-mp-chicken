// авторизация
export const AUTH_SET_USER_LOGGED_IN = '[auth] AUTH_SET_USER_LOGGED_IN'
export const AUTH_LOG_OUT = '[auth] AUTH_LOG_OUT'
export const AUTH_SET_TOKEN = '[auth] AUTH_SET_TOKEN'
// отправка телефона
export const AUTH_SET_PHONE = '[auth] AUTH_SET_PHONE'
// отправка кода подтверждения
export const AUTH_CODE_ERROR = '[auth] AUTH_CODE_ERROR'
export const AUTH_CODE_SUCCESS = '[auth] AUTH_CODE_SUCCESS'
