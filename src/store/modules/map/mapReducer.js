import {
  MAP_DESTROY,
  MAP_SET_CENTER,
  MAP_SET_INSTACE,
  MAP_SET_MARKERS,
} from './actions';

const initialState = {
  map: null,
  polygon: [
    { lat: 56.860272, lng: 53.197304 },
    { lat: 56.843557, lng: 53.20113 },
    { lat: 56.849073, lng: 53.25423 },
    { lat: 56.863919, lng: 53.278291 },
    { lat: 56.87072, lng: 53.250235 },
  ],
  center: { lat: 56.851872, lng: 53.206573 },
  markers: {},
};

export const mapReducer = (state = initialState, action) => {
  switch (action.type) {
    // меняет цетр
    case MAP_SET_CENTER:
      return { ...state, center: action.payload };
    // записывает карту в store
    case MAP_SET_INSTACE:
      return { ...state, map: action.payload };
    // записывает маркеры, нужно для смены иконок маркеров
    case MAP_SET_MARKERS:
      return { ...state, markers: action.payload };
    case MAP_DESTROY:
      window.google.maps.event.clearInstanceListeners(window);
      window.google.maps.event.clearInstanceListeners(document);
      return { ...state, map: null };
    default:
      return state;
  }
};
