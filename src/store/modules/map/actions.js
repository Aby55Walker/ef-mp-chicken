// action types
export const MAP = '[map}';
export const MAP_SET_CENTER = `${MAP} SET_CENTER`;
export const MAP_SET_INSTACE = `${MAP} SET_INSTANCE`;
export const MAP_SET_MARKERS = `${MAP} SET_MARKERS`;
export const MAP_SET_ACTIVE_MARKER = `${MAP} SET_ACTIVE_MARKER`;
export const MAP_DESTROY = `${MAP} DESTROY`;

// action creators
export const mapSetCenter = (map, coords) => ({
  type: MAP_SET_CENTER,
  payload: {
    data: {
      map,
      coords,
    },
  },
});

export const mapDestroy = () => ({ type: MAP_DESTROY });
export const mapSetInstance = (payload) => ({ type: MAP_SET_INSTACE, payload });
export const mapSetMarkers = (payload) => ({ type: MAP_SET_MARKERS, payload });
export const mapSetActiveMarker = (index) => ({
  type: MAP_SET_ACTIVE_MARKER,
  payload: index,
});
