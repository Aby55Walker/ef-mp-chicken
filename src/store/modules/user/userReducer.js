import {
  USER_DELETE_ADDRESS,
  USER_GET_DATA_ERROR,
  USER_GET_DATA_SUCCESS,
  USER_RESET,
  USER_SET_ADDRESS,
  USER_SET_DATA,
  USER_SET_LOADING,
  USER_SET_NEW_PHONE,
  USER_SET_UPDATE_ADDRESS,
  USER_UPDATE_ADDRESS,
} from './actionCreators'

const initialState = {
  data: {
    addresses: [],
    birthday: '',
    email: '',
    first_name: '',
    id: '',
    is_accept_notifications: true,
    last_name: '',
    phone: '',
    newPhone: '',
  },
  error: null,
  success: false,
  loading: false,
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_SET_LOADING:
      return { ...state, loading: action.payload }

    case USER_SET_DATA:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.payload,
        },
      }

    case USER_GET_DATA_SUCCESS:
      return { ...state, success: true, error: null }

    case USER_GET_DATA_ERROR:
      return { ...state, error: action.payload, success: false }

    case USER_RESET:
      return { ...state, ...initialState }

    case USER_SET_ADDRESS:
      return {
        ...state,
        data: {
          ...state.data,
          addresses: [...state.data.addresses, action.payload.data],
        },
      }

    case USER_SET_NEW_PHONE:
      return {
        ...state,
        data: {
          ...state.data,
          newPhone: action.payload.data,
        },
      }

    case USER_SET_UPDATE_ADDRESS:
      return {
        ...state,
        data: {
          ...state.data,
          addresses: state.data.addresses.map((address) => {
            if (address.id === action.payload.data.id) {
              return action.payload.data
            } else {
              return address
            }
          }),
        },
      }

    case USER_DELETE_ADDRESS:
      return {
        ...state,
        data: {
          ...state.data,
          addresses: state.data.addresses.filter((address) => {
            return address.id !== action.payload
          }),
        },
      }

    default:
      return state
  }
}
