export const USER = '[user]';

// ui
export const USER_SET_LOADING = `${USER} SET_LOADING`;
// user
export const USER_GET_DATA = `${USER} USER_GET_DATA`;
export const USER_RESET = `${USER} USER_RESET`;
export const USER_SET_DATA = `${USER} USER_SET_DATA`;
export const USER_GET_DATA_SUCCESS = `${USER} USER_GET_DATA_SUCCESS`;
export const USER_GET_DATA_ERROR = `${USER} USER_GET_DATA_ERROR`;

export const USER_UPDATE_DATA = `${USER} USER_UPDATE_DATA`;
export const USER_ADD_ADDRESS = `${USER} USER_ADD_ADDRESS`;
export const USER_SET_ADDRESS = `${USER} USER_SET_ADDRESS`;
export const USER_SET_NEW_PHONE = `${USER} SET_NEW_PHONE`;

export const USER_UPDATE_ADDRESS = `${USER} USER_UPDATE_ADDRESS`;
export const USER_SET_UPDATE_ADDRESS = `${USER} SET_UPDATE_ADDRESS`;

export const USER_DELETE_ADDRESS_REQUEST = `${USER} DELETE_ADDRESS_REQUEST`;
export const USER_DELETE_ADDRESS = `${USER} DELETE_ADDRESS`;

export const userSetLoading = (payload) => ({
  type: USER_SET_LOADING,
  payload,
});

export const userError = (payload) => ({
  type: USER_GET_DATA_ERROR,
  payload,
});

export const userSuccess = (payload) => ({
  type: USER_GET_DATA_SUCCESS,
  payload,
});

export const userSetData = (payload) => ({
  type: USER_SET_DATA,
  payload,
});

export const userReset = () => ({ type: USER_RESET });

export const userGetData = () => ({ type: USER_GET_DATA });

export const userUpdateData = (payload, history) => ({
  type: USER_UPDATE_DATA,
  payload,
  meta: {
    history,
  },
});

export const userSetNewPhone = (newPhone) => ({
  type: USER_SET_NEW_PHONE,
  payload: {
    data: newPhone,
  },
});

export const userAddAddress = (payload, history) => ({
  type: USER_ADD_ADDRESS,
  payload,
  meta: {
    history,
  },
});

export const userSetAddress = (payload) => ({
  type: USER_SET_ADDRESS,
  payload,
});

export const userUpdateAddress = (payload) => ({
  type: USER_UPDATE_ADDRESS,
  payload,
});
export const userSetUpdateAddress = (payload) => ({
  type: USER_SET_UPDATE_ADDRESS,
  payload,
});

export const userDeleteAddressRequest = (payload, history) => ({
  type: USER_DELETE_ADDRESS_REQUEST,
  payload,
  meta: {
    history,
  },
});

export const userDeleteAddress = (payload) => ({
  type: USER_DELETE_ADDRESS,
  payload,
});
