import {
  ORDER_REGISTRATION_TYPE,
  ORDER_REGISTRATION_READY_TIME,
  ORDER_REGISTRATION_CONTACT,
  ORDER_REGISTRATION_ADDRESS,
  ORDER_REGISTRATION_RESTAURANT_ID,
} from './actions';
// import { API_SUCCESS } from '../api/actions';

const initState = {
  ready_time: '', // datetime || null
  payment_type: '', // string
  restaurant_id: null, // id ресторана
  // type: 1, // integer тип заказа (1-Доставка, 2-Самовывоз, 3-В ресторане)
  contact: {
    name: '', // string
    phone: '', // string
  },
  address: {
    address: '', // string
    type: '', // string
    flat: '', // string
    porch: '', // string
    floor: 0, // integer
    room: '', // string
    call_intercom: true, // boolean
    is_intercom_broken: false, // boolean
    comment: '', // string
  },
};

export const orderRegistrationReducer = (state = initState, action) => {
  switch (action.type) {
    case ORDER_REGISTRATION_READY_TIME:
      return {
        ...state,
        ready_time: action.payload.data,
      };
    case ORDER_REGISTRATION_RESTAURANT_ID:
      return {
        ...state,
        restaurant_id: action.payload.data,
      };
    case ORDER_REGISTRATION_CONTACT:
      return {
        ...state,
        contact: action.payload.data,
      };
    case ORDER_REGISTRATION_TYPE:
      return {
        ...state,
        type: action.payload.data,
      };
    case ORDER_REGISTRATION_ADDRESS:
      return {
        ...state,
        address: action.payload.data,
      };
    default:
      return state;
  }
};
