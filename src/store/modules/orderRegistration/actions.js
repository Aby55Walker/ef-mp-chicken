export const ORDER_REGISTRATION = 'order registration';
export const ORDER_REGISTRATION_CONTACT = `[${ORDER_REGISTRATION} contact]`;
export const ORDER_REGISTRATION_ADDRESS = `[${ORDER_REGISTRATION} address]`;
export const ORDER_REGISTRATION_READY_TIME = `[${ORDER_REGISTRATION} ready time]`;
export const ORDER_REGISTRATION_RESTAURANT_ID = `[${ORDER_REGISTRATION} restaurant id]`;
export const ORDER_REGISTRATION_TYPE = `[${ORDER_REGISTRATION} type]`;

export const setContactOrderRegistration = (body) => ({
  type: ORDER_REGISTRATION_CONTACT,
  payload: {
    data: body,
  },
});

export const setRestaurantIdOrderRegistration = (body) => ({
  type: ORDER_REGISTRATION_RESTAURANT_ID,
  payload: {
    data: body,
  },
});

export const setTypeOrderRegistration = (body) => ({
  type: ORDER_REGISTRATION_TYPE,
  payload: {
    data: body,
  },
});

export const setReadyTimeOrderRegistration = (body) => ({
  type: ORDER_REGISTRATION_READY_TIME,
  payload: {
    data: body,
  },
});

export const setAddressOrderRegistration = (body) => ({
  type: ORDER_REGISTRATION_ADDRESS,
  payload: {
    data: body,
  },
});
