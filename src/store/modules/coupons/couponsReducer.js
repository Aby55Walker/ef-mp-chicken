import { COUPONS } from './actions';
import { API_FETCHING, API_SUCCESS } from '../api/actions';

const initState = {
  loading: false,
  done: false,
  data: [],
};

export const couponReducer = (state = initState, action) => {
  switch (action.type) {
    case `${COUPONS} ${API_SUCCESS}`:
      return { ...state, data: action.payload.data };

    case `${COUPONS} ${API_FETCHING}`:
      return { ...state, loading: action.payload };

    default: return state;
  }
};
