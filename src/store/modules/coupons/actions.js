import { API_REQUEST } from '../api/actions';
import api from '../../../api/api';

// export const COUPON = 'COUPON';
export const COUPONS = 'COUPONS';
export const ADD_COUPON = `${COUPONS} ADD`;
export const COUPONS_REQUEST = `${COUPONS} ${API_REQUEST}`;
// export const COUPON_REQUEST = `${COUPON} ${API_REQUEST}`;

export const addCoupon = (payload) => ({
  type: ADD_COUPON,
});

// получить список купонов
// [type] - доставка/самовывоз - delivery/restaurant
export const couponsRequest = (type) => ({
  type: COUPONS_REQUEST,
  payload: {
    data: type,
    meta: {
      apiMethod: api.fetchCoupons,
      entity: COUPONS,
    },
  },
});

// получить детальнаю информацию о купоне
// [code] - код купона
// [type] - доставка/самовывоз - delivery/restaurant
// export const couponRequest = ({ code, type }) => ({
//   type: COUPON_REQUEST,
//   payload: {
//     data: { code, type },
//     meta: {
//       apiMethod: api.fetchCoupon,
//       entity: COUPON,
//     },
//   },
// });
