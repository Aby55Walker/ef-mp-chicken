import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { rootReducer } from './rootReducer';
import { loadState, saveState } from '../functions/localStorage';
import { setOrderTypeDelivery, setOrderTypePickup } from './actions';
import middleWares from './middlewares';

const persistedState = loadState();

const store = createStore(
  rootReducer,
  persistedState,
  compose(
    applyMiddleware(thunk, logger, ...middleWares),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  ),
);

const subscribe = () => {
  const {
    notification, auth, user, orderType, cart, orderRegistration, problems,
  } = store.getState();

  saveState({
    notification,
    auth,
    user,
    orderType,
    cart,
    orderRegistration,
    problems,
  });
};

store.subscribe(subscribe);

window.store = store;
// устанавливает цвет интерфейса (доставка или самовывоз)
persistedState && persistedState.orderType?.isOrderTypeDelivery
  ? store.dispatch(setOrderTypeDelivery())
  : store.dispatch(setOrderTypePickup());

export default store;
