// cartCheckout
import { COUPON_REQUEST } from '../modules/product/actions';
import { showNotification, stopModalContent } from '../actions';
import formatResponseError from '../../functions/formatResponseError';
import { currentOrderPage, modalStop } from '../../constants/pagelinks';
import {
  API_REQUEST,
  apiError,
  apiSuccess,
  apiFetching,
} from '../modules/api/actions';

export const couponFetchMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  // если в action.type есть CART_CHECKOUT_REQUEST, то делается запрос переданным методом
  if (action.type.includes(COUPON_REQUEST)) {
    const {
      method, // функция запроса из API
      entity,
      history, // history для того, чтобы перейти по ссылке после загрузки
    } = action.payload.meta;
    const { data } = action.payload; // data для передачи данных в запросе

    if (!method) return;

    method(data)
      .then((res) => {
        const couponData = res.data.data;

        const { items, code } = couponData;
        couponData.comboItems = items;
        couponData.id = code;

        console.log('DATA COUPON', couponData);

        const response = { data: { data: { ...couponData } } };

        // используется для обновления данных в store
        dispatch(apiSuccess(response, entity));
      });
  }
};
