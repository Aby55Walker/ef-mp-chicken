import { notificationMiddleware } from './notification';
import { userMiddleware } from './user';
import { mapMiddleware } from './map';
import { apiMiddleware } from './api';
import { authMiddleware } from './auth';
import { cartCheckoutMiddleware } from './cartCheckout';
import { couponFetchMiddleware } from './couponFetch';

export default [
  notificationMiddleware,
  userMiddleware,
  mapMiddleware,
  apiMiddleware,
  authMiddleware,
  cartCheckoutMiddleware,
  couponFetchMiddleware,
];
