import {
  API_REQUEST,
  apiError,
  apiSuccess,
  apiFetching,
} from '../modules/api/actions';
import { showNotification } from '../actions';
import formatResponseError from '../../functions/formatResponseError';

export const apiMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  // если в action.type есть API_REQUEST, то делается запрос переданным методом
  if (action.type.includes(API_REQUEST)) {
    const {
      apiMethod, // функция запроса из API
      entity, // имя сущности, например [user], [cart] и т.д.
      withNotification, // [message] || [code], отвечает за показ алерта
      notificationText, // текст алерта
      history, // history для того, чтобы перейти по ссылке после загрузки
      link, // ссылка, по которой нужно перейти
    } = action.payload.meta;
    // data для передачи данных в запросе
    const { data } = action.payload;

    dispatch(apiFetching(entity, true));

    if (!apiMethod) return false;

    apiMethod(data)
      .then((response) => {
        // используется для обновления данных в store
        dispatch(apiSuccess(response, entity));
        // используется для отображения состояния "loading"
        dispatch(apiFetching(entity, false));
        // алерт/уведомление
        // если передан history, то переходит по ссылке после apiSuccess
        if (history) history.push(link);

        if (withNotification) {
          dispatch(
            showNotification({
              text: notificationText || response.data.data[withNotification],
              isError: false,
            }),
          );
        }
      })
      .catch((e) => {
        dispatch(apiFetching(entity, false));
        // алерт/уведомление
        if (withNotification) {
          dispatch(
            showNotification({
              text: formatResponseError(e),
              isError: true,
            }),
          );
        }
        dispatch(apiError(e, entity));
      });
  }
};
