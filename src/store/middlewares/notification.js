import { SHOW_NOTIFICATION } from '../types';
import { hideNotification, setNotificationTimeoutID } from '../actions';
import { notificationTimeout } from '../../constants/ui';

export const notificationMiddleware = ({ dispatch, getState }) => (next) => (
  action,
) => {
  const {
    notificationIsVisible,
    notificationTimoutID,
  } = getState().notification;

  if (action.type === SHOW_NOTIFICATION) {
    if (!notificationIsVisible) {
      const timeoutID = setTimeout(() => {
        dispatch(hideNotification());
      }, notificationTimeout);
      dispatch(setNotificationTimeoutID(timeoutID));
    } else if (notificationIsVisible) {
      // если уведомление уже показано, то обновляет таймер и удаляет старый таймер
      clearTimeout(notificationTimoutID);
      const timeoutID = setTimeout(() => {
        if (action.payload.withTimer) {
          dispatch(hideNotification());
        }
      }, notificationTimeout);
      dispatch(setNotificationTimeoutID(timeoutID));
    }
  }

  next(action);
};
