import { cartReset } from '../modules/cart/cartMethods/actions';
import { AUTH_LOG_OUT } from '../modules/auth/actionTypes';

export const authMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  if (action.type === AUTH_LOG_OUT) {
    dispatch(cartReset());
  }
};
