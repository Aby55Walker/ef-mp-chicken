// cartCheckout
import { CART_CHECKOUT_REQUEST } from '../modules/cart/cartCheckout/actions';
import { showNotification, stopModalContent } from '../actions';
import formatResponseError from '../../functions/formatResponseError';
import { currentOrderPage, modalStop } from '../../constants/pagelinks';

export const cartCheckoutMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  // если в action.type есть CART_CHECKOUT_REQUEST, то делается запрос переданным методом
  if (action.type.includes(CART_CHECKOUT_REQUEST)) {
    const withNotification = 'message';
    const notificationText = 'Ваш заказ успешно оформлен';

    const {
      method, // функция запроса из API
      history, // history для того, чтобы перейти по ссылке после загрузки
    } = action.payload.meta;
    const { data } = action.payload; // data для передачи данных в запросе

    method(data)
      .then((response) => {
        const cartChanges = response.data.data['cart-changes'];

        if (history) {
          if (cartChanges) {
            dispatch(stopModalContent(cartChanges)); // записать данные
            history.push(`${history.location.pathname}${modalStop}`); // открыть stop-modal
          } else {
            // перейти на страницу Текущий заказ
            history.push(currentOrderPage);
          }
        }

        // алерт/уведомление
        if (withNotification && !cartChanges) {
          dispatch(
            showNotification({
              text: notificationText || response.data.data[withNotification],
              isError: false,
            }),
          );
        }
      })
      .catch((e) => {
        // алерт/уведомление
        if (withNotification) {
          dispatch(
            showNotification({
              text: formatResponseError(e),
              isError: true,
            }),
          );
        }
      });
  }
};
