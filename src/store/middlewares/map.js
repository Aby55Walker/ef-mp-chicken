import { MAP_SET_ACTIVE_MARKER, MAP_SET_CENTER } from '../modules/map/actions'
import IconPinYellow from '../../assets/pin_yellow.png'
import IconPinPink from '../../assets/pin_pink.png'
import MarkerYellow from '../../assets/icons/map-marker_yellow.svg'
import MarkerPink from '../../assets/icons/map-marker_pink.svg'

export const mapMiddleware = (store) => (next) => (action) => {
  next(action)

  if (action.type === MAP_SET_ACTIVE_MARKER) {
    const { isOrderTypeDelivery } = store.getState().orderType
    const { markers, map } = store.getState().map

    const { lat, lng } = store
      .getState()
      .map.markers[action.payload].getPosition()
    map.setCenter(new window.google.maps.LatLng(lat(), lng()))
    map.setZoom(15)
    markers.forEach((marker, i) => {
      if (i === action.payload) {
        marker.setIcon(isOrderTypeDelivery ? IconPinYellow : IconPinPink)
      } else {
        marker.setIcon(isOrderTypeDelivery ? MarkerYellow : MarkerPink)
      }
    })
  }
}
