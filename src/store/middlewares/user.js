import { AUTH_LOG_OUT } from '../modules/auth/actionTypes'
import {
  USER_UPDATE_DATA,
  USER_ADD_ADDRESS,
  USER_DELETE_ADDRESS_REQUEST,
  USER_GET_DATA,
  USER_UPDATE_ADDRESS,
  userDeleteAddress,
  userError,
  userReset,
  userSetAddress,
  userSetData,
  userSetLoading,
  userSetUpdateAddress,
  userSuccess,
  userUpdateAddress,
} from '../modules/user/actionCreators'
import Api from '../../api/api'
import formatError from '../../functions/formatResponseError'
import { showNotification } from '../actions'

export const userMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action)

  if (action.type === AUTH_LOG_OUT) {
    dispatch(userReset())
  }

  if (action.type === USER_UPDATE_DATA) {
    dispatch(userSetLoading(true))
    Api.userUpdateData(action.payload)
      .then((response) => {
        dispatch(userSetData(response.data.data))
        dispatch(userSuccess())
        dispatch(userSetLoading(false))
        action.meta.history.push('/personal-account/profile')
        dispatch(
          showNotification({
            text: 'Данные обновлены',
            isError: false,
          })
        )
      })
      .catch((e) => {
        dispatch(userError(formatError(e)))
        dispatch(userSetLoading(false))
        dispatch(
          showNotification({
            text: formatError(e),
            isError: true,
          })
        )
      })
  }

  if (action.type === USER_GET_DATA) {
    Api.getUser()
      .then((response) => {
        dispatch(userSetData(response.data.data))
        dispatch(userSuccess())
      })
      .catch((e) => {
        dispatch(userError(formatError(e)))
        dispatch(
          showNotification({
            text: formatError(e),
            isError: true,
          })
        )
      })
  }

  if (action.type === USER_ADD_ADDRESS) {
    dispatch(userSetLoading(true))
    Api.addUserAddress(action.payload)
      .then((response) => {
        const isUpdate = !!action.payload.id
        dispatch(userSetLoading(false))
        if (isUpdate) {
          dispatch(userSetUpdateAddress(response.data))
        } else {
          dispatch(userSetAddress(response.data))
        }

        action.meta.history.push('/personal-account/profile')

        dispatch(userSuccess())
        dispatch(
          showNotification({
            text: isUpdate ? 'Адрес обновлён' : 'Адрес добавлен',
            isError: false,
          })
        )
      })
      .catch((e) => {
        dispatch(userSetLoading(false))
        dispatch(userError(formatError(e)))
        dispatch(
          showNotification({
            text: formatError(e),
            isError: true,
          })
        )
      })
  }

  if (action.type === USER_DELETE_ADDRESS_REQUEST) {
    dispatch(userSetLoading(true))
    Api.deleteAddress(action.payload.data)
      .then((response) => {
        dispatch(userSetLoading(false))
        dispatch(userDeleteAddress(action.payload.data))
        dispatch(userSuccess())
        action.payload.meta.history.push('/personal-account/profile')
        dispatch(
          showNotification({
            text: response.data.data.message,
            isError: false,
          })
        )
      })
      .catch((e) => {
        dispatch(userSetLoading(false))
        dispatch(userError(formatError(e)))
        dispatch(
          showNotification({
            text: formatError(e),
            isError: true,
          })
        )
      })
  }
}
