import { combineReducers } from 'redux';
import { counterReducer } from './reducers/counterReducer';
import { showSlideModal } from './reducers/showSlideModal';
import { screenSizeReducer } from './reducers/screenWidth';
import { stopModalContent } from './reducers/stopModalContent';
import auth from './modules/auth/authReducer';
import { orderTypeReducer } from './reducers/orderTypeReducer';
import { notificationReducer } from './reducers/notificationReducer';
import { paymentMethodReducer } from './reducers/paymentMethodReducer';
import { userReducer } from './modules/user/userReducer';
import { mapReducer } from './modules/map/mapReducer';
import { staticPageReducer } from './modules/staticPages/reducers';
import { catalogReducer } from './modules/catalog/catalogReducer';
import { restaurantsReducer } from './modules/Restaurants/restaurantsReducer';
import { promotionsReducer } from './modules/promotions/promotionsReducer';
import { problemsReducer } from './modules/problems/problemsReducer';
import { cartReducer } from './modules/cart/cartReducer';
import { orderHistoryReducer } from './modules/orderHistory/orderHistoryReducer';
import { productReducer } from './modules/product/productReducer';
import { currentOrderReducer } from './modules/currentOrder/currentOrderReducer';
import { couponReducer } from './modules/coupons/couponsReducer';
import { orderRegistrationReducer } from './modules/orderRegistration/orderRegistrationReducer';
import { innerProductReducer } from './modules/product/innerProduct/innerProductReducer';

export const rootReducer = combineReducers({
  notification: notificationReducer,
  counter: counterReducer,
  showSlideModal,
  screenSize: screenSizeReducer,
  stopModalContent,
  auth,
  staticPage: staticPageReducer,
  orderType: orderTypeReducer,
  paymentMethod: paymentMethodReducer,
  user: userReducer,
  map: mapReducer,
  catalog: catalogReducer,
  restaurants: restaurantsReducer,
  promotions: promotionsReducer,
  problems: problemsReducer,
  cart: cartReducer,
  orderHistory: orderHistoryReducer,
  product: productReducer,
  innerProduct: innerProductReducer,
  orderRegistration: orderRegistrationReducer,
  currentOrder: currentOrderReducer,
  coupons: couponReducer,
});
