export const INCREMENT = '[app] INCREMENT'
export const DECREMENT = '[app] DECREMENT'

export const SHOW_SLIDE_MODAL = '[app] SHOW_SLIDE_MODAL'

export const SCREEN_WIDTH = '[app] SCREEN_WIDTH'
export const SCREEN_HEIGHT = '[app] SCREEN_HEIGHT'
// передать контент в модалку стоп
export const STOP_MODAL_CONTENT = '[app] STOP_MODAL_CONTENT'
// доставка или самовывоз
export const SET_ORDER_TYPE_DELIVERY = '[app] SET_ORDER_TYPE_DELIVERY'
export const SET_ORDER_TYPE_PICKUP = '[app] SET_ORDER_TYPE_PICKUP'

export const SET_PAYMENT_METHOD = '[app] SET_PAYMENT_METHOD'
export const ADD_PAYMENT_GOOGLE_PAY = '[app] ADD_PAYMENT_GOOGLE_PAY'
export const ADD_PAYMENT_APPLE_PAY = '[app] ADD_PAYMENT_APPLE_PAY'

export const SHOW_NOTIFICATION = '[app] SHOW_NOTIFICATION'
export const HIDE_NOTIFICATION = '[app] HIDE_NOTIFICATION'
export const SET_NOTIFICATION_TIMEOUT_ID = '[app] SET_NOTIFICATION_TIMEOUT'
