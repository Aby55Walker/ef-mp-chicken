import React, { useEffect } from 'react';
import SmoothScroll from 'smooth-scroll';

export const useSmoothScroll = (onScrollStart, onScrollStop) => {
  useEffect(() => {
    const scroll = new SmoothScroll('a[href*="#"]', {
      speed: 500,
      speedAsDuration: true,
      offset: 100,
    });
    if (onScrollStart || onScrollStop) {
      document.addEventListener('scrollStart', onScrollStart, false);
      document.addEventListener('scrollStop', onScrollStop, false);
    }
    return () => {
      scroll.destroy();
      if (onScrollStart || onScrollStop) {
        document.removeEventListener('scrollStart', onScrollStart, false);
        document.removeEventListener('scrollStop', onScrollStop, false);
      }
    };
  }, []);
};
