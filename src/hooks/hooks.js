import { useEffect, useState } from 'react'
import getGeoLocation from '../functions/getGeoLocation'
import { randomInteger } from '../functions/randomInteger'

export const useGeolocation = () => {
  const [userCoords, setUserCoords] = useState([])

  // определяет местоположения пользователя
  useEffect(() => {
    getGeoLocation()
      .then(([latitude, longitude]) => {
        setUserCoords([latitude, longitude])
      })
      .catch((e) => {
        console.log(e.message)
      })
  }, [])

  return userCoords
}

export const useRandomClass = (arrayOfClasses) => {
  const [themeClass, setThemeClass] = useState(arrayOfClasses)

  useEffect(() => {
    const withTheme = randomInteger(0, 2) // bool (добавлять класс или нет)
    if (withTheme) {
      setThemeClass('')
    } else {
      const themeIndex = randomInteger(0, arrayOfClasses.length - 1)
      setThemeClass(arrayOfClasses[themeIndex])
    }
  }, [])

  return themeClass
}

export const useCSSProperty = (propName) => {
  const [value, setValue] = useState('')

  useEffect(() => {
    const styles = getComputedStyle(document.documentElement)
    const propValue = styles.getPropertyValue(propName)
    setValue(propValue)
  }, [])

  return value
}
