import React, { useEffect, useState } from 'react';

export const useSwipeClose = (maxDiff, closeCallback) => {
  const maxSwipeDiff = maxDiff;

  const [swipeStyles, setSwipeStyles] = useState({});
  const [swipeParams, setSwipeParams] = useState({
    opacity: 1,
    completePercent: 0,
  });

  // swipe
  const touchStartHandler = (e) => {
    const yPos = e.touches[0].clientY;
    e.persist();
    setSwipeParams((prevState) => ({
      ...prevState,
      initialTop: yPos,
      moveTop: yPos,
      completePercent: 0,
      top: 0,
    }));
  };

  const touchMoveHandler = (e) => {
    const yPos = e.touches[0].clientY;
    const swipeCompletePercent = (yPos - swipeParams.initialTop) / (maxSwipeDiff * 2);

    setSwipeParams((prevState) => ({
      ...prevState,
      moveTop: yPos,
      completePercent: swipeCompletePercent,
    }));
    setSwipeStyles({
      ...swipeStyles,
      transition: '',
    });
  };

  const touchEndHandler = (e) => {
    const yPos = e.changedTouches[0].clientY;

    if (yPos - swipeParams.initialTop > maxSwipeDiff) {
      closeCallback();
    } else {
      setSwipeParams((prevState) => ({
        ...prevState,
        moveTop: prevState.initialTop,
        completePercent: 0,
      }));
      setSwipeStyles({
        ...swipeStyles,
        transform: '',
        opacity: 1,
        transition: 'all .2s linear',
      });
    }
  };

  // меняет стили модалки при свайпе
  useEffect(() => {
    const top = swipeParams.moveTop - swipeParams.initialTop;
    const opacityCalc = 1.5 - swipeParams.completePercent;
    const scaleCalc = 1 - swipeParams.completePercent / 10;

    setSwipeStyles({
      ...swipeStyles,
      transform: `scale(${scaleCalc})`,
      top: `${top}px`,
      opacity: opacityCalc,
    });
  }, [swipeParams]);

  return {
    touchStartHandler,
    touchMoveHandler,
    touchEndHandler,
    swipeStyles,
  };
};
