import React from 'react'
import { useEffect } from 'react'

export const useFullWidthLayout = () => {
  // дулает ширину футера и хедера на весь экран
  useEffect(() => {
    let elems = []

    const headerContainer = document.querySelector('.header .container')
    const footerContainers = document.querySelectorAll('.footer .container')
    elems = [headerContainer, ...footerContainers]

    if (elems.length) {
      elems.forEach((elem) => elem.classList.add('catalog-page__container'))
    }
    return () =>
      elems.forEach((elem) => elem.classList.remove('catalog-page__container'))
  }, [])
}
