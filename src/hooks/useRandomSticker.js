import sticker1 from '../assets/images/sticker-1@2x.png'
import sticker2 from '../assets/images/sticker-2@2x.png'
import sticker3 from '../assets/images/sticker-3@2x.png'
import sticker4 from '../assets/images/sticker-4@2x.png'
import { randomInteger } from '../functions/randomInteger'

const stickers = [sticker1, sticker2, sticker3, sticker4]
export const randomSticker = () => stickers[randomInteger(0, stickers.length - 1)]
