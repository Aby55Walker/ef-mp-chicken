//

/**
 * возвращает дату в виде строки дд.мм.гггг
 *
 * @param {string} dateString
 * @returns {string} дд.мм.гггг
 */
export default (dateString) => {
  if (!dateString) return null

  const date = new Date(dateString)

  let day = date.getDate()
  let month = date.getMonth() + 1 // getMonth() возвращает число от 0 до 11, поэтому прибавляем 1
  let year = date.getFullYear()

  if (day < 10) day = `0${day}`
  if (month < 10) month = `0${month}`

  return `${day}.${month}.${year}`
}
