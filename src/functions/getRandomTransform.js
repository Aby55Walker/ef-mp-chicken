import { randomInteger } from './randomInteger'

export const getRandomTransform = (min, max) => {
  const firstPositiveOrNegative = randomInteger(0, 1) ? '-' : ''
  const secondPositiveOrNegative = randomInteger(0, 1) ? '-' : ''
  return `
    translate(
      ${firstPositiveOrNegative}${randomInteger(min, max)}px,
      ${secondPositiveOrNegative}${randomInteger(min, max)}px
    )`
}
