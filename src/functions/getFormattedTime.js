//

/**
 * возвращает время в виде строки чч:мм
 *
 * @param {string} dateString
 * @returns {string} чч:мм
 */
export default (dateString) => {
  if (!dateString) return null;

  const date = new Date(dateString);

  const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
  const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();

  return `${hours}:${minutes}`;
};
