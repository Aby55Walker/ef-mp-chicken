// deleteSlashAtTheEnd

/**
 * возвращает строку с удалённым слешем на конце
 *
 * @param {string} path
 * @returns {string}
 */
export default (path) => {
  if (!path) return null

  return path.charAt(path.length - 1) === '/' ? path.slice(0, -1) : path
}
