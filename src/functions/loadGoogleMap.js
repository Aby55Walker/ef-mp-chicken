// loadGoogleMap
import { googleMapURL } from '../constants/googleMapsApi';

export const loadGoogleMap = () => new Promise((resolve) => {
  const id = 'googleMap';
  const existingScript = document.getElementById(id);

  if (!existingScript) {
    const script = document.createElement('script');
    script.src = googleMapURL;
    script.id = id;
    document.body.appendChild(script);

    script.onload = () => {
      resolve(true);
    };
  }

  if (existingScript) resolve(true);
});
