// loadApplePay

export const loadApplePay = (callback) => {
  const existingScript = document.getElementById('applePay');

  if (!existingScript) {
    const script = document.createElement('script');
    script.src = 'https://js.braintreegateway.com/web/3.64.2/js/apple-pay.min.js';
    script.id = 'applePay';
    document.body.appendChild(script);

    script.onload = () => {
      // onGooglePayLoaded();
      if (callback) callback();
    };
  }

  if (existingScript && callback) callback();
};

// после загрузки скрипта
// const onGooglePayLoaded = () => {
//   window.googlePayClient = new window.google.payments.api.PaymentsClient({
//     environment: 'TEST',
//   });
// };
