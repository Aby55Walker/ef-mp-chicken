// loadGooglePay.js

export const loadGooglePay = (callback) => {
  const existingScript = document.getElementById('googlePay');

  if (!existingScript) {
    const script = document.createElement('script');
    script.src = 'https://pay.google.com/gp/p/js/pay.js';
    script.id = 'googlePay';
    document.body.appendChild(script);

    script.onload = () => {
      onGooglePayLoaded();
      if (callback) callback();
    };
  }

  if (existingScript && callback) callback();
};

// после загрузки скрипта
const onGooglePayLoaded = () => {
  window.googlePayClient = new window.google.payments.api.PaymentsClient({
    environment: 'TEST',
  });
};
