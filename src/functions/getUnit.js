// getUnit

export default (unit) => (unit === 'percentage' ? '%' : unit === 'ruble' ? '₽' : '');
