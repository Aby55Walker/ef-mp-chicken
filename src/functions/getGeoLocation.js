const options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0,
}

const success = ({ coords: { latitude, longitude } }, resolve) => {
  resolve([latitude, longitude])
}

const error = (e, reject) => {
  reject(e)
}

const getGeoLocation = () => {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      (pos) => success(pos, resolve),
      (e) => error(e, reject),
      options
    )
  })
}

export default getGeoLocation
