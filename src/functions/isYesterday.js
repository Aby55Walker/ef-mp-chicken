/**
 * проверить является ли указанная дата вчерашней
 *
 * @param {string} dateString
 * @returns {boolean}
 */
export default (dateString) => {
  if (!dateString) return null;

  let checkingDate = new Date(dateString); // проверяемая дата

  let yesterday = new Date(); // вчерашняя дата
  yesterday.setDate(yesterday.getDate() - 1);

  checkingDate = checkingDate.toISOString().split('T')[0];
  yesterday = yesterday.toISOString().split('T')[0];

  return checkingDate === yesterday;
};
