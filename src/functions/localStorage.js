export function getFromStorage(key) {
  return JSON.parse(localStorage.getItem(key))
}

export function setToStorage(key, payload) {
  localStorage.setItem(key, JSON.stringify(payload))
}

export function removeFromStorage(key) {
  localStorage.removeItem(key)
}

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === null) {
      return undefined
    }
    console.log(JSON.parse(serializedState))
    return JSON.parse(serializedState)
  } catch (e) {
    console.log(e + 'loadState Error')
    return undefined
  }
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch (e) {}
}
