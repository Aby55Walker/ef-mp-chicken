export const makeMaskedPhone = (phone) => {
  // делает из 79123456789 => +7 912 345 67 89

  if (phone.includes('+')) {
    return phone
  }

  let unmaskedPhone = phone
  return `+${unmaskedPhone.substr(0, 1)} ${unmaskedPhone.substr(
    1,
    3
  )} ${unmaskedPhone.substr(4, 3)}-${unmaskedPhone.substr(
    7,
    2
  )}-${unmaskedPhone.substr(9, 2)}`
}
