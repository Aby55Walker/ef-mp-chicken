// getSearchParams

export default (id) => {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(id);
};
