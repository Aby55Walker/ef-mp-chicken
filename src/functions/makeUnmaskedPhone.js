export const makeUnmaskedPhone = (phone) => {
  return phone.replace(/[\s+-]+/gi, '')
}
