export default function formatError(e) {
  // если в ошибке есть ограничение по времени

  if (!e || !e.response) return 'Неизвестная ошибка';

  const { data } = e.response.data;

  if (data.retry_after) {
    return `${data.message}, повторите попытку через ${data.retry_after} сек.`;
  }
  // если массив ошибок
  if (data.errors) {
    const { errors } = e.response.data.data;
    let errorString = '';
    for (const key in errors) {
      if (errors.hasOwnProperty(key)) {
        errorString += `${errors[key][0]} `;
      }
    }
    return errorString;
  }
  // просто сообщение ошибки из API
  console.error(e);
  return data.message;
}
