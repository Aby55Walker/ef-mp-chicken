// checkAvailabilityGooglePay

// TODO указать нужные способы оплаты
// поддерживаемые способы оплаты (для примера)
const baseCardPaymentMethod = {
  type: 'CARD',
  parameters: {
    allowedCardNetworks: ['VISA', 'MASTERCARD'],
    allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
  },
}

// api versoin
const googlePayBaseConfiguration = {
  apiVersion: 2,
  apiVersionMinor: 0,
  allowedPaymentMethods: [baseCardPaymentMethod],
}

// проверить доступность GooglePay
const checkAvailabilityGooglePay = (callback) => {
  if (window.googlePayClient) {
    window.googlePayClient
      .isReadyToPay(googlePayBaseConfiguration)
      .then((res) => {
        if (res.result) {
          console.log('Google Play is supported')
          if (callback) callback(true)
        } else {
          console.log('Unable to pay using Google Pay')
          if (callback) callback(false)
        }
      })
      .catch(function (err) {
        console.error('Error determining readiness to use Google Pay: ', err)
        if (callback) callback(false)
      })
  }
}

export default checkAvailabilityGooglePay
