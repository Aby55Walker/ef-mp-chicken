export const toCamelCase = (str) => {
  return str.replace(/_([a-z])/g, (x, up) => up.toUpperCase())
}
