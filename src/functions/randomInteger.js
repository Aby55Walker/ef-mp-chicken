export const randomInteger = (min, max) => {
  // случайное число от min до max
  let rand = min + Math.random() * (max + 1 - min)
  return Math.floor(rand)
}
