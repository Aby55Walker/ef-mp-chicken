import { throttle } from 'lodash';

let observer = null;

export const initIntersectionObserver = (refs, setActiveItem, dontObserve, root = null) => {
  if (observer !== null) {
    observer.disconnect();
    observer = null;
  }
  const screenHeight = window.innerHeight;
  const observerMargin = Math.floor(screenHeight);
  const observerConfig = {
    root,
    rootMargin: `-${
      365
    }px 0px -${observerMargin - 366}px 0px`,
  };

  const handleIntersection = (entries) => {
    const observeHandler = throttle((entry) => {
      if (dontObserve) return;
      if (entry.isIntersecting) {
        setActiveItem(entry.target.id);
      }
    }, 100);

    entries.forEach(observeHandler);
  };

  // observe each target
  observer = new IntersectionObserver(
    handleIntersection,
    observerConfig
  );

  refs.forEach((r) => observer.observe(r.current));
};
