// checkAvailabilityApplePay

// проверить доступность ApplePay
const checkAvailabilityApplePay = (callback) => {
  if (window.ApplePaySession) {
    const merchantIdentifier = 'example.com.store';

    // поддерживает Apple Pay и есть ли активная карта в кошельке
    const promise = window.ApplePaySession.canMakePaymentsWithActiveCard(
      merchantIdentifier,
    );

    promise.then((canMakePayments) => {
      if (canMakePayments) {
        console.log('Apple Play is supported');
        if (callback) callback(true);
      } else {
        console.log('Unable to pay using Apple Pay');
        if (callback) callback(false);
      }
    });
  } else {
    console.log('Unable to pay using Apple Pay');
    if (callback) callback(false);
  }
};

export default checkAvailabilityApplePay;
