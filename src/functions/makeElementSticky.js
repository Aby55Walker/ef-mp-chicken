import React, { useEffect, useState } from 'react'

export const useStickyElem = (elem, minWindowWidth) => {
  const [stickyClass, setStickyClass] = useState('')

  useEffect(() => {
    if (!elem) return

    if (window.innerWidth < minWindowWidth) return

    let elemTopOffset = elem.offsetTop

    const scrollHandler = () => {
      let pageTop = window.pageYOffset

      if (pageTop > elemTopOffset) {
        if (!stickyClass) {
          setStickyClass('sticky')
        }
      } else {
        setStickyClass('')
      }
    }

    scrollHandler()

    window.addEventListener('scroll', scrollHandler)
    return () => window.removeEventListener('scroll', scrollHandler)
  }, [elem])

  return stickyClass
}
