// getFormattedPrice

export default (price) => {
  if (!price) return null;

  return Math.round(parseFloat(price))
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};
