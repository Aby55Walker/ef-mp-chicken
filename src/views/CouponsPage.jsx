import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { CouponCard } from '../components/CouponCard/CouponCard';
import { OrderType } from '../components/OrderType/OrderType';
import { Btn } from '../components/Btn/Btn';
import { NotAuth } from '../components/NotAuth/NotAuth';
import { useCouponModal } from '../constants/pagelinks';
import { couponsRequest } from '../store/modules/coupons/actions';

export const CouponsPage = () => {
  const { loggedIn } = useSelector((state) => state.auth);
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const { type } = useSelector((state) => state.orderType);
  const coupons = useSelector((state) => state.coupons.data);

  useEffect(() => {
    // получить список купонов
    if (loggedIn) dispatch(couponsRequest(type));
  }, [type]);

  return (
    <div className="container coupons-page">
      <div className="coupons-page__head">
        <h1 className="page-title coupons-page__page-title">Купоны</h1>
        {loggedIn && (
          <OrderType
            classes="coupons-page__order-type"
            deliveryClasses="coupons-page__order-type-btn"
            pickupClasses="coupons-page__order-type-btn"
            Tag="button"
          />
        )}
      </div>

      {
        // если авторизован, то выводятся купоны
        loggedIn ? (
          <>
            <div className="grid coupons-page__grid">
              {coupons.map((data, index) => (
                <div className="grid__item" key={index}>
                  <CouponCard data={data} />
                </div>
              ))}
            </div>

            <Btn
              text="Применить свой купон"
              outlined
              centred
              tag="Link"
              to={`${pathname}${useCouponModal}`}
              classes="coupons-page__use-coupon"
            />
          </>
        ) : (
          <NotAuth classes="coupons-page__not-auth" />
        )
      }
    </div>
  );
};
