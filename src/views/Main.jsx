import React from 'react'
import { MainPageHeader } from '../components/MainPageHeader/MainPageHeader'
import { NavCardList } from '../components/NavCardList/NavCardList'
import { ScreenStickersContainer } from '../components/ScreenSticker/ScreenStickersContainer'

export const Main = () => (
  <div className='container'>
    <MainPageHeader />

    <NavCardList />

    <ScreenStickersContainer img='sticker-1@2x.png' />
  </div>
)
