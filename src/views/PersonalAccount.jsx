import React, { useEffect } from 'react'
import { PersonalAccountTabs } from '../components/PersonalAccountTabs/PersonalAccountTabs'
import { userGetData } from '../store/modules/user/actionCreators'
import { useDispatch } from 'react-redux'

export const PersonalAccount = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(userGetData())
  }, [])

  return (
    <div className='container'>
      <PersonalAccountTabs />
    </div>
  )
}
