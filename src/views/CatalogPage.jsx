import React, { createRef, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCatalog } from '../store/modules/catalog/actions';

import { Cart } from '../modules/Cart/Cart';
import { useFullWidthLayout } from '../hooks/useFullWidthLayout';
import { ProductCategoryListContainer } from '../containers/catalog/ProductCategoryListContainer';
import { CatalogNav } from '../components/CatalogNav/CatalogNav';
import { initIntersectionObserver } from '../functions/initIntersectionObserver';
import { PromotionsContainer } from '../containers/PromotionsContainer';
import { lg } from '../constants/breakpoints';
import { CartBtn } from '../components/CartBtn/CartBtn';
import { CartContainer } from '../containers/cart/CartContainer';

export const CatalogPage = () => {
  const dispatch = useDispatch();
  const { isOrderTypeDelivery } = useSelector((state) => state.orderType);
  const [activeItem, setActiveItem] = useState('');
  const [refs, setRefs] = useState([]);
  const [nowScrollAnimating, setNowScrollAnimating] = useState(false);
  const categories = useSelector((state) => state.catalog.data?.categories);
  const { screenWidth, screenHeight } = useSelector((state) => state.screenSize);
  // создаёт список ссылок на элементы для категорий (нужно для IntersectionObserver)
  useEffect(() => {
    if (categories?.length) {
      setRefs(categories.map(() => createRef()));
    }
  }, [categories]);

  // определяет активную категорию, которая сейчас на экране и возвращает её название
  useEffect(() => {
    if (!refs.length) return;
    initIntersectionObserver(refs, setActiveItem, nowScrollAnimating);
  }, [refs, screenHeight]);

  // получение каталога из API
  useEffect(() => {
    dispatch(fetchCatalog());
  }, [dispatch, isOrderTypeDelivery]);

  // дулает ширину футера и хедера на весь экран
  useFullWidthLayout();

  return (
    <div className="catalog-page">
      <div className="container catalog-page__container">
        <h1 className="page-title">
          {isOrderTypeDelivery ? 'Привезите мне' : 'Заберу сам'}
        </h1>
      </div>

      <div className="container catalog-page__container">
        <div className="catalog-page__content">
          <CatalogNav
            data={categories}
            activeItem={activeItem}
            onScrollStart={() => setNowScrollAnimating(true)}
            onScrollStop={() => setTimeout(() => setNowScrollAnimating(false), 100)}
          />
          <PromotionsContainer classes="catalog-page__promotions" />
          <ProductCategoryListContainer data={categories} refs={refs} />
        </div>

        {screenWidth < lg && <CartBtn />}
        {screenWidth >= lg && <CartContainer classes="catalog-page__cart" />}
      </div>
    </div>
  );
};
