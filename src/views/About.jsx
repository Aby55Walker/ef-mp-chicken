import React from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as ArrowIcon } from '../assets/icons/arrow-filled.svg'

export const About = () => (
  <>
    <h2>About</h2>
    <ArrowIcon />
    <Link to='/'>Main</Link>
  </>
)
