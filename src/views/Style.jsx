import React from 'react';
import { useLocation } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { Btn } from '../components/Btn/Btn';
import { Counter } from '../components/Counter/Counter';

import { showSlideModal, stopModalContent } from '../store/actions';

import { Checkbox } from '../components/Checkbox/Checkbox';
import { Input } from '../components/Input/Input';
import { Spoiler } from '../components/Spoiler/Spoiler';
import { Statuses } from '../components/Statuses/Statuses';

import { Help } from '../components/Help/Help';
import { ReactComponent as IconCalendar } from '../assets/icons/calendar.svg';
import { ReactComponent as MessageIcon } from '../assets/icons/message.svg';

import { Profile } from '../modules/Profile/Profile';

import { PromotionsContainer } from '../containers/PromotionsContainer';
import { Dropdown } from '../components/Dropdown/Dropdown';
import { MasonryGrid } from '../components/MasonryGrid/MasonryGrid';
import { CouponsSlider } from '../components/Sliders/CouponsSlider/CouponsSlider';
import { modalCart, modalCoupon, useCouponModal } from '../constants/pagelinks';
import { couponId } from '../constants/urlParams';
import { AddCoupon } from '../components/AddCoupon/AddCoupon';
import { CartCouponsContainer } from '../containers/cart/CartCouponsContainer';

export const Style = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const handleCheckbox = (value) => {
    console.log('Main handleCheckbox (для примера) =>', value);
  };

  const handleInput = (value) => {
    console.log('Main handleInput (для примера) =>', value);
  };

  const openStopModal = () => {
  };

  // counter
  const handleIncrement = (amount) => {
    console.log(amount);
  };
  const handleDecrement = (amount) => {
    console.log(amount);
  };

  function dropDownChangeHandler(value) {
    console.log('Dropdown value: ', value);
  }

  return (
    <div className="container">
      <Btn
        text="открыть модалку добавления купона"
        primary
        tag="Link"
        to={{
          pathname: `${location.pathname}${useCouponModal}`,
          state: { modalIsOpen: true },
        }}
      />
      <CartCouponsContainer />
      <Btn
        text="открыть модалку купоны"
        primary
        tag="Link"
        to={{
          pathname: `${location.pathname}${modalCoupon}`,
          search: `${couponId}=1`,
          state: { modalIsOpen: true },
        }}
      />
      <PromotionsContainer />
      <Btn
        text="Открыть модалку корзины"
        primary
        tag="Link"
        to={`${location.pathname}${modalCart}`}
      />

      <Btn
        to={{
          pathname: `${location.pathname}/side-modal/product`,
          search: 'productId=3',
          state: { modalIsOpen: true },
        }}
        style={{ marginRight: '10px', marginBottom: '10px' }}
        text="Открыть модалку продукта"
        primary
        classes="tmp"
        tag="Link"
      />

      <CouponsSlider />

      <MasonryGrid
        classes="masonry-example"
        gridItemClasses="masonry-example__item"
      >
        <div>
          <h1>Неровная сетка, такой цвет для примера</h1>
          <h1>item</h1>
        </div>
        <div>
          <h1>item</h1>
        </div>
        <div>
          <h1>item</h1>
          <h1>item</h1>
          <h1>item</h1>
          <h1>item</h1>
        </div>
        <div>
          <h1>item</h1>
          <h1>item</h1>
          <h1>item</h1>
          <h1>item</h1>
        </div>
        <div>
          <h1>item</h1>
          <h1>item</h1>
        </div>
      </MasonryGrid>

      <Btn
        tag="Link"
        to="/pages/contacts"
        text='Открыть страницу "Контакты"'
        primary
      />
      <Btn
        tag="Link"
        to={`${location.pathname}/modal/choose-rest`}
        text='Открыть модалку "Выбор ресторана"'
        primary
      />

      <Dropdown label="label" onChange={dropDownChangeHandler} />
      <Dropdown label="label" onChange={dropDownChangeHandler} initialValue />

      <Counter onDecrement={handleDecrement} onIncrement={handleIncrement} />

      <div>
        <Btn
          to={{
            pathname: `${location.pathname}/side-modal/product`,
            search: 'productId=1',
            state: { modalIsOpen: true },
          }}
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="Открыть модалку продукта"
          primary
          classes="tmp"
          tag="Link"
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          tag="a"
          text="action"
          primary
          loading
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          primary
          disabled
        />
      </div>
      <div>
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          leftIcon={<MessageIcon />}
          text="action"
          outlined
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          rightIcon={<MessageIcon />}
          text="action"
          outlined
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          outlined
          loading
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          outlined
          disabled
        />
      </div>
      <div>
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          transparent
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          transparent
          loading
        />
        <Btn
          style={{ marginRight: '10px', marginBottom: '10px' }}
          text="action"
          transparent
          disabled
        />
      </div>

      <Btn
        to={{
          pathname: `${location.pathname}/stop-modal`,
          state: { modalIsOpen: true },
        }}
        style={{ marginRight: '10px', marginBottom: '10px' }}
        text="Открыть StopModal"
        primary
        tag="Link"
        onClick={openStopModal}
      />
      <Btn
        to={{
          pathname: `${location.pathname}/side-modal/registration`,
          state: { modalIsOpen: true },
        }}
        style={{ marginRight: '10px', marginBottom: '10px' }}
        text="открыть SideModal"
        primary
        tag="Link"
      />

      <Btn
        to={{
          pathname: '/delivery/1',
        }}
        style={{ marginRight: '10px', marginBottom: '10px' }}
        text="Перейти на страницу Доставки"
        primary
        tag="Link"
      />

      <Profile />

      {/* <OrderList array={FakeData.data} /> */}

      <h2>Элементы</h2>

      <Checkbox
        text={
          'Даю согласие на&nbsp;получение СМС и&nbsp;e&#8209;mail уведомлений'
        }
      />
      <Checkbox disabled text="Disabled" />
      <Checkbox disabled checked text="Disabled && Checked" />
      <Checkbox checked text="Checked" />
      <Checkbox onChange={handleCheckbox} text="onChange (см. консоль)" />

      <Input value="текст" label="label" />
      <Input disabled value="disabled" label="disabled" />
      <Input
        value="Usertext"
        label="Label"
        error
        errorText="Error&nbsp;Description"
        onChange={handleInput}
        onBlur={handleInput}
      />
      <Input
        placeholder="placeholder"
        mask="date"
        label="label"
        helperText="helperText"
        icon={<IconCalendar />}
      />

      <Spoiler title="Уведомления">
        <Checkbox
          text={
            'Даю согласие на&nbsp;получение СМС и&nbsp;e&#8209;mail уведомлений'
          }
        />
      </Spoiler>

      <Spoiler title="Личные данные" open>
        <Checkbox
          text={
            'Даю согласие на&nbsp;получение СМС и&nbsp;e&#8209;mail уведомлений'
          }
        />
      </Spoiler>

      <Help text="У меня есть вопрос" to={`${location.pathname}/side-modal/help`} />
      <Help type={2} text="Хотите отменить заказ?" to={`${location.pathname}/side-modal/help`} />
      <Help
        type={3}
        text="У меня есть вопрос"
        list={[
          { to: '/', text: 'Как изменить дату рождения?' },
          { to: '/', text: 'Как привязать аккаунт к новому телефону?' },
          { to: '/', text: 'Могу ли я отписаться от уведомлений на e-mail?' },
        ]}
      />

      <Statuses
        data={[
          { status: 'done', id: 1 },
          { status: 'process', id: 2 },
          { status: 'future', id: 3 },
          { status: 'cancel', id: 4 },
        ]}
      />
    </div>
  );
};
