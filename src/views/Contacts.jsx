import React, { useEffect, useState } from 'react'
import api from '../api/api'
import { Contact } from '../components/Contact/Contact'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchStaticPage,
  resetStaticPage,
} from '../store/modules/staticPages/actions'
import { InfoBlock } from '../components/Contact/includes/InfoBlock/InfoBlock'
import { Socials } from '../components/Socials/Socials'

export const ContactsPage = () => {
  const dispatch = useDispatch()
  const { data } = useSelector((state) => state.staticPage)

  const { title, other = {} } = data

  useEffect(() => {
    dispatch(fetchStaticPage(api.getContactsPage))
    return () => dispatch(resetStaticPage())
  }, [])

  return (
    <div className='container contacts'>
      <h1 className={'page-title contacts__title'}>{title}</h1>

      <div className='grid contacts__grid'>
        <div className='grid__item'>
          <InfoBlock
            buildRoad
            title={other.about?.title}
            text={other.about?.addresses[0].address}
          >
            <Socials style={{ marginTop: '20px' }} data={other.about} />
          </InfoBlock>
        </div>

        <div className='grid__item'>
          <InfoBlock title={'Как проехать'} text={other.about?.road} />
        </div>

        <div className='grid__item'>
          <InfoBlock
            title={'Режим работы  '}
            text={other.about?.operating_mode}
          />
        </div>

        <hr />

        <div className='grid__item'>
          <Contact
            title={'Отзывы и предложения'}
            data={other.comments_and_suggestions}
          />
        </div>

        <hr />

        <div className='grid__item'>
          <Contact title={'Пообщаться с главным'} data={other.talk_main} />
        </div>

        <hr />

        <div className='grid__item'>
          <Contact title={'Работа в Chikenbox'} data={other.work} />
        </div>

        <hr />

        <div className='grid__item'>
          <Contact
            classes={'contact_secondary'}
            title={'Секретарь'}
            data={other.secretary}
          />
        </div>

        <div className='grid__item'>
          <Contact
            classes={'contact_secondary'}
            title={'Сетевые проекты'}
            data={other.network_projects}
          />
        </div>

        <div className='grid__item'>
          <Contact
            classes={'contact_secondary'}
            title={'Мультиформатные проекты'}
            data={other.multiformat_projects}
          />
        </div>
      </div>
    </div>
  )
}
