// PromotionsPage
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { OrderType } from '../components/OrderType/OrderType'
import { PromotionCard } from '../components/PromotionCard/PromotionCard'
import { promotionsRequest } from '../store/modules/promotions/actions'
import PromotionsFakeData from '../components/Sliders/PromotionsSlider/PromotionFakeData.json'

export const PromotionsPage = () => {
  const dispatch = useDispatch()
  const { data } = useSelector((state) => state.promotions)
  // const data = PromotionsFakeData.data

  useEffect(() => {
    dispatch(promotionsRequest())
  }, [dispatch])

  return (
    <div className='container promotions-page'>
      <div className='promotions-page__head'>
        <h1 className='page-title promotions-page__page-title'>Акции</h1>
        <OrderType
          classes='promotions-page__order-type'
          deliveryClasses='promotions-page__order-type-btn'
          pickupClasses='promotions-page__order-type-btn'
          Tag='button'
        />
      </div>

      <div className='grid promotions-page__grid'>
        {data && data.length ? (
          <>
            {data.map((content, i) => (
              <div className='grid__item' key={i}>
                <PromotionCard
                  index={i}
                  data={content}
                  classes='promotions-page__card'
                />
              </div>
            ))}
          </>
        ) : null}
      </div>
    </div>
  )
}
