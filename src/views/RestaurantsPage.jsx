import React from 'react'
import { RestaurantsContainer } from '../containers/RestaurantsContainer'

export const RestaurantsPage = () => (
  <div className='container'>
    <RestaurantsContainer />
  </div>
)
