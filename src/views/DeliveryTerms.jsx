import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DeliveryTermsContent } from '../components/DeliveryTermsContent/DeliveryTermsContent';
import { Map } from '../components/Map/Map';
import { lg } from '../constants/breakpoints';
import { fetchStaticPage } from '../store/modules/staticPages/actions';
import api from '../api/api';
import { restaurantsRequest } from '../store/modules/Restaurants/actions';

export const DeliveryTerms = () => {
  const { screenWidth } = useSelector((state) => state.screenSize);

  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.staticPage);
  const restaurantsData = useSelector((state) => state.restaurants.data);
  const [polygon, setPolygon] = useState([]);
  const [points, setPoints] = useState([]);
  const [restsInfo, setRestsInfo] = useState([]);

  useEffect(() => {
    document.body.style.overflowX = 'visible';
    return () => { document.body.style.overflowX = ''; };
  }, []);

  // получение данных для страницы "условия доставки" и списка ресторанов
  useEffect(() => {
    dispatch(fetchStaticPage(api.fetchDeliveryTerms));
    dispatch(restaurantsRequest());
  }, [dispatch]);

  // собирает массив для полигона на карте
  useEffect(() => {
    if (data.other === undefined) return;
    setPolygon(data.other.delivery_area.map((item) => ({
      lat: item[0],
      lng: item[1],
    })));
  }, [data]);

  // собирает массив точек на карте
  useEffect(() => {
    // все рестораны с координатами
    const restsWithCoords = restaurantsData.filter(({ restaurant_coords: coords }) => coords);
    // маркеры для карты
    setPoints(restsWithCoords.map(({ restaurant_coords: coords }) => ({
      lat: +coords[0],
      lng: +coords[1],
    })));
    // адреса и названия ресторанов
    setRestsInfo(restsWithCoords.map(({ name, address, id }) => ({ name, address, id })));
  }, [restaurantsData]);

  return (
    <div className="container delivery-terms">
      <DeliveryTermsContent points={points} restsData={restsInfo} data={data} />
      {screenWidth <= lg && (
        <h3 className="delivery-terms__zones-title">Зоны доставки</h3>
      )}
      <Map polygons={polygon} points={points} hideUI={screenWidth <= lg} classes="delivery-terms__map-wp" />
    </div>
  );
};
