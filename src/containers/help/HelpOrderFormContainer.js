import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { sendProblemsRequest } from '../../store/modules/problems/actions';
import { HelpOrderForm } from '../../components/HelpModal/includes/HelpOrderForm';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';
import { orderProblemIndex } from '../../constants/problems';
import { modalHelp } from '../../constants/pagelinks';

export const HelpOrderFormContainer = (props) => {
  const {
    ...other
  } = props;
  const history = useHistory();
  const dispatch = useDispatch();
  const match = useRouteMatch(`*${modalHelp}`);
  const problems = useSelector((state) => state.problems);

  const onSubmit = (data) => {
    const {
      comment,
      firstName,
      phone,
    } = data;

    const request = {
      problem_name: problems.problemName,
      type: problems.problemType,
      report: `${comment || ''}`,
      name: firstName && `${firstName}`,
      phone: phone && `${makeUnmaskedPhone(phone)}`,
    };

    dispatch(sendProblemsRequest(
      request,
      history,
      `${match.url.split('side-modal/')[0]}side-modal/help-success`,
    ));
  };

  return (
    <HelpOrderForm onSubmit={onSubmit} {...other} />
  );
};
