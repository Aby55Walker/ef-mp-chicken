import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import {
  problemsRequest,
  setProblemName,
} from '../../store/modules/problems/actions';
import { HelpProblems } from '../../components/HelpModal/includes/HelpProblems';
import { siteProblemIndex } from '../../constants/problems';
import { modalHelp } from '../../constants/pagelinks';

export const HelpSiteProblemsContainer = (props) => {
  const {
    ...other
  } = props;
  const dispatch = useDispatch();
  const match = useRouteMatch(`*${modalHelp}`);
  const data = useSelector((state) => state.problems.data);

  useEffect(() => {
    // получить список проблем
    if (!data.length) dispatch(problemsRequest());
  }, []);

  const clickHandler = (problemName) => {
    dispatch(setProblemName(problemName));
  };

  return (
    <>
      {data && data.length && data[siteProblemIndex]
        ? (
          <HelpProblems
            {...other}
            title={data[siteProblemIndex].name}
            onClick={clickHandler}
            data={data[siteProblemIndex].options}
            path={`${match.url}/order-form`}
          />
        )
        : null}
    </>
  );
};
