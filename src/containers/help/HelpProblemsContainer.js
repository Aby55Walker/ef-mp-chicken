import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { HelpProblems } from '../../components/HelpModal/includes/HelpProblems';
import { lg } from '../../constants/breakpoints';
import deleteSlashAtTheEnd from '../../functions/deleteSlashAtTheEnd';
import { problemsRequest, setProblemType } from '../../store/modules/problems/actions';
import { modalHelp } from '../../constants/pagelinks';

export const HelpProblemsContainer = (props) => {
  const {
    path,
    ...other
  } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(problemsRequest());
  }, []);

  const location = useLocation();
  const { screenWidth } = useSelector((state) => state.screenSize);
  const { data } = useSelector((state) => state.problems);
  const nextPath = `${deleteSlashAtTheEnd(location.pathname.split('side-modal/')[0])}${modalHelp}`;

  const links = [
    `${nextPath}/order-questions`,
    `${nextPath}/site-questions`,
  ];

  const expandedList = data.map((parameters, i) => (
    { ...parameters, to: links[i] }
  ));

  const onClick = (data) => {
    dispatch(setProblemType(data.type));
  };

  return (
    <>
      {data.length
        ? (
          <HelpProblems
            {...other}
            title={screenWidth < lg ? 'Какой у&nbsp;вас вопрос?' : 'Какой у&nbsp;вас<br> Вопрос?'}
            data={expandedList}
            onClick={onClick}
          />
        )
        : null}
    </>
  );
};
