import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ChooseRest } from '../modules/ChooseRest/ChooseRest';
import { restaurantsRequest } from '../store/modules/Restaurants/actions';

export const ChooseRestContainer = () => {
  const { data = [] } = useSelector((state) => state.restaurants);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(restaurantsRequest());
  }, [dispatch]);

  return data.length ? <ChooseRest data={data} /> : null;
};
