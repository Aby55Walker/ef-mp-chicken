import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CartItem } from '../../components/CartItem/CartItem';
import { cartUpdateItem } from '../../store/modules/cart/cartContent/actions';

export const CartItemContainer = ({ data }) => {
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.cart.content);

  const changeCountHandler = (quantity) => {
    const transformedModifiers = {};
    if (data.modifiers?.length) {
      data.modifiers.forEach((item) => {
        transformedModifiers[item.id] = item.quantity;
      });
    }

    let combos = [];
    if (data.combo?.length) {
      combos = data.combo.map((combo) => {
        let transformedComboComments = [];
        const transformedComboModifiers = {};

        if (combo.comments?.length) {
          transformedComboComments = combo.comments.map((c) => c.id);
        }

        if (combo.modifiers?.length) {
          combo.modifiers.forEach((item) => {
            transformedComboModifiers[item.id] = item.quantity;
          });
        }
        return {
          id: combo.id,
          modifiers: transformedComboModifiers,
          comments: transformedComboComments,
        };
      });
    }

    dispatch(cartUpdateItem({
      quantity,
      modifiers: transformedModifiers,
      comments: data?.comments?.map((x) => x.id),
      itemId: data.id,
      rowId: data.row_id,
      comboItems: combos,
    }));
  };

  return (
    <CartItem loading={loading} onCountChange={changeCountHandler} data={data} />
  );
};
