// CartContainer
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Cart } from '../../modules/Cart/Cart';
import {
  cartContentRequest,
} from '../../store/modules/cart/cartContent/actions';
import { cartReset } from '../../store/modules/cart/cartMethods/actions';

export const CartContainer = (props) => {
  const {
    ...other
  } = props;
  const dispatch = useDispatch();

  const { data } = useSelector((state) => state.cart?.content);
  const addItemData = useSelector((state) => state.cart?.addItem?.data);
  const { loggedIn } = useSelector((state) => state.auth);
  const { cartId } = useSelector((state) => state.cart?.addItem);

  useEffect(() => {
    if (typeof cartId !== 'undefined') {
      dispatch(cartContentRequest({ cartId }));
    }
  }, [dispatch, addItemData, cartId]);

  useEffect(() => {
    if (cartId) dispatch(cartContentRequest({ cartId }));
  }, []);

  useEffect(() => {
    // dispatch(cartReset());
  }, [loggedIn]);

  return <Cart {...other} data={data} />;
};
