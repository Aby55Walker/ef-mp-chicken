import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ProductPrice } from '../../components/Product/includes/ProductPrice';
import { addItemToCartRequest } from '../../store/modules/cart/cartItem/actions';
import { catalogPage } from '../../constants/pagelinks';

export const AddItemToCartContainer = (props) => {
  const {
    comments,
    allAdditives,
    price,
    hidden,
    disabled,
    id,
  } = props;
  const dispatch = useDispatch();
  const orderType = useSelector((state) => state.orderType.type);
  const cartId = useSelector((state) => state.cart.addItem.cartId);
  const history = useHistory();
  const { loading } = useSelector((state) => state.cart.addItem);
  const innerProducts = useSelector((state) => state.innerProduct.state);
  const [innerProductsPrice, setInnerProductsPrice] = useState(0);
  const clickHandler = ({ id, count }) => {
    const transformedModifiers = {};
    Object.keys(allAdditives).forEach((key) => {
      transformedModifiers[key] = allAdditives[key].val;
    });

    const comboItems = [];
    if (Object.keys(innerProducts).length) {
      // продукты для комбо
      Object.keys(innerProducts).forEach((key) => {
        const { modifiers } = innerProducts[key];
        const { comments } = innerProducts[key];

        const transformedModifiers = {};
        Object.keys(modifiers.list).forEach((mKey) => {
          transformedModifiers[mKey] = modifiers.list[mKey].val;
        });

        comboItems.push({
          id: key,
          modifiers: transformedModifiers,
          comments: comments?.selected,
        });
      });
    }

    dispatch(addItemToCartRequest({
      menu_item_id: id,
      type: orderType,
      quantity: count,
      modifiers: transformedModifiers,
      comments,
      cartId: cartId || '',
      combo_items: comboItems,
    },
    history,
    catalogPage));
  };

  useEffect(() => {
    // суммирует цену всех внутренних продуктов
    setInnerProductsPrice(Object.values(innerProducts).reduce((acc, curr) => acc + Object.values(curr).reduce((innerAcc, innerCurr) => {
      if (innerCurr.price) return innerCurr.price + innerAcc;
      return innerAcc + 0;
    }, 0), 0));
  }, [innerProducts]);

  useEffect(() => {

  }, [innerProductsPrice]);

  return hidden
    ? null
    : (
      <ProductPrice
        loading={loading}
        onClick={clickHandler}
        disabled={disabled}
        price={price + innerProductsPrice}
        id={id}
      />
    );
};
