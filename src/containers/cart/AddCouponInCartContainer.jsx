import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddCoupon } from '../../components/AddCoupon/AddCoupon';
import { cartCouponSetStatus } from '../../store/modules/cart/addCoupon/actions';
import api from '../../api/api';
import formatError from '../../functions/formatResponseError';

export const AddCouponInCartContainer = ({ index, code, done }) => {
  const dispatch = useDispatch();
  const [error, setError] = useState('');
  const loading = useSelector((state) => state.cart.addCoupon.couponList[index]?.loading);
  const orderType = useSelector((state) => state.orderType.type);

  const submitHandler = ({ coupon }) => {
    dispatch(cartCouponSetStatus({ index, loading: true, done: false }));
    api.addCouponToCart({
      coupon_code: +coupon,
      type: orderType,
      quantity: 1,
      coupon_items: [],
    })
      .then(() => {
        dispatch(cartCouponSetStatus({
          index, loading: false, done: true, code: coupon,
        }));
        setError('');
      })
      .catch((e) => {
        setError(formatError(e));
        dispatch(cartCouponSetStatus({ index, loading: false, done: false }));
      });
  };

  const inputHandler = (val) => {
    setError('');
  };

  const resetHandler = () => dispatch(cartCouponSetStatus({
    index, loading: false, done: false, code: '',
  }));

  return (
    <AddCoupon onReset={resetHandler} onInput={inputHandler} code={code} requestError={error} loading={loading} done={done} onSubmit={submitHandler} />
  );
};
