import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddCouponInCartContainer } from './AddCouponInCartContainer';
import { cartCouponCreate } from '../../store/modules/cart/addCoupon/actions';

export const CartCouponsContainer = () => {
  const { couponList } = useSelector((state) => state.cart.addCoupon);
  const dispatch = useDispatch();

  return (
    <div className="add-coupon__container">
      {
        couponList.map(({ index, done, code }) => (
          <React.Fragment key={index}>
            <AddCouponInCartContainer done={done} code={code} index={index} />
          </React.Fragment>
        ))
      }
      { couponList[couponList.length - 1].done
        && <button className="add-coupon__create-btn" type="button" onClick={() => dispatch(cartCouponCreate())}>У меня есть еще купон</button>}
    </div>
  );
};
