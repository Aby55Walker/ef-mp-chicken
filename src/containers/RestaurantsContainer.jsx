import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Restaurants } from '../modules/Restaurants/Restaurants';
import { restaurantsRequest } from '../store/modules/Restaurants/actions';

export const RestaurantsContainer = () => {
  const dispatch = useDispatch();

  const { data, loading } = useSelector((state) => state.restaurants);

  useEffect(() => {
    dispatch(restaurantsRequest());
  }, [dispatch]);

  return <Restaurants data={data} loading={loading} />;
};
