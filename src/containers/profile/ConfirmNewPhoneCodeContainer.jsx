import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { CodeIntroduction } from '../../components/CodeIntroduction/CodeIntroduction';
import { checkCodeRequest, setCode } from '../../store/modules/auth/actions';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';
import { userUpdateData } from '../../store/modules/user/actionCreators';

export const ConfirmNewPhoneCodeContainer = (props) => {
  const { onCodeSuccessLink, matchUrl } = props;

  const { newPhone } = useSelector((state) => state.auth);
  const history = useHistory();
  const dispatch = useDispatch();

  const submitHandler = (enteredCode) => {
    dispatch(setCode(enteredCode));
    dispatch(
      userUpdateData(
        {
          ...data,
          phone: makeUnmaskedPhone(newPhone),
          enteredCode,
        },
        history,
      ),
    );
  };

  return (
    <CodeIntroduction
      changePhoneBtn={false}
      onCodeSuccessLink={onCodeSuccessLink}
      matchUrl={matchUrl}
      onSubmit={submitHandler}
    />
  );
};
