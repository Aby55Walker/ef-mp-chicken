import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { CodeIntroduction } from '../../components/CodeIntroduction/CodeIntroduction';
import { checkCodeRequest, setCode } from '../../store/modules/auth/actions';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';

export const ChangePhoneCodeIntroductionContainer = (props) => {
  const { onCodeSuccessLink, matchUrl, onSubmit } = props;

  const { phone } = useSelector((state) => state.auth);
  const history = useHistory();
  const dispatch = useDispatch();

  const submitHandler = (enteredCode) => {
    if (onSubmit) {
      onSubmit(enteredCode);
      return;
    }
    dispatch(setCode(enteredCode));
    dispatch(
      checkCodeRequest(
        makeUnmaskedPhone(phone),
        enteredCode,
        history,
        `${matchUrl}/new-phone`,
      ),
    );
  };

  return (
    <CodeIntroduction
      changePhoneBtn={false}
      onCodeSuccessLink={onCodeSuccessLink}
      matchUrl={matchUrl}
      onSubmit={submitHandler}
    />
  );
};
