import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AddAddress } from '../../components/AddAddress/AddAddress';
import {
  userAddAddress,
  userUpdateAddress,
} from '../../store/modules/user/actionCreators';
import { toCamelCase } from '../../functions/toCamelCase';
import { allAddressFields } from '../../components/EditAddress/EditAddressFormContainer';

export const AddAddressContainer = () => {
  const id = useSelector((state) => state.user.data?.id);
  const dispatch = useDispatch();
  const history = useHistory();

  const submitHandler = (data) => {
    const transformedData = {};

    allAddressFields.forEach((item) => {
      if (data[toCamelCase(item)]) {
        transformedData[item] = data[toCamelCase(item)];
      }
    });

    dispatch(userAddAddress(transformedData, history));
  };

  return <AddAddress triggerOnMount={false} onSubmit={submitHandler} />;
};
