/* eslint-disable camelcase */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Profile } from '../../modules/Profile/Profile';
import {
  userUpdateData,
} from '../../store/modules/user/actionCreators';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';

export const ProfileContainer = ({ ...props }) => {
  const dispatch = useDispatch();

  const userData = useSelector((state) => state.user.data);
  const { code } = useSelector((state) => state.auth);
  const { is_accept_notifications } = useSelector((state) => state.user.data);

  const submitHandler = (data) => {
    const {
      phone, birthday, firstName, lastName, email,
    } = data;

    const transformedData = {
      phone: makeUnmaskedPhone(phone),
      birthday,
      first_name: firstName,
      last_name: lastName,
      email,
      code: code.toString(),
      is_accept_notifications,
    };

    dispatch(userUpdateData(transformedData));
  };

  return <Profile onSubmit={submitHandler} userData={userData} {...props} />;
};
