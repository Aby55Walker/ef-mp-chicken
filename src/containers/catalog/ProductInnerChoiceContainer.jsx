import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Product } from '../../components/Product/Product';
import { Btn } from '../../components/Btn/Btn';
import {
  innerProductRequest, innerProductReset,
  innerProductSetAllChosen,
  innerProductSetComments,
  innerProductSetModifiers,
} from '../../store/modules/product/innerProduct/actions';
import { getQueryParam } from '../../functions/getQueryParams';
import { productId } from '../../constants/urlParams';
// import { productFakeData } from '../../components/Product/ProductFakeData';

export const ProductInnerChoiceContainer = (props) => {
  const {} = props;

  const {
    weight,
    price,
    name,
    comments,
    description,
    id,
    pictures: { detail } = {},
    modifiers,
  } = useSelector((state) => state.innerProduct.data);

  const history = useHistory();
  const param = getQueryParam(productId);
  const dispatch = useDispatch();
  const [innerProductState, setInnerProductState] = useState({});

  useEffect(() => {
    dispatch(innerProductRequest(param));
  }, [dispatch]);

  const commentsChangeHandler = (selected, price, id) => {
    setInnerProductState((prevState) => ({
      ...prevState,
      comments: { selected, price, id },
    }));
  };

  const allChosenHandler = (val, id) => {
    setInnerProductState((prevState) => ({
      ...prevState,
      allChosen: { val, id },
    }));
  };

  const additivesChangeHandler = (list, price, id) => {
    setInnerProductState((prevState) => ({
      ...prevState,
      modifiers: { list, price, id },
    }));
  };

  const clickHandler = () => {
    const { comments, allChosen, modifiers } = innerProductState;
    dispatch(innerProductSetComments(comments));
    dispatch(innerProductSetAllChosen(allChosen));
    dispatch(innerProductSetModifiers(modifiers));
    history.goBack();
  };
  // сбрасывает данные для внутреннего рподукта
  useEffect(() => () => dispatch(innerProductReset()), []);

  return (
    <Product
      weight={weight}
      price={price}
      name={name}
      description={description}
      id={id}
      internals={false}
      img={detail}
      modifiers={modifiers}
      comments={comments}
      onCommentsChange={commentsChangeHandler}
      onAllChosen={allChosenHandler}
      onAdditivesChange={additivesChangeHandler}
      renderActionBlock={(childProps) => (
        <div className="product-price product-price_dark">
          <Btn disabled={childProps.disabled} onClick={clickHandler} text="Применить" fullWidth centred primary />
        </div>
      )}
    />
  );
};
