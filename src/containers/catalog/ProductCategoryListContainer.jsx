import React from 'react';
import { ProductCategoryList } from '../../components/ProductCategoryList/ProductCategoryList';
import { catalogData } from '../../data/catalog';

export const ProductCategoryListContainer = React.forwardRef(
  ({ ...props }, ref) => <ProductCategoryList {...props} ref={ref} />,
);
