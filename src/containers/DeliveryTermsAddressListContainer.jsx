import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DeliveryTermsAddressList } from '../components/DeliveryTermsContent/includes/DeliveryTermsAddressList';
import { mapSetActiveMarker, mapSetCenter } from '../store/modules/map/actions';

export const DeliveryTermsAddressListContainer = (props) => {
  const { list = [], points, ...other } = props;

  const dispatch = useDispatch();
  const { map } = useSelector((state) => state.map);

  const clickHandler = (addressIndex) => {
    dispatch(mapSetCenter(map, points[addressIndex]));
    dispatch(mapSetActiveMarker(addressIndex));
  };

  return (
    <DeliveryTermsAddressList {...other} onClick={clickHandler} list={list} />
  );
};
