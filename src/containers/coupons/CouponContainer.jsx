import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cn from 'classnames';
import { Product } from '../../components/Product/Product';
// import { AddItemToCartContainer } from '../cart/AddItemToCartContainer';
import { couponFetch } from '../../store/modules/product/actions';
import { getQueryParam } from '../../functions/getQueryParams';
import { productId } from '../../constants/urlParams';

export const CouponContainer = (props) => {
  const { hidden } = props;
  const dispatch = useDispatch();
  const param = getQueryParam(productId);
  const { data } = useSelector((state) => state.product);
  const innerProduct = useSelector((state) => state.innerProduct.state);
  const { type } = useSelector((state) => state.orderType);

  useEffect(() => {
    if (!Object.keys(data).length) {
      dispatch(couponFetch({ code: param, type }));
    }
  }, []);

  const [commentsList, setCommentsList] = useState([]);

  const {
    weight,
    price,
    name,
    description,
    id,
    comments,
    pictures: { detail } = {},
    modifiers,
    combo = [],
  } = data;

  const commentsChangeHandler = (commentsIds) => {
    setCommentsList(commentsIds);
  };

  const blockClasses = cn('', { hidden });

  return (
    <Product
      innerProduct={innerProduct}
      onCommentsChange={commentsChangeHandler}
      weight={weight}
      price={price}
      name={name}
      description={description}
      id={id}
      combo={combo}
      img={detail}
      comments={comments}
      modifiers={modifiers}
      classes={blockClasses}
      // renderActionBlock={(childProps) => (
      //   <AddItemToCartContainer
      //     id={id}
      //     disabled={childProps.disabled}
      //     price={childProps.fullPrice}
      //     comments={commentsList}
      //     allAdditives={childProps.allAdditives}
      //     hidden={hidden}
      //   />
      // )}
    />
  );
};
