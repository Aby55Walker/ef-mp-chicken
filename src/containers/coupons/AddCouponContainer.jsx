import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { AddCoupon } from '../../components/AddCoupon/AddCoupon';
import { cartCouponSetStatus } from '../../store/modules/cart/addCoupon/actions';
import { Btn } from '../../components/Btn/Btn';

export const AddCouponContainer = ({ id, closeModal }) => {
  const dispatch = useDispatch();
  const [done, setDone] = useState(false);
  const loading = useSelector((state) => state.cart.addCoupon.couponList[id]?.loading);
  const { pathname } = useLocation();

  const submitHandler = (data) => {
    dispatch(cartCouponSetStatus({ id, loading: true }));
    setTimeout(() => {
      dispatch(cartCouponSetStatus({ id, loading: false }));
      setDone(true);
    }, 1000);
  };

  return (
    <>
      <AddCoupon
        wrapperClasses="use-coupon__input-wrapper"
        setDone={setDone}
        loading={loading}
        done={done}
        btnProps={{
          primary: !done,
          outlined: done,
        }}
        onSubmit={submitHandler}
      />
      { done
      && (
        <Btn
          primary
          classes="use-coupon__cart-btn"
          text="В корзину"
          onClick={closeModal}
          fullWidth
          centred
        />
      )}
    </>
  );
};
