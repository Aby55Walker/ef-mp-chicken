import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { PromotionsSlider } from '../components/Sliders/PromotionsSlider/PromotionsSlider'
import { promotionsRequest } from '../store/modules/promotions/actions'
import { lg } from '../constants/breakpoints'
import { PromotionList } from '../components/PromotionList/PromotionList'

export const PromotionsContainer = (props) => {
  const { ...other } = props
  const dispatch = useDispatch()
  const { data, loading } = useSelector((state) => state.promotions)
  const { screenWidth } = useSelector((state) => state.screenSize)

  useEffect(() => {
    dispatch(promotionsRequest())
  }, [dispatch])

  return (
    <>
      {screenWidth >= lg ? (
        <PromotionsSlider data={data} loading={loading} {...other} />
      ) : (
        <PromotionList data={data} loading={loading} {...other} />
      )}
    </>
  )
}
