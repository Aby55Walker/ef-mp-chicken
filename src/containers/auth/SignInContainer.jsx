import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { SignIn } from '../../components/SignIn/SignIn';
import { authSetPhone } from '../../store/modules/auth/actionCreators';
import { fetchCodeRequest } from '../../store/modules/auth/actions';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';
import { authPage } from '../../constants/pagelinks';

export const SignInContainer = (props) => {
  const {
    title = 'Вход или регистрация',
    path = authPage,
    ...other
  } = props;
  const { loading } = useSelector((state) => state.auth);
  const history = useHistory();
  const dispatch = useDispatch();
  const match = useRouteMatch(`*${path}`);

  const submitHandler = (phone) => {
    dispatch(
      fetchCodeRequest(
        {
          phone: makeUnmaskedPhone(phone),
        },
        history,
        `${match.url}/code-introduction`,
      ),
    );
    dispatch(authSetPhone(phone));
  };

  return (
    <SignIn
      {...other}
      title={title}
      loading={loading}
      onSubmit={submitHandler}
    />
  );
};
