import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { CodeIntroduction } from '../../components/CodeIntroduction/CodeIntroduction';
import {
  authRequest,
  checkCodeRequest,
  setCode,
} from '../../store/modules/auth/actions';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';
import { authPage } from '../../constants/pagelinks';

/**
 * В данном компоненте пользователь вводит код
 * Сценарии:
 *
 * 1. зарегистрирован
 * 1.2. записать код
 * 1.2. авторизовать по номеру телефона и коду
 *
 * 2. незарегистрирован
 * 2.1. записать код
 * 2.2. если goToCheckout
 *      - авторизовать по номеру телефона и коду
 *      - перейти на страницу оформления
 *      если !goToCheckout
 *      - проверить код
 *      - перейти к модалке регистрации
 */

export const AuthCodeIntroductionContainer = (props) => {
  const {
    path = authPage,
    registeredRelocation = '/personal-account/profile',
    unregisteredDispatch,
    goToCheckout, // перейти к формлению заказа в сценарии 2
    ...other
  } = props;

  const { registered, phone } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch(`*${path}`);

  const submitHandler = (enteredCode) => {
    // (+) если зарегестрирован, то на сервер отправляется номер и код для авторизации
    if (registered) {
      // (+) записать код
      dispatch(setCode(enteredCode));
      // (+) авторизовать по номеру телефона и коду
      dispatch(
        authRequest(
          makeUnmaskedPhone(phone),
          enteredCode,
          history,
          registeredRelocation,
        ),
      );

      // (-) если не зарегестрирован
    } else {
      // (-) записать код
      dispatch(setCode(enteredCode));

      // (-) если нужно перейти к оформлению заказа
      if (goToCheckout) {
        // (-) авторизовать по номеру телефона и коду
        dispatch(authRequest(makeUnmaskedPhone(phone), enteredCode));
        // (-) перейти на страницу оформления
        history.push('/delivery/1');
      // eslint-disable-next-line brace-style
      }
      // (-) иначе проверить код и перейти к модалке регистрации
      else {
        dispatch(
          checkCodeRequest(
            makeUnmaskedPhone(phone),
            enteredCode,
            history,
            `${match.url}/profile`,
          ),
        );
      }
    }
  };

  return <CodeIntroduction onSubmit={submitHandler} matchUrl={`*${path}`} {...other} />;
};
