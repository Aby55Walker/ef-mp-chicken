import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { RegistrationData } from '../../components/RegistrationData/RegistrationData';
import { register, registrationRequest } from '../../store/modules/auth/actions';
import { authRegSuccess } from '../../store/modules/auth/actionCreators';
import { getFromStorage } from '../../functions/localStorage';
import { makeUnmaskedPhone } from '../../functions/makeUnmaskedPhone';

export const RegistrationFormContainer = () => {
  const defaultValues = {
    phone: getFromStorage('state').auth.phone || '',
    firstName: '',
    lastName: '',
    birthday: '',
    email: '',
    isAcceptNotifications: true,
  };

  const history = useHistory();
  const dispatch = useDispatch();
  const { code } = useSelector((state) => state.auth);

  const submitHandler = (data) => {
    const regData = { ...data };
    regData.code = code.toString();
    regData.phone = makeUnmaskedPhone(regData.phone);
    dispatch(registrationRequest(regData, history, '/personal-account'));
  };

  return <RegistrationData values={defaultValues} onSubmit={submitHandler} />;
};
