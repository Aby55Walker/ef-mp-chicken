import React, { useEffect } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Layout } from './components/Layout/Layout';
import { LayoutDelivery } from './components/LayoutDelivery/LayoutDelivery';
import { Style } from './views/Style';
import { PersonalAccount } from './views/PersonalAccount';
import { ProductModal } from './components/ProductModal/ProductModal';
import { StopModal } from './components/StopModal/StopModal';
import { Registration } from './modules/Registration/Registration';
import { ShortRegistration } from './modules/ShortRegistration/ShortRegistration';
import { RestaurantsPage } from './views/RestaurantsPage';
import { Notification } from './components/Notification/Notification';
import { DeliveryTerms } from './views/DeliveryTerms';
import { Main } from './views/Main';
import { AddAddressContainer } from './containers/profile/AddAddressContainer';
import { EditAddressContainer } from './containers/profile/EditAddressContainer';
import { ChangePhone } from './modules/ChangePhone/ChangePhone';
import { showSlideModal } from './store/actions';
import { StoriesModal } from './components/StoriesModal/StoriesModal';
import { ContactsPage } from './views/Contacts';
import { Burger } from './components/Burger/Burger';
import { HelpModal } from './components/HelpModal/HelpModal';
import { CatalogPage } from './views/CatalogPage';
import { CouponsPage } from './views/CouponsPage';
import { PromotionsPage } from './views/PromotionsPage';
import {
  couponsPage,
  promotionsPage,
  catalogPage,
  deliveryTermPage,
  restaurantsPage,
  personalAccountPage,
  contactsPage,
  modalCart,
  modalHelp,
  modalHelpSuccess,
  useCouponModal,
  shortAuthPage,
  modalStop,
} from './constants/pagelinks';
import { CartModal } from './components/CartModal/CartModal';
import { ChooseRestContainer } from './containers/ChooseRestContainer';
import { CouponModal } from './components/CouponModal/CouponModal';
import { HelpSuccessModal } from './components/HelpSuccessModal/HelpSuccessModal';
import { UseCouponModal } from './components/UseCouponModal/UseCouponModal';

export const RouterSwitch = () => {
  const location = useLocation();
  const dispatch = useDispatch();

  // если url заканчивается на 'side-modal', то закрыть боковую модалку
  useEffect(() => {
    const { pathname } = location;
    const isOpenModal = pathname.endsWith('side-modal') || pathname.endsWith('side-modal/');
    if (isOpenModal) dispatch(showSlideModal(false));
  }, [location]);

  return (
    <>
      {/* Layouts */}
      <Switch>
        <Route path="/delivery">
          <LayoutDelivery path="/delivery" />
        </Route>

        <Route path="/">
          <Layout>
            <Switch>
              <Route path={restaurantsPage}>
                <RestaurantsPage />
              </Route>
              <Route path="/style">
                <Style />
              </Route>
              <Route path={personalAccountPage}>
                <PersonalAccount />
              </Route>
              <Route path={contactsPage}>
                <ContactsPage />
              </Route>
              <Route path={deliveryTermPage}>
                <DeliveryTerms />
              </Route>
              <Route path={catalogPage}>
                <CatalogPage />
              </Route>
              <Route path={couponsPage}>
                <CouponsPage />
              </Route>
              <Route path={promotionsPage}>
                <PromotionsPage />
              </Route>
              <Route path="/">
                <Main />
              </Route>
            </Switch>
          </Layout>
        </Route>
      </Switch>

      {/* Modals */}
      <Registration />
      <ChangePhone />
      <AddAddressContainer />
      <EditAddressContainer />
      {/*  Notification */}
      <Notification />
      <Burger />
      {/* TODO ниже правильный способ вывода модалок, свёрху не работает hook componentDidMount */}
      <Switch>
        {/* Модалка купона */}
        {/* сториз */}
        <Route
          path="*/stories-modal/:slideIndex"
          render={() => <StoriesModal />}
        />
        {/* Выбор ресторана */}
        <Route path="*/modal/choose-rest" render={() => <ChooseRestContainer />} />
        {/* Модалка корзины */}
        <Route path={`*${modalCart}`} render={() => <CartModal />} />
        <Route path={`*${modalHelp}`} render={() => <HelpModal />} />
        <Route path={`*${modalHelpSuccess}`} render={() => <HelpSuccessModal />} />
        <Route path={`*${useCouponModal}`} render={() => <UseCouponModal />} />
        <Route path="*/side-modal/coupon" render={() => <CouponModal />} />
        <Route path={`*${shortAuthPage}`} render={() => <ShortRegistration />} />
        <Route path={`*${modalStop}`} render={() => <StopModal />} />
      </Switch>

      <Switch>
        <Route path="/*/side-modal/product" render={() => <ProductModal />} />
      </Switch>
    </>
  );
};
