import axios from 'axios';
import { authSetToken } from '../store/modules/auth/actionCreators';
import { getFromStorage } from '../functions/localStorage';

const instance = axios.create({
  baseURL: 'http://api.chickenbox.nutnetdev.ru/',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

instance.interceptors.response.use((response) => {
  Object.keys(response).forEach((key) => {
    // console.log(response[key]);
    // console.log(key, 'KEY');
    // console.log(response.config.headers.Authorization);
  });
  if (response.data.data.token) {
    updateToken(response.data.data.token);
  } else if (response.config?.headers?.authorization) {
    updateToken(response.headers.Authorization);
  }

  return response;
});

function formatToken(token) {
  return token.indexOf('Bearer ') === -1 ? `Bearer ${token}` : token;
}

function updateToken(token) {
  window.store.dispatch(authSetToken(formatToken(token)));
  axios.defaults.headers.Authorization = formatToken(token);
}

export default instance;
