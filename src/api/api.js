import { apiRequest } from './apiRequest';
import { getFromStorage } from '../functions/localStorage';

function tokenHeader() {
  if (!getFromStorage('state').auth.loggedIn) return null;
  return {
    headers: {
      Authorization: getFromStorage('state')?.auth?.userToken,
    },
  };
}

const Api = {
  // auth
  async sendPhone(body) {
    return apiRequest('auth/send_code', 'post', body);
  },

  async sendCode(body) {
    return apiRequest('auth/phone', 'post', body);
  },

  // проверка правильности кода
  async checkCode(body) {
    return apiRequest('auth/check_code', 'post', body);
  },

  async sendRegistrationForm(body) {
    return apiRequest('auth/register', 'post', body);
  },

  // user
  async getUser() {
    return apiRequest('cabinet/user', 'get', null, tokenHeader());
  },

  // текущий заказ
  async fetchCurrentOrder() {
    return apiRequest('cabinet/current_order', 'get', null, tokenHeader());
  },

  async userUpdateData(body) {
    return apiRequest('cabinet/user', 'put', body, tokenHeader());
  },

  async addUserAddress(body) {
    return apiRequest('cabinet/address', 'put', body, tokenHeader());
  },

  async deleteAddress(id) {
    return apiRequest(`cabinet/address/${id}`, 'delete', null, tokenHeader());
  },

  // static pages
  async getContactsPage() {
    return apiRequest('pages/kontakty', 'get', null);
  },

  async fetchDeliveryTerms() {
    return apiRequest('pages/usloviya-dostavki', 'get', null);
  },

  // catalog
  async fetchCatalog() {
    return apiRequest(`catalog/${getFromStorage('state')?.orderType?.type}`, 'get', null);
  },

  // catalog coupons
  async fetchCoupons(body) {
    return apiRequest(`catalog/coupons/${body.type || getFromStorage('state')?.orderType?.type}`, 'get', null, tokenHeader());
  },

  // детальная информация о купоне
  async fetchCoupon(body) {
    return apiRequest(`catalog/coupon/${body.code}`, 'get', body, tokenHeader());
  },

  async fetchProduct(productId) {
    return apiRequest(`catalog/product/${productId}`, 'get', null);
  },

  // restaurants
  async fetchRestaurants() {
    return apiRequest('restaurants', 'get', null);
  },

  // promotions
  async fetchPromotions() {
    return apiRequest('stocks', 'get', null);
  },

  // problems
  async fetchProblems() {
    return apiRequest('problems', 'get', null);
  },
  async sendProblems(body) {
    return apiRequest('problems', 'post', body);
  },

  // order history
  async fetchOrderHistory() {
    return apiRequest('cabinet/order_history', 'get', null, tokenHeader());
  },
  // cart
  async addItemToCart(body) {
    return apiRequest(`cart/item/${body.cartId || ''}`, 'post', body, tokenHeader());
  },
  // cart info
  async fetchCartContent(body) {
    return apiRequest(`cart/${body.cartId}`, 'get', null, tokenHeader());
  },
  // cart preorder
  async fetchCartPreorder(body) {
    return apiRequest(`cart/${body.cartId}/preorder`, 'post', null, tokenHeader());
  },
  // cart checkout
  async cartCheckout(body) {
    return apiRequest(`cart/${body.cartId || getFromStorage('state').cart?.addItem?.cartId}/checkout`, 'post', body, tokenHeader());
  },
  // cart add coupon
  async addCouponToCart(body) {
    return apiRequest(`cart/coupon/${getFromStorage('state').cart?.addItem?.cartId || ''}`, 'post', body, tokenHeader());
  },
  // cart update item
  async cartUpdateItem(body) {
    return apiRequest(`cart/item/${getFromStorage('state').cart?.addItem?.cartId || ''}/update`, 'patch', body, tokenHeader());
  },
};

export default Api;
