import instance from './axiosConfig';

export const apiRequest = async (url, method, body = {}, options = {}) => {
  if (method === 'get' || method === 'delete') {
    return instance[method](`${url}?hash=${new Date().getTime()}`, options);
  }
  return instance[method](`${url}?hash=${new Date().getTime()}`, body, options);
};
